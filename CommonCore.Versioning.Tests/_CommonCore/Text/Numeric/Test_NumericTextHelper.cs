﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Text.Numeric.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_NumericTextHelper
    {
        [Theory]
        [InlineData(false, "0123456789")]
        [InlineData(false, "abcdefghijklmnopqrstuvwxyz")]
        [InlineData(false, "ABCDEFGHIJKLMNOPQRSTUVWXYZ")]
        [InlineData(false, "!@#$%^&*()_+=,./<>?[]{};:'\"\\|")]
        [InlineData(true, "-")]
        public void IsDash_Char(bool expected, string subjects)
        {
            foreach (char subject in subjects)
            { NumericTextHelper.IsDash(subject).Should().Be(expected); }
        }

        [Theory]
        [InlineData(false, "0123456789")]
        [InlineData(true, "abcdefghijklmnopqrstuvwxyz")]
        [InlineData(true, "ABCDEFGHIJKLMNOPQRSTUVWXYZ")]
        [InlineData(false, "-!@#$%^&*()_+=,./<>?[]{};:'\"\\|")]
        public void IsAlphabetic_Char(bool expected, string subjects)
        {
            foreach (char subject in subjects)
            { NumericTextHelper.IsAlphabetic(subject).Should().Be(expected); }
        }

        [Theory]
        [InlineData(false, null)]
        [InlineData(false, "")]
        [InlineData(false, "a0b")]
        [InlineData(true, "abc")]
        public void IsAlphabetic_String(bool expected, string subject)
        {
            NumericTextHelper.IsAlphabetic(subject.AsSpan()).Should().Be(expected);
            NumericTextHelper.IsAlphabetic(subject).Should().Be(expected);
        }

        [Theory]
        [InlineData(true, "0123456789")]
        [InlineData(false, "abcdefghijklmnopqrstuvwxyz")]
        [InlineData(false, "ABCDEFGHIJKLMNOPQRSTUVWXYZ")]
        [InlineData(false, "-!@#$%^&*()_+=,./<>?[]{};:'\"\\|")]
        public void IsNumeric_Char(bool expected, string subjects)
        {
            foreach (char subject in subjects)
            { NumericTextHelper.IsNumeric(subject).Should().Be(expected); }
        }

        [Theory]
        [InlineData(false, null)]
        [InlineData(false, "")]
        [InlineData(false, "0x9")]
        [InlineData(true, "059")]
        public void IsNumeric_String(bool expected, string subject)
        {
            NumericTextHelper.IsNumeric(subject.AsSpan()).Should().Be(expected);
            NumericTextHelper.IsNumeric(subject).Should().Be(expected);
        }

        [Theory]
        [InlineData(true, "0123456789")]
        [InlineData(true, "abcdefghijklmnopqrstuvwxyz")]
        [InlineData(true, "ABCDEFGHIJKLMNOPQRSTUVWXYZ")]
        [InlineData(false, "-!@#$%^&*()_+=,./<>?[]{};:'\"\\|")]
        public void IsAlphaNumeric_Char(bool expected, string subjects)
        {
            foreach (char subject in subjects)
            { NumericTextHelper.IsAlphaNumeric(subject).Should().Be(expected); }
        }

        [Theory]
        [InlineData(false, "")]
        [InlineData(false, "0a#b9")]
        [InlineData(true, "0a5b9")]
        public void IsAlphaNumeric_String(bool expected, string subject)
        {
            NumericTextHelper.IsAlphaNumeric(subject.AsSpan()).Should().Be(expected);
            NumericTextHelper.IsAlphaNumeric(subject).Should().Be(expected);
        }

        [Theory]
        [InlineData(0, null)]
        [InlineData(0, "")]
        [InlineData(0, "0")]
        [InlineData(0, "10")]
        [InlineData(2, "001")]
        [InlineData(2, "000")]
        public void CountLeadingZeros(int expected, string subject)
        {
            NumericTextHelper.CountLeadingZeros(subject.AsSpan()).Should().Be(expected);
            NumericTextHelper.CountLeadingZeros(subject).Should().Be(expected);
        }

        [Theory]
        [InlineData(false, null, "0")]
        [InlineData(false, "", "0")]
        [InlineData(false, "1", "0")]
        [InlineData(true, null, null)]
        [InlineData(true, null, "")]
        [InlineData(true, "", "")]
        [InlineData(true, "0", "0")]
        [InlineData(true, "1", "01")]
        public void Equate(bool expected, string left, string right)
        {
            NumericTextHelper.Equate(left.AsSpan(), right.AsSpan()).Should().Be(expected);
            NumericTextHelper.Equate(right.AsSpan(), left.AsSpan()).Should().Be(expected);
            NumericTextHelper.Equate(left, right).Should().Be(expected);
            NumericTextHelper.Equate(right, left).Should().Be(expected);
        }

        [Theory]
        [InlineData(0, null, null)]
        [InlineData(0, null, "")]
        [InlineData(0, "", "")]
        [InlineData(-1, "", "0")]
        [InlineData(1, "0", "")]
        [InlineData(-1, "0", "00")]
        [InlineData(-1, "01", "001")]
        [InlineData(0, "010", "010")]
        [InlineData(-1, "a", "z")]
        public void Compare_Specific(int expected, string left, string right)
        {
            int expectInverted = expected == 0 ? 0 : expected * -1;
            NumericTextHelper.Compare(left, left).Should().Be(0);
            NumericTextHelper.Compare(right, right).Should().Be(0);
            NumericTextHelper.Compare(left, right).Should().Be(expected);
            NumericTextHelper.Compare(right, left).Should().Be(expectInverted);
        }

        [Theory]
        [InlineData(0, 100)]
        [InlineData(100, 500)]
        [InlineData(500, 1100)]
        public void Compare_Range(int minInclusive, int maxExclusive)
        {
            for (int n = minInclusive; n < maxExclusive; n++)
            {
                string cur = n.ToString();
                NumericTextHelper.Compare(cur, cur).Should().Be(0);
                if (n > 0)
                {
                    string prev = (n - 1).ToString();
                    NumericTextHelper.Compare(prev, prev).Should().Be(0);
                    NumericTextHelper.Compare(prev, cur).Should().Be(-1);
                    NumericTextHelper.Compare(cur, prev).Should().Be(1);
                }
                string next = (n + 1).ToString();
                NumericTextHelper.Compare(next, next).Should().Be(0);
                NumericTextHelper.Compare(cur, next).Should().Be(-1);
                NumericTextHelper.Compare(next, cur).Should().Be(1);
            }
        }

        [Theory]
        [InlineData("0", null)]
        [InlineData("0", "")]
        [InlineData("001", "000")]
        [InlineData("010", "009")]
        [InlineData("100", "099")]
        public void Increment_Specific(string expected, string subject)
            => NumericTextHelper.Increment(subject).Should().Be(expected);

        [Theory]
        [InlineData(0, 100)]
        [InlineData(100, 500)]
        [InlineData(500, 1000)]
        public void Increment_Range(int minInclusive, int maxExclusive)
        {
            for (int n = minInclusive; n < maxExclusive; n++)
            { NumericTextHelper.Increment(n.ToString()).Should().Be((n + 1).ToString()); }
        }

        //[Theory]
        //[InlineData("1", 1)]
        //[InlineData("1,000", 0)]
        //[InlineData("1.1", 0)]
        //[InlineData("abcd", 0)]
        //public void ParseNumber(string subject, int expected)
        //    => NumericTextHelper.ParseNumber(subject, 0).Should().Be(expected);
    }
}
