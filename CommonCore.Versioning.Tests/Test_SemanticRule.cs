﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Versioning.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_SemanticRule
    {
        [Fact]
        public void Constructor_Throw()
        {
            Action act;

            act = () => new SemanticRule<string>(null, null, null, null);
            act.Should().Throw<ArgumentNullException>();

            act = () => new SemanticRule<string>("", null, null, null);
            act.Should().Throw<ArgumentNullException>();

            act = () => new SemanticRule<string>("", "", null, null);
            act.Should().Throw<ArgumentNullException>();

            act = () => new SemanticRule<string>("", "", x => true, null);
            act.Should().NotThrow<ArgumentNullException>();

            act = () => new SemanticRule<string>("", "", x => true, x => string.Empty);
            act.Should().NotThrow<ArgumentNullException>();
        }
    }
}
