﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Versioning.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_Semantic
    {
        [Fact]
        public void Constants()
        {
            Semantic.Texts.CycleSymbol.Should().Be('-');
            Semantic.Texts.CycleSymbolString.Should().Be(Semantic.Texts.CycleSymbol.ToString());

            Semantic.Texts.BuildSymbol.Should().Be('+');
            Semantic.Texts.BuildSymbolString.Should().Be(Semantic.Texts.BuildSymbol.ToString());

            Semantic.Texts.DelimitSymbol.Should().Be('.');
            Semantic.Texts.DelimitSymbolString.Should().Be(Semantic.Texts.DelimitSymbol.ToString());

            Semantic.Texts.DefaultToken.Should().Be('0');
            Semantic.Texts.DefaultTokenString.Should().Be(Semantic.Texts.DefaultToken.ToString());
        }
    }
}
