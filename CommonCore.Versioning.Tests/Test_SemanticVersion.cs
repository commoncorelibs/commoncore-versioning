﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace CommonCore.Versioning.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_SemanticVersion
    {
        [Theory]
        [MemberData(nameof(TestInfo.GetValidVersionStrings), MemberType = typeof(TestInfo))]
        [MemberData(nameof(TestInfo.GetInvalidVersionStrings), MemberType = typeof(TestInfo))]
        public void Create(string versionString)
        {
            var subject = SemanticVersion.Create(versionString);
            //subject.Core.Should().HaveCount(3);
            //subject.Cycle.Should().HaveCount(4);
            //subject.Build.Should().HaveCount(5);

            var subject2 = SemanticVersion.Create(subject.Core, subject.Cycle, subject.Build);
            subject.Equals(subject2).Should().BeTrue();
        }

        [Fact]
        public void Create_Empty()
        {
            var subject = SemanticVersion.Create("");
            ((IEnumerable<SemanticToken>) subject.Core).Should().NotBeNull().And.BeEmpty();
            ((IEnumerable<SemanticToken>) subject.Cycle).Should().NotBeNull().And.BeEmpty();
            ((IEnumerable<SemanticToken>) subject.Build).Should().NotBeNull().And.BeEmpty();

            var subject1 = SemanticVersion.Create((string[]?) null, (string[]?) null, (string[]?) null);
            subject.Equals(subject1).Should().BeTrue();
            ((IEnumerable<SemanticToken>) subject1.Core).Should().NotBeNull().And.BeEmpty();
            ((IEnumerable<SemanticToken>) subject1.Cycle).Should().NotBeNull().And.BeEmpty();
            ((IEnumerable<SemanticToken>) subject1.Build).Should().NotBeNull().And.BeEmpty();

            var subject2 = SemanticVersion.Create((IEnumerable<string>?) null, (IEnumerable<string>?) null, (IEnumerable<string>?) null);
            subject.Equals(subject2).Should().BeTrue();
            ((IEnumerable<SemanticToken>) subject2.Core).Should().NotBeNull().And.BeEmpty();
            ((IEnumerable<SemanticToken>) subject2.Cycle).Should().NotBeNull().And.BeEmpty();
            ((IEnumerable<SemanticToken>) subject2.Build).Should().NotBeNull().And.BeEmpty();
        }
    }
}
