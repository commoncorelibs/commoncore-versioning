﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace CommonCore.Versioning.Tests
{
    [ExcludeFromCodeCoverage]
    public static class TestInfo
    {
        public static IEnumerable<object[]> GetData_Validate(bool valid)
        {
            var versions = valid ? ValidVersionStrings : InvalidVersionStrings;
            foreach (var version in versions) { yield return new object[] { version }; }
        }

        public static IEnumerable<object[]> GetValidVersionStrings()
        { foreach (var version in ValidVersionStrings) { yield return new object[] { version }; } }

        public static IEnumerable<object[]> GetInvalidVersionStrings()
        { foreach (var version in InvalidVersionStrings) { yield return new object[] { version }; } }

        public static IReadOnlyList<string> ValidVersionStrings { get; } = new string[]
        {
            "0.0.0-0+0",
            "0.0.0",
            "0.0.0-0+001",
            "0.0.0-pre.0",
            "0.0.0+build.0",
            "0.0.0-pre.0+build.0",
            "0.0.0-pre.alpha",
            "0.0.0-rc.1",
            "0.0.0+test-deploy.0",
            "0.0.0-pre.0.0.alpha.0.0.exp",
            "1.2.3-pre.4.5.6.7+build.8.9.10.11.test",
        };

        public static IReadOnlyList<string> InvalidVersionStrings { get; } = new string[]
        {
            //"0.0.0-0+0",
            "",
            "0",
            "0.0",
            "0.",
            "0.0.",
            ".0",
            ".0.0",
            "0.0.0-",
            "0.0.0+",
            "0.0.0-0+",
            "0.0.0-+0",
            "X.0.0",
            "0.X.0",
            "0.0.X",
        };

        //public readonly struct Count : IComparable, IComparable<Count>, IComparable<Index>, IComparable<int>, IEquatable<Count>, IEquatable<Index>, IEquatable<int>
        //{
        //    public static Count None { get; } = new Count();

        //    public Count(int value)
        //    {
        //        if (value < 0) { throw new ArgumentOutOfRangeException(nameof(value), "Non-negative number required."); }
        //        this.Value = value;
        //    }

        //    public readonly bool IsEmpty => this.Value == 0;

        //    public readonly int Value { get; }

        //    public readonly override string ToString() => this.Value.ToString();

        //    public readonly override int GetHashCode() => HashCode.Combine(this.Value);

        //    public readonly override bool Equals(object obj) => obj is Count other && this.Value == other.Value;

        //    public readonly bool Equals(Count other) => this.Value == other.Value;

        //    public readonly bool Equals(Index other) => this.Value == other.Value;

        //    public readonly bool Equals(int other) => this.Value.Equals(other);

        //    public readonly int CompareTo(object obj) => this.Value.CompareTo(obj);

        //    public readonly int CompareTo(Count other) => this.Value.CompareTo(other.Value);

        //    public readonly int CompareTo(Index other) => this.Value.CompareTo(other.Value);

        //    public readonly int CompareTo(int other) => this.Value.CompareTo(other);

        //    public static bool operator ==(Count left, Count right) => left.Value.Equals(right.Value);

        //    public static bool operator !=(Count left, Count right) => !left.Value.Equals(right.Value);

        //    public static bool operator <(Count left, Count right) => left.Value.CompareTo(right.Value) < 0;

        //    public static bool operator <=(Count left, Count right) => left.Value.CompareTo(right.Value) <= 0;

        //    public static bool operator >(Count left, Count right) => left.Value.CompareTo(right.Value) > 0;

        //    public static bool operator >=(Count left, Count right) => left.Value.CompareTo(right.Value) >= 0;




        //    public static bool operator ==(Index left, Count right) => left.Value.Equals(right.Value);

        //    public static bool operator !=(Index left, Count right) => !left.Value.Equals(right.Value);

        //    public static bool operator <(Index left, Count right) => left.Value.CompareTo(right.Value) < 0;

        //    public static bool operator <=(Index left, Count right) => left.Value.CompareTo(right.Value) <= 0;

        //    public static bool operator >(Index left, Count right) => left.Value.CompareTo(right.Value) > 0;

        //    public static bool operator >=(Index left, Count right) => left.Value.CompareTo(right.Value) >= 0;




        //    public static bool operator ==(Count left, Index right) => left.Value.Equals(right.Value);

        //    public static bool operator !=(Count left, Index right) => !left.Value.Equals(right.Value);

        //    public static bool operator <(Count left, Index right) => left.Value.CompareTo(right.Value) < 0;

        //    public static bool operator <=(Count left, Index right) => left.Value.CompareTo(right.Value) <= 0;

        //    public static bool operator >(Count left, Index right) => left.Value.CompareTo(right.Value) > 0;

        //    public static bool operator >=(Count left, Index right) => left.Value.CompareTo(right.Value) >= 0;




        //    public static bool operator ==(int left, Count right) => left.Equals(right.Value);

        //    public static bool operator !=(int left, Count right) => !left.Equals(right.Value);

        //    public static bool operator <(int left, Count right) => left.CompareTo(right.Value) < 0;

        //    public static bool operator <=(int left, Count right) => left.CompareTo(right.Value) <= 0;

        //    public static bool operator >(int left, Count right) => left.CompareTo(right.Value) > 0;

        //    public static bool operator >=(int left, Count right) => left.CompareTo(right.Value) >= 0;




        //    public static bool operator ==(Count left, int right) => left.Value.Equals(right);

        //    public static bool operator !=(Count left, int right) => !left.Value.Equals(right);

        //    public static bool operator <(Count left, int right) => left.Value.CompareTo(right) < 0;

        //    public static bool operator <=(Count left, int right) => left.Value.CompareTo(right) <= 0;

        //    public static bool operator >(Count left, int right) => left.Value.CompareTo(right) > 0;

        //    public static bool operator >=(Count left, int right) => left.Value.CompareTo(right) >= 0;




        //    public static implicit operator int(Count count) => count.Value;

        //    public static implicit operator Count(int value) => new Count(value);
        //}

        //public readonly struct Index : IEquatable<Index>
        //{
        //    public Index(int value, bool fromEnd = false);

        //    public static Index End { get; }

        //    public static Index Start { get; }

        //    public bool IsFromEnd { get; }

        //    public int Value { get; }

        //    public static Index FromEnd(int value);

        //    public static Index FromStart(int value);

        //    public bool Equals(Index other);

        //    [NullableContextAttribute(2)]
        //    public override bool Equals(object? value);

        //    public override int GetHashCode();

        //    public int GetOffset(int length);

        //    [NullableContextAttribute(1)]
        //    public override string ToString();

        //    public static implicit operator Index(int value);
        //}

        //public readonly struct RangeX //: IEquatable<Range>
        //{
        //    public RangeX(Index start, Index end);

        //    public static RangeX All { get; }

        //    public Index End { get; }

        //    public Index Start { get; }

        //    public static RangeX EndAt(Index end);

        //    public static RangeX StartAt(Index start);

        //    public override bool Equals(object value);

        //    public bool Equals(RangeX other);

        //    public override int GetHashCode();

        //    [return: TupleElementNames(new[] { "Offset", "Length" })]
        //    public (int Offset, int Length) GetOffsetAndLength(int length);

        //    public override string ToString();
        //}
















        ///// <summary>
        ///// The <see cref="tri"/> struct represents a tri-state value, similar to [-1, 0, 1].
        ///// </summary>
        //public struct tri : IComparable, IComparable<tri>, IEquatable<tri>
        //{
        //    public static readonly tri neg = new tri(-1);
        //    public static readonly tri nil = new tri(0);
        //    public static readonly tri pos = new tri(1);

        //    public static bool Equate(tri left, tri right) => left._value.Equals(right._value);

        //    public static int Compare(tri left, tri right) => left._value.CompareTo(right._value);

        //    private tri(sbyte value) => this._value = value;
        //    private readonly sbyte _value;

        //    public override int GetHashCode() => HashCode.Combine(this._value);

        //    public override bool Equals(object obj) => obj is tri tri && tri.Equate(this, tri);

        //    public bool Equals(tri other) => tri.Equate(this, other);

        //    public int CompareTo(object obj) => obj is tri tri ? this.CompareTo(tri) : this._value.CompareTo(obj);

        //    public int CompareTo(tri other) => tri.Compare(this, other);

        //    public static implicit operator bool?(tri item) => item._value == -1 ? false : item._value == 1 ? true : (bool?)null;
        //    public static implicit operator sbyte(tri item) => item._value;
        //    public static implicit operator short(tri item) => item._value;
        //    public static implicit operator int(tri item) => item._value;
        //    public static implicit operator long(tri item) => item._value;

        //    public static bool operator ==(tri left, tri right) => tri.Equate(left, right);

        //    public static bool operator !=(tri left, tri right) => !tri.Equate(left, right);

        //    public static bool operator >(tri left, tri right) => tri.Compare(left, right) > 0;

        //    public static bool operator >=(tri left, tri right) => tri.Compare(left, right) >= 0;

        //    public static bool operator <(tri left, tri right) => tri.Compare(left, right) < 0;

        //    public static bool operator <=(tri left, tri right) => tri.Compare(left, right) <= 0;
        //}

        /*
        This code below SHOULD be all that is necessary, but C# structs make it impossible.
        Biggest reason is that there is no way to set the the initial value of struct field.
        Can't do it in the declaration, can't have a paramless construct, just can't do it.
        */

        //public struct tritrue : tri { private trifalse() => this.value = 1; }

        //public struct trinil : tri { private trifalse() => this.value = 0; }

        //public struct trifalse : tri { private trifalse() => this.value = -1; }

        //public interface tri
        //{
        //    public sbyte value { get; }
        //    //public static implicit operator bool(tritrue item) => true;
        //    //public static implicit operator sbyte(tritrue item) => 1;
        //    //public static implicit operator short(tritrue item) => 1;
        //    //public static implicit operator int(tritrue item) => 1;
        //    //public static implicit operator long(tritrue item) => 1;
        //}
    }
}
