//using System;
//using FluentAssertions;
//using Xunit;

//namespace CommonCore.Versioning.Tests
//{
//    public class Test_SemanticVersion
//    {
//        [Fact]
//        public void ExceptionConstructor()
//        {
//            new SemanticVersionException().Should().NotBeNull();
//        }

//        [Fact]
//        public void StaticZero()
//        {
//            string text = "0.0.0";
//            var version = SemanticVersionStruct.Zero;
//            version.Text.Should().Be(text);
//            version.ToString().Should().Be(text);
//            version.Length.Should().Be(text.Length);
//            version.Major.Value.Should().Be(0);
//            version.Major.Title.Should().Be(SemanticVersionStruct.DefaultTitleMajor);
//            version.Minor.Value.Should().Be(0);
//            version.Minor.Title.Should().Be(SemanticVersionStruct.DefaultTitleMinor);
//            version.Patch.Value.Should().Be(0);
//            version.Patch.Title.Should().Be(SemanticVersionStruct.DefaultTitlePatch);
//            version.Cycle.Exists.Should().Be(false);
//            version.Cycle.Title.Should().Be(SemanticVersionStruct.DefaultTitleCycle);
//            version.Build.Exists.Should().Be(false);
//            version.Build.Title.Should().Be(SemanticVersionStruct.DefaultTitleBuild);
//        }

//        [Fact]
//        public void StaticOne()
//        {
//            string text = "1.0.0";
//            var version = SemanticVersionStruct.One;
//            version.Text.Should().Be(text);
//            version.ToString().Should().Be(text);
//            version.Length.Should().Be(text.Length);
//            version.Major.Value.Should().Be(1);
//            version.Major.Title.Should().Be(SemanticVersionStruct.DefaultTitleMajor);
//            version.Minor.Value.Should().Be(0);
//            version.Minor.Title.Should().Be(SemanticVersionStruct.DefaultTitleMinor);
//            version.Patch.Value.Should().Be(0);
//            version.Patch.Title.Should().Be(SemanticVersionStruct.DefaultTitlePatch);
//            version.Cycle.Exists.Should().Be(false);
//            version.Cycle.Title.Should().Be(SemanticVersionStruct.DefaultTitleCycle);
//            version.Build.Exists.Should().Be(false);
//            version.Build.Title.Should().Be(SemanticVersionStruct.DefaultTitleBuild);
//        }

//        [Theory]
//        [InlineData("1.0.0", true, true)]
//        [InlineData(".0.0", true, false)]
//        public void StaticPatterns(string text, bool lazy, bool strict)
//        {
//            SemanticVersionStruct.PatternLazy.Match(text).Success.Should().Be(lazy);
//            SemanticVersionStruct.PatternStrict.Match(text).Success.Should().Be(strict);
//        }

//        [Theory]
//        [InlineData(null)]
//        public void ConstructNull(string version)
//        {
//            Action act = () => new SemanticVersionStruct(version);
//            act.Should().Throw<ArgumentNullException>();
//        }

//        [Theory]
//        [InlineData("0.0.0",                      0, 0, 0, 0, 0)]
//        [InlineData("1.1.1",                      1, 1, 1, 0, 0)]
//        [InlineData("1.1.1-1",                    1, 1, 1, 1, 0)]
//        [InlineData("1.1.1-pre1",                 1, 1, 1, 1, 0)]
//        [InlineData("1.1.1-alpha",                1, 1, 1, 1, 0)]
//        [InlineData("1.1.1-alpha.1",              1, 1, 1, 2, 0)]
//        [InlineData("1.1.1-alpha.beta",           1, 1, 1, 2, 0)]
//        [InlineData("1.1.1-alpha.11.beta",        1, 1, 1, 3, 0)]
//        [InlineData("1.1.1-alpha6.11.beta",       1, 1, 1, 3, 0)]
//        [InlineData("1.1.1-pre+build",            1, 1, 1, 1, 1)]
//        [InlineData("1.0.0-0.3.7",                1, 0, 0, 3, 0)]
//        [InlineData("1.0.0-x.7.z.92",             1, 0, 0, 4, 0)]
//        [InlineData("1.0.0-alpha+001",            1, 0, 0, 1, 1)]
//        [InlineData("1.0.0+20130313144700",       1, 0, 0, 0, 1)]
//        [InlineData("1.0.0-beta+exp.sha.5114f85", 1, 0, 0, 1, 3)]
//        public void ConstructValid(string text, int major, int minor, int patch, int prereleaseCount, int buildCount)
//        {
//            var version = new SemanticVersionStruct(text);
//            version.Text.Should().Be(text);
//            version.ToString().Should().Be(text);
//            string implicitString = version;
//            implicitString.Should().Be(text);
//            version.Length.Should().Be(text.Length);
//            version.Major.Value.Should().Be(major);
//            version.Major.Title.Should().Be(SemanticVersionStruct.DefaultTitleMajor);
//            version.Minor.Value.Should().Be(minor);
//            version.Minor.Title.Should().Be(SemanticVersionStruct.DefaultTitleMinor);
//            version.Patch.Value.Should().Be(patch);
//            version.Patch.Title.Should().Be(SemanticVersionStruct.DefaultTitlePatch);
//            version.Cycle.Exists.Should().Be(prereleaseCount > 0);
//            version.Cycle.Title.Should().Be(SemanticVersionStruct.DefaultTitleCycle);
//            version.Build.Exists.Should().Be(buildCount > 0);
//            version.Build.Title.Should().Be(SemanticVersionStruct.DefaultTitleBuild);
//        }

//        [Theory]
//        [InlineData("")]
//        [InlineData("a")]
//        [InlineData("1")]
//        [InlineData("1abcd")]
//        [InlineData("1.1")]
//        public void ConstructInvalid(string version)
//        {
//            Action act = () => new SemanticVersionStruct(version);
//            act.Should().ThrowExactly<FormatException>();
//        }

//        [Theory]
//        [InlineData(".1.1", SemanticVersionStruct.DefaultTitleMajor, nameof(SemanticVersionRule.NotEmptyRule))]
//        [InlineData("1..1", SemanticVersionStruct.DefaultTitleMinor, nameof(SemanticVersionRule.NotEmptyRule))]
//        [InlineData("1.1.", SemanticVersionStruct.DefaultTitlePatch, nameof(SemanticVersionRule.NotEmptyRule))]
//        [InlineData("1.1.-1", SemanticVersionStruct.DefaultTitlePatch, nameof(SemanticVersionRule.NotEmptyRule))]

//        [InlineData("@1.1.1", SemanticVersionStruct.DefaultTitleMajor, nameof(SemanticVersionRule.OnlyDigitsRule))]
//        [InlineData("-1.1.1", SemanticVersionStruct.DefaultTitleMajor, nameof(SemanticVersionRule.OnlyDigitsRule))]
//        [InlineData("1a.1.1", SemanticVersionStruct.DefaultTitleMajor, nameof(SemanticVersionRule.OnlyDigitsRule))]
//        [InlineData("1.@1.1", SemanticVersionStruct.DefaultTitleMinor, nameof(SemanticVersionRule.OnlyDigitsRule))]
//        [InlineData("1.-1.1", SemanticVersionStruct.DefaultTitleMinor, nameof(SemanticVersionRule.OnlyDigitsRule))]
//        [InlineData("1.1a.1", SemanticVersionStruct.DefaultTitleMinor, nameof(SemanticVersionRule.OnlyDigitsRule))]
//        [InlineData("1.1.@1", SemanticVersionStruct.DefaultTitlePatch, nameof(SemanticVersionRule.OnlyDigitsRule))]
//        [InlineData("1.1.1a", SemanticVersionStruct.DefaultTitlePatch, nameof(SemanticVersionRule.OnlyDigitsRule))]

//        [InlineData("01.1.1", SemanticVersionStruct.DefaultTitleMajor, nameof(SemanticVersionRule.NotLeadingZeroRule))]
//        [InlineData("1.01.1", SemanticVersionStruct.DefaultTitleMinor, nameof(SemanticVersionRule.NotLeadingZeroRule))]
//        [InlineData("1.1.01", SemanticVersionStruct.DefaultTitlePatch, nameof(SemanticVersionRule.NotLeadingZeroRule))]

//        [InlineData("1.1.1-", SemanticVersionStruct.DefaultTitleCycle + "[0]", nameof(SemanticVersionRule.NotEmptyRule))]
//        [InlineData("1.1.1-pre..1", SemanticVersionStruct.DefaultTitleCycle + "[1]", nameof(SemanticVersionRule.NotEmptyRule))]
//        [InlineData("1.1.1-pre.1.", SemanticVersionStruct.DefaultTitleCycle + "[2]", nameof(SemanticVersionRule.NotEmptyRule))]
//        [InlineData("1.1.1-01", SemanticVersionStruct.DefaultTitleCycle + "[0]", nameof(SemanticVersionRule.NotLeadingZeroRule))]
//        [InlineData("1.1.1-pre.01.1", SemanticVersionStruct.DefaultTitleCycle + "[1]", nameof(SemanticVersionRule.NotLeadingZeroRule))]
//        [InlineData("1.1.1-pre.1.01", SemanticVersionStruct.DefaultTitleCycle + "[2]", nameof(SemanticVersionRule.NotLeadingZeroRule))]

//        //[InlineData("1.1.1-@", SemanticVersionStruct.DefaultTitleCycle + "[0]", nameof(SemanticVersionRule.OnlyDigitsRule))]
//        //[InlineData("1.1.1-@0", SemanticVersionStruct.DefaultTitleCycle + "[0]", nameof(SemanticVersionRule.OnlyDigitsRule))]
//        [InlineData("1.1.1-@0a", SemanticVersionStruct.DefaultTitleCycle + "[0]", nameof(SemanticVersionRule.OnlyAlphaNumericRule))]
//        [InlineData("1.1.1-@pre", SemanticVersionStruct.DefaultTitleCycle + "[0]", nameof(SemanticVersionRule.OnlyAlphaNumericRule))]

//        [InlineData("1.1.1+", SemanticVersionStruct.DefaultTitleBuild + "[0]", nameof(SemanticVersionRule.NotEmptyRule))]
//        [InlineData("1.1.1+build..1", SemanticVersionStruct.DefaultTitleBuild + "[1]", nameof(SemanticVersionRule.NotEmptyRule))]
//        [InlineData("1.1.1+build.1.", SemanticVersionStruct.DefaultTitleBuild + "[2]", nameof(SemanticVersionRule.NotEmptyRule))]

//        //[InlineData("1.1.1+@", SemanticVersionStruct.DefaultTitleBuild + "[0]", nameof(SemanticVersionRule.OnlyDigitsRule))]
//        //[InlineData("1.1.1+@0", SemanticVersionStruct.DefaultTitleBuild + "[0]", nameof(SemanticVersionRule.OnlyDigitsRule))]
//        [InlineData("1.1.1+@0a", SemanticVersionStruct.DefaultTitleBuild + "[0]", nameof(SemanticVersionRule.OnlyAlphaNumericRule))]
//        [InlineData("1.1.1+@build", SemanticVersionStruct.DefaultTitleBuild + "[0]", nameof(SemanticVersionRule.OnlyAlphaNumericRule))]
//        public void ConstructInvalidIdentifier(string text, string idTitle, string ruleTitle)
//        {
//            var version = new SemanticVersionStruct(text);
//            Action act = () => version.Validate();
//            act.Should().Throw<AggregateException>().WithInnerException<SemanticVersionException>().Where(e => e.Identifier.Title == idTitle).Where(e => e.Rule.Title == ruleTitle);
//        }

//        [Theory]
//        [InlineData("0.0.0", "0.0.0", 0)]
//        [InlineData("0.0.0-pre.0", "0.0.0-pre.0", 0)]
//        [InlineData("0.0.0+build.0", "0.0.0+build.0", 0)]
//        [InlineData("0.0.0-pre.0+build.0", "0.0.0-pre.0+build.0", 0)]
//        [InlineData("0.0.0", "1.0.0", -1)]
//        [InlineData("0.0.0", "0.1.0", -1)]
//        [InlineData("0.0.0", "0.0.1", -1)]
//        [InlineData("0.0.2", "0.1.0", -1)]
//        [InlineData("0.2.0", "1.0.0", -1)]
//        [InlineData("0.0.0-pre", "0.0.0", -1)]
//        [InlineData("0.0.0-pre", "0.0.0-prer", -1)]
//        [InlineData("0.0.0-pre", "0.0.0-pre0", -1)]
//        [InlineData("0.0.0-pre0", "0.0.0-pre1", -1)]
//        [InlineData("0.0.0-pre0", "0.0.0-pre10", -1)]
//        [InlineData("0.0.0-pre00", "0.0.0-pre1", -1)]
//        [InlineData("0.0.0-pre00", "0.0.0-pre10", -1)]
//        [InlineData("0.0.0-pre", "0.0.0-pre.alpha", -1)]
//        [InlineData("0.0.0-pre", "0.0.0-pre.0", -1)]
//        [InlineData("0.0.0-pre.0", "0.0.0-pre.alpha", -1)]
//        [InlineData("0.0.0-pre.alpha", "0.0.0-pre.beta", -1)]
//        [InlineData("0.0.0-beta.11", "0.0.0-rc.1", -1)]
//        [InlineData("0.0.0-pre.0", "0.0.0-pre.0+build.0", -1)]
//        [InlineData("0.0.0", "0.0.0+build.0", -1)]
//        public void Compare(string leftInput, string rightInput, int expectedCompare)
//        {
//            var left = new SemanticVersionStruct(leftInput);
//            var right = new SemanticVersionStruct(rightInput);

//            Action act;

//            act = () => { left.Validate(); right.Validate(); };
//            act.Should().NotThrow();

//            int compare;

//            // left compare right
//            compare = expectedCompare;
//            SemanticVersionStruct.Compare(left, right).Should().Be(compare);
//            left.CompareTo(right).Should().Be(compare);
//            left.CompareTo((object)right).Should().Be(compare);
//            act = () => left.CompareTo(new object());
//            act.Should().Throw<ArgumentException>();
//            (left < right).Should().Be(compare == -1);
//            (left > right).Should().Be(compare == 1);
//            (left <= right).Should().Be(compare <= 0);
//            (left >= right).Should().Be(compare >= 0);

//            // left equals right
//            SemanticVersionStruct.Equate(left, right).Should().Be(compare == 0);
//            left.Equals(right).Should().Be(compare == 0);
//            left.Equals((object)right).Should().Be(compare == 0);
//            left.Equals(new object()).Should().BeFalse();
//            (left == right).Should().Be(compare == 0);
//            (left != right).Should().Be(compare != 0);
//            if (expectedCompare == 0)
//            { left.GetHashCode().Should().Be(right.GetHashCode()); }

//            // right compare left
//            compare = expectedCompare * -1;
//            SemanticVersionStruct.Compare(right, left).Should().Be(compare);
//            right.CompareTo(left).Should().Be(compare);
//            right.CompareTo((object)left).Should().Be(compare);
//            act = () => right.CompareTo(new object());
//            act.Should().Throw<ArgumentException>();
//            (right < left).Should().Be(compare == -1);
//            (right > left).Should().Be(compare == 1);
//            (right <= left).Should().Be(compare <= 0);
//            (right >= left).Should().Be(compare >= 0);

//            // right equals left
//            SemanticVersionStruct.Equate(right, left).Should().Be(compare == 0);
//            right.Equals(left).Should().Be(compare == 0);
//            right.Equals((object)left).Should().Be(compare == 0);
//            right.Equals(new object()).Should().BeFalse();
//            (right == left).Should().Be(compare == 0);
//            (right != left).Should().Be(compare != 0);
//            if (expectedCompare == 0)
//            { right.GetHashCode().Should().Be(left.GetHashCode()); }
//        }

//        [Theory]
//        [InlineData("1.1.1-pre.0+build.0", "1.1.1-pre.0+build.0", SemanticVersionStruct.BumpType.None)]
//        [InlineData("1.1.1-pre.0+build.0", "2.0.0", SemanticVersionStruct.BumpType.Major)]
//        [InlineData("1.1.1-pre.0+build.0", "1.2.0", SemanticVersionStruct.BumpType.Minor)]
//        [InlineData("1.1.1+build.0", "1.1.2", SemanticVersionStruct.BumpType.Patch)]
//        [InlineData("1.1.1-pre.0", "1.1.1", SemanticVersionStruct.BumpType.Patch)]
//        [InlineData("1.1.1-pre.0+build.0", "1.1.1", SemanticVersionStruct.BumpType.Patch)]
//        [InlineData("1.1.1-pre.0+build.0", "1.1.1-pre.1", SemanticVersionStruct.BumpType.Cycle)]
//        [InlineData("1.1.1", "1.1.2-pre.0", SemanticVersionStruct.BumpType.Cycle)]
//        [InlineData("1.1.1-pre", "1.1.1-pre.0", SemanticVersionStruct.BumpType.Cycle)]
//        [InlineData("1.1.1-pre0", "1.1.1-pre0.0", SemanticVersionStruct.BumpType.Cycle)]
//        [InlineData("1.1.1-pre.0+build.0", "1.1.1-pre.0+build.1", SemanticVersionStruct.BumpType.Build)]
//        public void Bump(string originalInput, string expectedInput, SemanticVersionStruct.BumpType bump)
//        {
//            var original = new SemanticVersionStruct(originalInput);
//            var expected = new SemanticVersionStruct(expectedInput);

//            Action act = () => { original.Validate(); expected.Validate(); };
//            act.Should().NotThrow();

//            var bumped = original.Bump(bump);
//            bumped.Should().BeEquivalentTo(expected);

//            SemanticVersionStruct otherBumped;
//            switch (bump)
//            {
//                case SemanticVersionStruct.BumpType.Major: { otherBumped = original.BumpMajor(); break; }
//                case SemanticVersionStruct.BumpType.Minor: { otherBumped = original.BumpMinor(); break; }
//                case SemanticVersionStruct.BumpType.Patch: { otherBumped = original.BumpPatch(); break; }
//                case SemanticVersionStruct.BumpType.Cycle: { otherBumped = original.BumpCycle(); break; }
//                case SemanticVersionStruct.BumpType.Build: { otherBumped = original.BumpBuild(); break; }
//                default: { otherBumped = original; break; }
//            }
//            otherBumped.Should().BeEquivalentTo(bumped);
//        }

//        [Theory]

//        [InlineData("1.1.1-pre.0+build.0", "1.1.1-pre.0+build.0", false, false)]
//        [InlineData("1.1.1-pre.0+build.0", "1.1.1+build.0", true, false)]
//        [InlineData("1.1.1-pre.0+build.0", "1.1.1-pre.0", false, true)]
//        [InlineData("1.1.1-pre.0+build.0", "1.1.1", true, true)]

//        [InlineData("1.1.1-pre.0", "1.1.1-pre.0", false, false)]
//        [InlineData("1.1.1-pre.0", "1.1.1", true, false)]
//        [InlineData("1.1.1-pre.0", "1.1.1-pre.0", false, true)]
//        [InlineData("1.1.1-pre.0", "1.1.1", true, true)]

//        [InlineData("1.1.1+build.0", "1.1.1+build.0", false, false)]
//        [InlineData("1.1.1+build.0", "1.1.1+build.0", true, false)]
//        [InlineData("1.1.1+build.0", "1.1.1", false, true)]
//        [InlineData("1.1.1+build.0", "1.1.1", true, true)]
//        public void Strip(string originalInput, string expectedInput, bool prerelease, bool build)
//        {
//            var original = new SemanticVersionStruct(originalInput);
//            var expected = new SemanticVersionStruct(expectedInput);
//            SemanticVersionStruct version;
//            if (prerelease && build) { version = original.StripMeta(); }
//            else if (prerelease) { version = original.StripCycle(); }
//            else if (build) { version = original.StripBuild(); }
//            else { version = original; }

//            version.Should().BeEquivalentTo(expected);
//        }
//    }
//}
