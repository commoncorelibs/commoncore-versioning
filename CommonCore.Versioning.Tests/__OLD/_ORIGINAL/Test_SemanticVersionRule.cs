﻿//using System;
//using FluentAssertions;
//using Xunit;

//namespace CommonCore.Versioning.Tests
//{
//    public class Test_SemanticVersionRule
//    {
//        [Theory]
//        [InlineData("title", "message")]
//        public void Construct(string title, string message)
//        {
//            var rule0 = new SemanticVersionRule(title, message, null);
//            rule0.Title.Should().Be(title);
//            rule0.Message.Should().Be(message);
//            rule0.IsCompliant(SemanticVersionIdentifier.None).Should().Be(false);

//            var rule1 = new SemanticVersionRule(title, message, id => true);
//            rule1.Title.Should().Be(title);
//            rule1.Message.Should().Be(message);
//            rule1.IsCompliant(SemanticVersionIdentifier.None).Should().Be(true);
//        }
//    }
//}
