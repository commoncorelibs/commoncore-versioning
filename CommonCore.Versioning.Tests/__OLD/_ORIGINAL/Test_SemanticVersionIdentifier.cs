﻿//using System;
//using FluentAssertions;
//using Xunit;

//namespace CommonCore.Versioning.Tests
//{
//    public class Test_SemanticVersionIdentifier
//    {
//        [Fact]
//        public void None()
//        {
//            string text = null;
//            int value = 0;
//            var id = SemanticVersionIdentifier.None;
//            id.Title.Should().Be(null);
//            id.Text.Should().Be(text);
//            id.ToString().Should().Be(text);
//            ((string)id).Should().Be(text);
//            id.Value.Should().Be(value);
//            ((int)id).Should().Be(value);
//            id.Numeric.Should().Be(false);
//            id.Length.Should().Be(0);

//            Action act = () => { var x = id[0]; };
//            act.Should().Throw<IndexOutOfRangeException>();
//        }

//        [Fact]
//        public void ConstructTitleNull()
//        {
//            Action act = () => new SemanticVersionIdentifier(null, null);
//            act.Should().Throw<ArgumentNullException>();
//        }

//        [Theory]
//        [InlineData("test", null, false)]
//        [InlineData("test", "", true)]
//        public void ConstructTextNullEmpty(string title, string text, bool numericExpected)
//        {
//            int value = 0;
//            var id = new SemanticVersionIdentifier(title, text);
//            id.Title.Should().Be(title);
//            id.Text.Should().Be(text);
//            id.ToString().Should().Be(text);
//            ((string)id).Should().Be(text);
//            id.Value.Should().Be(value);
//            ((int)id).Should().Be(value);
//            id.Numeric.Should().Be(numericExpected);
//            id.Length.Should().Be(0);

//            Action act;
//            act = () => { var x = id[-1]; };
//            act.Should().Throw<IndexOutOfRangeException>();
//            act = () => { var x = id[0]; };
//            act.Should().Throw<IndexOutOfRangeException>();
//            act = () => { var x = id[1]; };
//            act.Should().Throw<IndexOutOfRangeException>();
//        }

//        [Theory]
//        [InlineData("test", "0", 0, true)]
//        [InlineData("test", "00", 0, true)]
//        [InlineData("test", "1", 1, true)]
//        [InlineData("test", "01", 1, true)]
//        [InlineData("test", "10", 10, true)]
//        [InlineData("test", "010", 10, true)]
//        [InlineData("test", "abc", 0, false)]
//        [InlineData("test", "abc0", 0, false)]
//        [InlineData("test", "abc1", 0, false)]
//        [InlineData("test", "0abc", 0, false)]
//        [InlineData("test", "1abc", 0, false)]
//        [InlineData("test", "0abc1", 0, false)]
//        [InlineData("test", "1abc2", 0, false)]
//        public void ConstructText(string title, string text, int value, bool numeric)
//        {
//            var id = new SemanticVersionIdentifier(title, text);
//            id.Title.Should().Be(title);
//            id.Text.Should().Be(text);
//            id.ToString().Should().Be(text);
//            ((string)id).Should().Be(text);
//            id.Value.Should().Be(value);
//            ((int)id).Should().Be(value);
//            id.Numeric.Should().Be(numeric);
//            id.Length.Should().Be(text.Length);

//            id[0].Should().Be(text[0]);
//            id[text.Length - 1].Should().Be(text[text.Length - 1]);

//            Action act;
//            act = () => { var x = id[-1]; };
//            act.Should().Throw<IndexOutOfRangeException>();
//            act = () => { var x = id[text.Length]; };
//            act.Should().Throw<IndexOutOfRangeException>();
//        }

//        [Theory]
//        [InlineData("test", "0", 0, true)]
//        [InlineData("test", "1", 1, true)]
//        [InlineData("test", "10", 10, true)]
//        public void ConstructValue(string title, string text, int value, bool numeric)
//        {
//            var id = new SemanticVersionIdentifier(title, value);
//            id.Title.Should().Be(title);
//            id.Text.Should().Be(text);
//            id.ToString().Should().Be(text);
//            ((string)id).Should().Be(text);
//            id.Value.Should().Be(value);
//            ((int)id).Should().Be(value);
//            id.Numeric.Should().Be(numeric);
//            id.Length.Should().Be(text.Length);
//        }

//        [Theory]
//        [InlineData("test", "0", "1")]
//        [InlineData("test", "00", "1")]
//        [InlineData("test", "1", "2")]
//        [InlineData("test", "01", "2")]
//        [InlineData("test", "9", "10")]
//        [InlineData("test", "090", "100")]
//        public void BumpNumeric(string title, string originalText, string expectedText)
//        {
//            var original = new SemanticVersionIdentifier(title, originalText);
//            var expected = new SemanticVersionIdentifier(title, expectedText);

//            original.Numeric.Should().BeTrue();
//            expected.Numeric.Should().BeTrue();
//            ((IComparable<SemanticVersionIdentifier>)original).Should().BeLessThan(expected);

//            var bumped = original.GetBumped();
//            bumped.Numeric.Should().BeTrue();
//            ((IComparable<SemanticVersionIdentifier>)original).Should().BeLessThan(bumped);
//        }

//        [Theory]
//        [InlineData("test", "-")]
//        [InlineData("test", "a")]
//        [InlineData("test", "a0")]
//        [InlineData("test", "0a")]
//        [InlineData("test", "0a0")]
//        [InlineData("test", "a0a")]
//        public void BumpAlphanumeric(string title, string text)
//        {
//            var original = new SemanticVersionIdentifier(title, text);
//            original.Numeric.Should().BeFalse();
//            Action act = () => original.GetBumped();
//            act.Should().Throw<InvalidOperationException>();
//        }

//        [Theory]
//        [InlineData("test", "0")]
//        [InlineData("test", "00")]
//        [InlineData("test", "1")]
//        [InlineData("test", "01")]
//        [InlineData("test", "9")]
//        [InlineData("test", "090")]
//        public void ResetNumeric(string title, string originalText)
//        {
//            var original = new SemanticVersionIdentifier(title, originalText);
//            original.Numeric.Should().BeTrue();

//            var reset = original.GetReset();
//            reset.Numeric.Should().BeTrue();
//            reset.Value.Should().Be(0);
//            reset.Title.Should().Be(original.Title);
//        }

//        [Theory]
//        [InlineData("test", "-")]
//        [InlineData("test", "a")]
//        [InlineData("test", "a0")]
//        [InlineData("test", "0a")]
//        [InlineData("test", "0a0")]
//        [InlineData("test", "a0a")]
//        public void ResetAlphanumeric(string title, string text)
//        {
//            var original = new SemanticVersionIdentifier(title, text);
//            original.Numeric.Should().BeFalse();
//            Action act = () => original.GetReset();
//            act.Should().Throw<InvalidOperationException>();
//        }

//        [Theory]
//        [InlineData("test", "0", "0", 0)]
//        [InlineData("test", "1", "0", 1)]
//        [InlineData("test", "0", "1", -1)]
//        [InlineData("test", "a", "9", 1)]
//        [InlineData("test", "9", "a", -1)]
//        public void Compare(string title, string leftText, string rightText, int expected)
//        {
//            var left = new SemanticVersionIdentifier(title, leftText);
//            var right = new SemanticVersionIdentifier(title, rightText);

//            Action act;

//            // left equals right
//            SemanticVersionIdentifier.Equals(left, right).Should().Be(expected == 0);
//            left.Equals(right).Should().Be(expected == 0);
//            left.Equals((object)right).Should().Be(expected == 0);
//            left.Equals(new object()).Should().Be(false);
//            (left == right).Should().Be(expected == 0);
//            (left != right).Should().Be(expected != 0);
//            if (expected == 0)
//            { left.GetHashCode().Should().Be(right.GetHashCode()); }

//            // left compare right
//            SemanticVersionIdentifier.Compare(left, right).Should().Be(expected);
//            left.CompareTo(right).Should().Be(expected);
//            left.CompareTo((object)right).Should().Be(expected);
//            act = () => left.CompareTo(new object());
//            act.Should().Throw<ArgumentException>();
//            (left < right).Should().Be(expected == -1);
//            (left <= right).Should().Be(expected <= 0);
//            (left > right).Should().Be(expected == 1);
//            (left >= right).Should().Be(expected >= 0);

//            // right equals left
//            SemanticVersionIdentifier.Equals(right, left).Should().Be((expected * -1) == 0);
//            right.Equals(left).Should().Be((expected * -1) == 0);
//            right.Equals((object)left).Should().Be((expected * -1) == 0);
//            right.Equals(new object()).Should().Be(false);
//            (right == left).Should().Be((expected * -1) == 0);
//            (right != left).Should().Be((expected * -1) != 0);
//            if (expected == 0)
//            { right.GetHashCode().Should().Be(left.GetHashCode()); }

//            // right compare left
//            SemanticVersionIdentifier.Compare(right, left).Should().Be(expected * -1);
//            right.CompareTo(left).Should().Be(expected * -1);
//            right.CompareTo((object)left).Should().Be(expected * -1);
//            act = () => right.CompareTo(new object());
//            act.Should().Throw<ArgumentException>();
//            (right < left).Should().Be((expected * -1) == -1);
//            (right <= left).Should().Be((expected * -1) <= 0);
//            (right > left).Should().Be((expected * -1) == 1);
//            (right >= left).Should().Be((expected * -1) >= 0);
//        }
//    }
//}
