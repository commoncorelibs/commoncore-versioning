﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using FluentAssertions;
//using Xunit;

//namespace CommonCore.Versioning.Tests
//{
//    public class Test_SemanticVersionMeta
//    {
//        [Fact]
//        public void StaticNone()
//        {
//            var id = SemanticVersionMeta.None;
//            id.Title.Should().Be(null);
//            id.Input.Should().Be(null);
//            id.ToString().Should().Be(null);
//            id.Identifiers.Should().BeNull();
//            ((IEnumerable)id).GetEnumerator().Should().BeNull();
//            id.GetEnumerator().Should().BeNull();
//            id.Count.Should().Be(0);
//            id.Exists.Should().Be(false);
//            id.Length.Should().Be(0);

//            Action act;
//            act = () => { var x = id[-1]; };
//            act.Should().Throw<IndexOutOfRangeException>();
//            act = () => { var x = id[0]; };
//            act.Should().Throw<IndexOutOfRangeException>();
//            act = () => { var x = id[1]; };
//            act.Should().Throw<IndexOutOfRangeException>();
//        }

//        [Fact]
//        public void ConstructTitleNull()
//        {
//            Action act = () => new SemanticVersionMeta(null, null);
//            act.Should().Throw<ArgumentNullException>();
//        }

//        [Fact]
//        public void ConstructTextNull()
//        {
//            string title = "test";
//            int count = 0;
//            bool exists = false;
//            var id = new SemanticVersionMeta(title, null);
//            id.Title.Should().Be(title);
//            id.Input.Should().Be(null);
//            id.ToString().Should().Be(null);
//            id.Identifiers.Should().BeNull();
//            ((IEnumerable)id).GetEnumerator().Should().BeNull();
//            id.GetEnumerator().Should().BeNull();
//            id.Count.Should().Be(count);
//            id.Exists.Should().Be(exists);
//            id.Length.Should().Be(0);

//            Action act;
//            act = () => { var x = id[-1]; };
//            act.Should().Throw<IndexOutOfRangeException>();
//            act = () => { var x = id[0]; };
//            act.Should().Throw<IndexOutOfRangeException>();
//            act = () => { var x = id[1]; };
//            act.Should().Throw<IndexOutOfRangeException>();
//        }

//        [Fact]
//        public void ConstructTextEmpty()
//        {
//            string title = "test";
//            int count = 1;
//            bool exists = true;
//            var id = new SemanticVersionMeta(title, string.Empty);
//            id.Title.Should().Be(title);
//            id.Input.Should().Be(string.Empty);
//            id.ToString().Should().Be(string.Empty);
//            id.Identifiers.Should().NotBeNull().And.HaveCount(count);
//            ((IEnumerable)id).Should().HaveCount(count);
//            ((IEnumerable<SemanticVersionIdentifier>)id).Should().HaveCount(count);
//            id.Count.Should().Be(count);
//            id.Exists.Should().Be(exists);
//            id.Length.Should().Be(0);

//            Action act;
//            act = () => { var x = id[-1]; };
//            act.Should().Throw<IndexOutOfRangeException>();
//            act = () => { var x = id[0]; };
//            act.Should().NotThrow();
//            act = () => { var x = id[1]; };
//            act.Should().Throw<IndexOutOfRangeException>();
//        }

//        [Theory]
//        [InlineData("test", "0", 1)]
//        [InlineData("test", "0.0", 2)]
//        [InlineData("test", "0.0.0", 3)]
//        [InlineData("test", "alpha.beta", 2)]
//        [InlineData("test", "alpha.beta.0", 3)]
//        [InlineData("test", "0.0-0.0", 3)]
//        [InlineData("test", "0-0", 1)]
//        public void ConstructText(string title, string text, int count)
//        {
//            var id = new SemanticVersionMeta(title, text);
//            id.Title.Should().Be(title);
//            id.Input.Should().Be(text);
//            id.ToString().Should().Be(text);
//            id.Identifiers.Should().NotBeNull().And.HaveCount(count);
//            ((IEnumerable)id).Should().HaveCount(count);
//            ((IEnumerable<SemanticVersionIdentifier>)id).Should().HaveCount(count);
//            id.Count.Should().Be(count);
//            id.Exists.Should().Be(true);
//            id.Length.Should().Be(text.Length);

//            id[0].Text.Should().Be(text.Split('.', 2)[0]);
//            id[id.Count - 1].Text.Should().Be(text.Split('.')[id.Count - 1]);

//            Action act;
//            act = () => { var x = id[-1]; };
//            act.Should().Throw<IndexOutOfRangeException>();
//            act = () => { var x = id[id.Count]; };
//            act.Should().Throw<IndexOutOfRangeException>();
//        }

//        [Theory]
//        [InlineData("test", "0", "0", 0, SemanticVersionMeta.EPrecedence.PreRelease)]
//        [InlineData("test", "1", "0", 1, SemanticVersionMeta.EPrecedence.PreRelease)]
//        [InlineData("test", "0", "1", -1, SemanticVersionMeta.EPrecedence.PreRelease)]
//        [InlineData("test", "a", "9", 1, SemanticVersionMeta.EPrecedence.PreRelease)]
//        [InlineData("test", "9", "a", -1, SemanticVersionMeta.EPrecedence.PreRelease)]

//        [InlineData("test", "0.0", "0.0", 0, SemanticVersionMeta.EPrecedence.PreRelease)]
//        [InlineData("test", "0.1", "0.0", 1, SemanticVersionMeta.EPrecedence.PreRelease)]
//        [InlineData("test", "0.0", "0.1", -1, SemanticVersionMeta.EPrecedence.PreRelease)]
//        [InlineData("test", "0.0", "0", 1, SemanticVersionMeta.EPrecedence.PreRelease)]
//        [InlineData("test", "0", "0.0", -1, SemanticVersionMeta.EPrecedence.PreRelease)]
//        [InlineData("test", "1.0", "0", 1, SemanticVersionMeta.EPrecedence.PreRelease)]
//        [InlineData("test", "0", "1.0", -1, SemanticVersionMeta.EPrecedence.PreRelease)]

//        [InlineData("test", null, null, 0, SemanticVersionMeta.EPrecedence.PreRelease)]
//        [InlineData("test", null, "0", 1, SemanticVersionMeta.EPrecedence.PreRelease)]
//        [InlineData("test", "0", null, -1, SemanticVersionMeta.EPrecedence.PreRelease)]

//        [InlineData("test", null, null, 0, SemanticVersionMeta.EPrecedence.PostRelease)]
//        [InlineData("test", null, "0", -1, SemanticVersionMeta.EPrecedence.PostRelease)]
//        [InlineData("test", "0", null, 1, SemanticVersionMeta.EPrecedence.PostRelease)]
//        public void Compare(string title, string leftText, string rightText, int expected, SemanticVersionMeta.EPrecedence precedence)
//        {
//            var left = new SemanticVersionMeta(title, leftText, precedence);
//            var right = new SemanticVersionMeta(title, rightText, precedence);

//            Action act;

//            // left equals right
//            SemanticVersionMeta.Equate(left, right).Should().Be(expected == 0);
//            left.Equals(right).Should().Be(expected == 0);
//            left.Equals((object)right).Should().Be(expected == 0);
//            left.Equals(new object()).Should().BeFalse();
//            (left == right).Should().Be(expected == 0);
//            (left != right).Should().Be(expected != 0);
//            if (expected == 0)
//            { left.GetHashCode().Should().Be(right.GetHashCode()); }

//            // left compare right
//            SemanticVersionMeta.Compare(left, right).Should().Be(expected);
//            left.CompareTo(right).Should().Be(expected);
//            left.CompareTo((object)right).Should().Be(expected);
//            act = () => left.CompareTo(new object());
//            act.Should().Throw<ArgumentException>();
//            (left < right).Should().Be(expected == -1);
//            (left <= right).Should().Be(expected <= 0);
//            (left > right).Should().Be(expected == 1);
//            (left >= right).Should().Be(expected >= 0);

//            // right equals left
//            SemanticVersionMeta.Equate(right, left).Should().Be((expected * -1) == 0);
//            right.Equals(left).Should().Be((expected * -1) == 0);
//            right.Equals((object)left).Should().Be((expected * -1) == 0);
//            right.Equals(new object()).Should().BeFalse();
//            (right == left).Should().Be((expected * -1) == 0);
//            (right != left).Should().Be((expected * -1) != 0);
//            if (expected == 0)
//            { right.GetHashCode().Should().Be(left.GetHashCode()); }

//            // right compare left
//            SemanticVersionMeta.Compare(right, left).Should().Be(expected * -1);
//            right.CompareTo(left).Should().Be(expected * -1);
//            right.CompareTo((object)left).Should().Be(expected * -1);
//            act = () => right.CompareTo(new object());
//            act.Should().Throw<ArgumentException>();
//            (right < left).Should().Be((expected * -1) == -1);
//            (right <= left).Should().Be((expected * -1) <= 0);
//            (right > left).Should().Be((expected * -1) == 1);
//            (right >= left).Should().Be((expected * -1) >= 0);
//        }

//        [Theory]
//        [InlineData("test", "", "pre.0")]
//        [InlineData("test", "pre", "pre.0")]
//        [InlineData("test", "pre.0", "pre.1")]
//        [InlineData("test", "pre.0.alpha", "pre.1.alpha")]
//        [InlineData("test", "pre.0.0.alpha.0.0.exp", "pre.0.0.alpha.0.1.exp")]
//        [InlineData("test", "pre.0.0.alpha.0.0.exp.0", "pre.0.0.alpha.0.0.exp.1")]
//        [InlineData("test", "-1", "-1.0")]
//        public void BumpExistent(string title, string originalText, string expectedText)
//        {
//            var original = new SemanticVersionMeta(title, originalText);
//            var expected = new SemanticVersionMeta(title, expectedText);
//            (original < expected).Should().BeTrue();

//            var bumped = original.GetBumped("pre");
//            (bumped == expected).Should().BeTrue();
//            (original < bumped).Should().BeTrue();
//        }

//        [Theory]
//        [InlineData("test", null, "pre.0", SemanticVersionMeta.EPrecedence.PreRelease)]
//        [InlineData("test", null, "pre.0", SemanticVersionMeta.EPrecedence.PostRelease)]
//        public void BumpNonExistent(string title, string originalText, string expectedText, SemanticVersionMeta.EPrecedence precedence)
//        {
//            var original = new SemanticVersionMeta(title, originalText, precedence);
//            var expected = new SemanticVersionMeta(title, expectedText, precedence);
//            SemanticVersionMeta.Compare(original, expected).Should().Be(-1 * (int)precedence);

//            var bumped = original.GetBumped("pre");
//            (bumped == expected).Should().BeTrue();
//            SemanticVersionMeta.Compare(original, bumped).Should().Be(-1 * (int)precedence);
//        }
//    }
//}
