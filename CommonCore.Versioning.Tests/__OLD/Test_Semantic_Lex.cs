﻿//#region Copyright (c) 2022 Jay Jeckel
//// Copyright (c) 2022 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//#nullable enable

//using System.Diagnostics.CodeAnalysis;
//using FluentAssertions;
//using Xunit;

//namespace CommonCore.Versioning.Tests
//{
//    [ExcludeFromCodeCoverage]
//    public class Test_Semantic_Lex
//    {

//        [Theory]
//        [InlineData(0, "", "")]
//        [InlineData(-1, "", "0")]
//        [InlineData(0, "0", "0")]
//        [InlineData(-1, "0", "a")]
//        [InlineData(0, "a", "a")]
//        public void Compare_Tokens(int expected, string left, string right)
//        {
//            int expectInverted = expected == 0 ? 0 : expected * -1;
//            SemanticTokenComparer.Default.Compare(left, left).Should().Be(0);
//            SemanticTokenComparer.Default.Compare(right, right).Should().Be(0);
//            SemanticTokenComparer.Default.Compare(left, right).Should().Be(expected);
//            SemanticTokenComparer.Default.Compare(right, left).Should().Be(expectInverted);
//        }

//        [Theory]
//        [InlineData(0, new string[0], new string[0])]
//        [InlineData(-1, new string[0], new[] { "" })]
//        [InlineData(0, new[] { "" }, new[] { "" })]
//        [InlineData(-1, new[] { "" }, new[] { "", "" })]
//        [InlineData(0, new[] { "a" }, new[] { "a" })]
//        [InlineData(-1, new[] { "a" }, new[] { "a", "b" })]
//        [InlineData(1, new[] { "x" }, new[] { "a", "b" })]
//        public void Compare_Sets(int expected, string[] left, string[] right)
//        {
//            int expectInverted = expected == 0 ? 0 : expected * -1;
//            SemanticSectionComparer.Default.Compare(left, left, false).Should().Be(0);
//            SemanticSectionComparer.Default.Compare(right, right, false).Should().Be(0);
//            SemanticSectionComparer.Default.Compare(left, right, false).Should().Be(expected);
//            SemanticSectionComparer.Default.Compare(right, left, false).Should().Be(expectInverted);

//            if (expected != 0 && (left is null || left.Length == 0 || right is null || right.Length == 0))
//            {
//                expected = -expected;
//                expectInverted = -expectInverted;
//            }
//            SemanticSectionComparer.Default.Compare(left, left, true).Should().Be(0);
//            SemanticSectionComparer.Default.Compare(right, right, true).Should().Be(0);
//            SemanticSectionComparer.Default.Compare(left, right, true).Should().Be(expected);
//            SemanticSectionComparer.Default.Compare(right, left, true).Should().Be(expectInverted);
//        }

//        [Theory]
//        [InlineData(0, null, null)]
//        [InlineData(-1, null, "0.0.0")]
//        [InlineData(0, "0.0.0-pre.0", "0.0.0-pre.0")]
//        [InlineData(0, "0.0.0+build.0", "0.0.0+build.0")]
//        [InlineData(0, "0.0.0-pre.0+build.0", "0.0.0-pre.0+build.0")]
//        [InlineData(-1, "0.0.0", "1.0.0")]
//        [InlineData(-1, "0.0.0", "0.1.0")]
//        [InlineData(-1, "0.0.0", "0.0.1")]
//        [InlineData(-1, "0.0.2", "0.1.0")]
//        [InlineData(-1, "0.2.0", "1.0.0")]
//        [InlineData(-1, "0.0.0-pre", "0.0.0")]
//        [InlineData(-1, "0.0.0-pre", "0.0.0-prer")]
//        [InlineData(-1, "0.0.0-pre", "0.0.0-pre0")]
//        [InlineData(-1, "0.0.0-pre0", "0.0.0-pre1")]
//        [InlineData(-1, "0.0.0-pre0", "0.0.0-pre10")]
//        [InlineData(-1, "0.0.0-pre00", "0.0.0-pre1")]
//        [InlineData(-1, "0.0.0-pre00", "0.0.0-pre10")]
//        [InlineData(-1, "0.0.0-pre", "0.0.0-pre.alpha")]
//        [InlineData(-1, "0.0.0-pre", "0.0.0-pre.0")]
//        [InlineData(-1, "0.0.0-pre.0", "0.0.0-pre.alpha")]
//        [InlineData(-1, "0.0.0-pre.alpha", "0.0.0-pre.beta")]
//        [InlineData(-1, "0.0.0-beta.11", "0.0.0-rc.1")]
//        [InlineData(-1, "0.0.0-pre.0", "0.0.0-pre.0+build.0")]
//        [InlineData(-1, "0.0.0", "0.0.0+test-deploy.0")]
//        public void Compare_Models(int expected, string leftSubject, string rightSubject)
//        {
//            Semantic.Scan(leftSubject).Parse(out var leftCore, out var leftCycle, out var leftBuild);
//            //if (left != null) { Semantic.Lex.Validate(left).Should().BeEmpty(); }
//            Semantic.Scan(rightSubject).Parse(out var rightCore, out var rightCycle, out var rightBuild);
//            //if (right != null) { Semantic.Lex.Validate(right).Should().BeEmpty(); }

//            int expectInverted = expected == 0 ? 0 : expected * -1;
//            SemanticVersionComparer.Default.Compare(leftCore, leftCycle, leftBuild, leftCore, leftCycle, leftBuild).Should().Be(0);
//            SemanticVersionComparer.Default.Compare(rightCore, rightCycle, rightBuild, rightCore, rightCycle, rightBuild).Should().Be(0);
//            SemanticVersionComparer.Default.Compare(leftCore, leftCycle, leftBuild, rightCore, rightCycle, rightBuild).Should().Be(expected);
//            SemanticVersionComparer.Default.Compare(rightCore, rightCycle, rightBuild, leftCore, leftCycle, leftBuild).Should().Be(expectInverted);
//        }

//        [Theory]
//        [InlineData("", new string[0])]
//        [InlineData("0.0", new[] { "0", "0" })]
//        public void Join_Set(string expected, string[] subject)
//            => SemanticVersionFormatter.Default.Join(subject).Should().Be(expected);

//        [Theory]
//        [InlineData("", new string[0], null, null)]
//        [InlineData("-", new string[0], new string[0], null)]
//        [InlineData("+", new string[0], null, new string[0])]
//        [InlineData("-+", new string[0], new string[0], new string[0])]
//        [InlineData("0.0.0", new[] { "0", "0", "0" }, null, null)]
//        [InlineData("0.0.0-pre.0", new[] { "0", "0", "0" }, new[] { "pre", "0" }, null)]
//        [InlineData("0.0.0-pre.0+build.0", new[] { "0", "0", "0" }, new[] { "pre", "0" }, new[] { "build", "0" })]
//        [InlineData("0.0.0+build.0", new[] { "0", "0", "0" }, null, new[] { "build", "0" })]
//        public void Join_Model(string expected, string[] core, string[] cycle, string[] build)
//            => SemanticVersionFormatter.Default.Join(core, cycle, build).Should().Be(expected);

//        [Theory]
//        [MemberData(nameof(TestInfo.GetValidVersionStrings), MemberType = typeof(TestInfo))]
//        [MemberData(nameof(TestInfo.GetInvalidVersionStrings), MemberType = typeof(TestInfo))]
//        public void Join_Model_General(string subject)
//        {
//            Semantic.Scan(subject).Parse(out var core, out var cycle, out var build);
//            SemanticVersionFormatter.Default.Join(core, cycle, build).Should().Be(subject);
//        }

//        [Theory]
//        [InlineData(-1, new string[0])]
//        [InlineData(-1, new[] { "test", "test" })]
//        [InlineData(1, new[] { "0", "0" })]
//        [InlineData(0, new[] { "0", "test" })]
//        [InlineData(2, new[] { "0", "test", "0" })]
//        public void FindBumpIndex(int expected, string[] subject)
//            => SemanticVersionFormatter.Default.FindBumpIndex(subject).Should().Be(expected);
//    }
//}
