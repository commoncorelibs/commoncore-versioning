﻿//using System;
//using System.Diagnostics.CodeAnalysis;
//using FluentAssertions;
//using Xunit;

//namespace CommonCore.Versioning.Tests
//{
//    [ExcludeFromCodeCoverage]
//    public class Test_SemanticRule
//    {
//        [Fact]
//        public void Constructor_Throw()
//        {
//            Action act;

//            act = () => new SemanticRule<string>(null, null, null, null);
//            act.Should().Throw<ArgumentNullException>();

//            act = () => new SemanticRule<string>("", null, null, null);
//            act.Should().Throw<ArgumentNullException>();

//            act = () => new SemanticRule<string>("", "", null, null);
//            act.Should().Throw<ArgumentNullException>();

//            act = () => new SemanticRule<string>("", "", x => true, null);
//            act.Should().NotThrow<ArgumentNullException>();

//            act = () => new SemanticRule<string>("", "", x => true, x => string.Empty);
//            act.Should().NotThrow<ArgumentNullException>();
//        }
//    }
//}
