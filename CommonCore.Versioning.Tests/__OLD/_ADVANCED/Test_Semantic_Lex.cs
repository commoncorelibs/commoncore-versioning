﻿//using System;
//using System.Collections.Generic;
//using System.Diagnostics.CodeAnalysis;
//using FluentAssertions;
//using Xunit;

//namespace CommonCore.Versioning.Tests
//{
//    [ExcludeFromCodeCoverage]
//    public class Test_Semantic_Lex
//    {
//        public static IEnumerable<object[]> GetData_Validate(bool valid) => TestInfo.GetData_Validate(valid);

//        [Fact]
//        public void Constants()
//        {
//            Semantic.Lex.SemanticSetCount.Should().Be(3);
//            Semantic.Lex.CoreIndex.Should().Be(new Index(0));
//            Semantic.Lex.CycleIndex.Should().Be(new Index(1));
//            Semantic.Lex.BuildIndex.Should().Be(new Index(2));
//            Semantic.Lex.CycleSymbol.Should().Be('-');
//            Semantic.Lex.BuildSymbol.Should().Be('+');
//            Semantic.Lex.DelimitSymbol.Should().Be('.');
//            Semantic.Lex.DefaultToken.Should().Be("0");
//        }

//        [Theory]
//        [InlineData(1, "1")]
//        [InlineData(0, "1,000")]
//        [InlineData(0, "1.1")]
//        [InlineData(0, "abcd")]
//        public void ParseNumber(int expected, string subject)
//            => Semantic.Lex.ParseNumber(subject, 0).Should().Be(expected);

//        [Theory]
//        [InlineData(0, "", "")]
//        [InlineData(-1, "", "0")]
//        [InlineData(0, "0", "0")]
//        [InlineData(-1, "0", "a")]
//        [InlineData(0, "a", "a")]
//        public void Compare_Tokens(int expected, string left, string right)
//        {
//            int expectInverted = expected == 0 ? 0 : expected * -1;
//            Semantic.Lex.Compare(left, left).Should().Be(0);
//            Semantic.Lex.Compare(right, right).Should().Be(0);
//            Semantic.Lex.Compare(left, right).Should().Be(expected);
//            Semantic.Lex.Compare(right, left).Should().Be(expectInverted);
//        }

//        [Theory]
//        [InlineData(0, null, null)]
//        [InlineData(-1, null, new string[0])]
//        [InlineData(-1, null, new[] { "" })]
//        [InlineData(0, new string[0], new string[0])]
//        [InlineData(-1, new string[0], new[] { "" })]
//        [InlineData(0, new[] { "" }, new[] { "" })]
//        [InlineData(-1, new[] { "" }, new[] { "", "" })]
//        [InlineData(0, new[] { "a" }, new[] { "a" })]
//        [InlineData(-1, new[] { "a" }, new[] { "a", "b" })]
//        [InlineData(1, new[] { "x" }, new[] { "a", "b" })]
//        public void Compare_Sets(int expected, string[] left, string[] right)
//        {
//            int expectInverted = expected == 0 ? 0 : expected * -1;
//            Semantic.Lex.Compare(left, left, false).Should().Be(0);
//            Semantic.Lex.Compare(right, right, false).Should().Be(0);
//            Semantic.Lex.Compare(left, right, false).Should().Be(expected);
//            Semantic.Lex.Compare(right, left, false).Should().Be(expectInverted);

//            if (expected != 0 && (left is null || left.Length == 0 || right is null || right.Length == 0))
//            {
//                expected = -expected;
//                expectInverted = -expectInverted;
//            }
//            Semantic.Lex.Compare(left, left, true).Should().Be(0);
//            Semantic.Lex.Compare(right, right, true).Should().Be(0);
//            Semantic.Lex.Compare(left, right, true).Should().Be(expected);
//            Semantic.Lex.Compare(right, left, true).Should().Be(expectInverted);
//        }

//        [Theory]
//        [InlineData(0, null, null)]
//        [InlineData(-1, null, "0.0.0")]
//        [InlineData(0, "0.0.0-pre.0", "0.0.0-pre.0")]
//        [InlineData(0, "0.0.0+build.0", "0.0.0+build.0")]
//        [InlineData(0, "0.0.0-pre.0+build.0", "0.0.0-pre.0+build.0")]
//        [InlineData(-1, "0.0.0", "1.0.0")]
//        [InlineData(-1, "0.0.0", "0.1.0")]
//        [InlineData(-1, "0.0.0", "0.0.1")]
//        [InlineData(-1, "0.0.2", "0.1.0")]
//        [InlineData(-1, "0.2.0", "1.0.0")]
//        [InlineData(-1, "0.0.0-pre", "0.0.0")]
//        [InlineData(-1, "0.0.0-pre", "0.0.0-prer")]
//        [InlineData(-1, "0.0.0-pre", "0.0.0-pre0")]
//        [InlineData(-1, "0.0.0-pre0", "0.0.0-pre1")]
//        [InlineData(-1, "0.0.0-pre0", "0.0.0-pre10")]
//        [InlineData(-1, "0.0.0-pre00", "0.0.0-pre1")]
//        [InlineData(-1, "0.0.0-pre00", "0.0.0-pre10")]
//        [InlineData(-1, "0.0.0-pre", "0.0.0-pre.alpha")]
//        [InlineData(-1, "0.0.0-pre", "0.0.0-pre.0")]
//        [InlineData(-1, "0.0.0-pre.0", "0.0.0-pre.alpha")]
//        [InlineData(-1, "0.0.0-pre.alpha", "0.0.0-pre.beta")]
//        [InlineData(-1, "0.0.0-beta.11", "0.0.0-rc.1")]
//        [InlineData(-1, "0.0.0-pre.0", "0.0.0-pre.0+build.0")]
//        [InlineData(-1, "0.0.0", "0.0.0+test-deploy.0")]
//        public void Compare_Models(int expected, string subjectLeft, string subjectRight)
//        {
//            var left = Semantic.Lex.Scan(subjectLeft);
//            if (left != null) { Semantic.Lex.Validate(left).Should().BeEmpty(); }
//            var right = Semantic.Lex.Scan(subjectRight);
//            if (right != null) { Semantic.Lex.Validate(right).Should().BeEmpty(); }

//            int expectInverted = expected == 0 ? 0 : expected * -1;
//            Semantic.Lex.Compare(left, left).Should().Be(0);
//            Semantic.Lex.Compare(right, right).Should().Be(0);
//            Semantic.Lex.Compare(left, right).Should().Be(expected);
//            Semantic.Lex.Compare(right, left).Should().Be(expectInverted);
//        }

//        [Theory]
//        [InlineData("", null)]
//        [InlineData("", new string[0])]
//        [InlineData("0.0", new[] { "0", "0" })]
//        public void Join_Set(string expected, string[] subject)
//            => Semantic.Lex.Join(subject).Should().Be(expected);

//        [Fact]
//        public void Join_Model_Empty()
//        {
//            Semantic.Lex.Join((string[][])null).Should().BeEmpty();
//            Semantic.Lex.Join(new string[0][]).Should().BeEmpty();

//            Semantic.Lex.Join(new string[][] { new[] { "0", "0" } }).Should().Be("0.0");
//        }

//        [Theory]
//        [MemberData(nameof(GetData_Validate), true)]
//        [MemberData(nameof(GetData_Validate), false)]
//        public void Join_Model_General(string subject)
//        {
//            var model = Semantic.Lex.Scan(subject);
//            Semantic.Lex.Join(model).Should().Be(subject);
//        }

//        [Fact]
//        public void Validate_Throw()
//        {
//            Action act;

//            act = () => Semantic.Lex.Validate(null);
//            act.Should().Throw<ArgumentNullException>();

//            act = () => Semantic.Lex.Validate(null, new List<Exception>());
//            act.Should().Throw<ArgumentNullException>();

//            act = () => Semantic.Lex.Validate(new string[3][], null);
//            act.Should().Throw<ArgumentNullException>();
//        }

//        [Theory]
//        [MemberData(nameof(GetData_Validate), true)]
//        public void Validate_Valid(string subject)
//        {
//            var data = Semantic.Lex.Scan(subject);
//            var errors = new List<Exception>();
//            Semantic.Lex.Validate(data, errors).Should().BeTrue();
//            errors.Should().BeEmpty();
//            Semantic.Lex.Validate(data).Should().BeEmpty();
//        }

//        [Theory]
//        [MemberData(nameof(GetData_Validate), false)]
//        public void Validate_Invalid(string subject)
//        {
//            var data = Semantic.Lex.Scan(subject);
//            var errors = new List<Exception>();
//            Semantic.Lex.Validate(data, errors).Should().BeFalse();
//            errors.Should().NotBeEmpty();
//            Semantic.Lex.Validate(data).Should().NotBeEmpty();
//        }

//        [Theory]
//        [InlineData(-1, null)]
//        [InlineData(-1, new string[0])]
//        [InlineData(-1, new[] { "test", "test" })]
//        [InlineData(1, new[] { "0", "0" })]
//        [InlineData(0, new[] { "0", "test" })]
//        public void GetBumpIndex(int expected, string[] subject)
//            => Semantic.Lex.GetBumpIndex(subject).Should().Be(expected == -1 ? Index.End : new Index(expected));
//    }
//}
