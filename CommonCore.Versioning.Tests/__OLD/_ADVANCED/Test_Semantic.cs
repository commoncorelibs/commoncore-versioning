﻿//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Diagnostics.CodeAnalysis;
//using FluentAssertions;
//using Xunit;
//using System.Linq;

//namespace CommonCore.Versioning.Tests
//{
//    [ExcludeFromCodeCoverage]
//    public class Test_Semantic
//    {
//        [Fact]
//        public void Parse()
//        {
//            (SemanticModel model, List<Exception> errors) = Semantic.Parse("0.0.0");
//            model.Should().NotBeNull();
//        }

//        [Theory]
//        [InlineData("#", new string[0])]
//        [InlineData("#", new[] { "" })]
//        [InlineData("#.#.#", new[] { "", "", "" })]
//        [InlineData("#.^._", new[] { "", "^", "" })]
//        public void NormalizeBumpSet(string expected, string[] subject)
//        {
//            var set = subject.ToList();
//            Semantic.Bumper.Normalize(set);
//            string.Join(Semantic.Lex.DelimitSymbol, set).Should().Be(expected);
//        }

//        [Theory]
//        [InlineData("#.#.#", "", 3)]
//        [InlineData("#.#.#", ".", 3)]
//        [InlineData("#.^._", ".^.", 3)]
//        public void NormalizeBumpSetCount(string expected, string subject, int length)
//        {
//            var set = subject.Split(Semantic.Lex.DelimitSymbol).ToList();
//            Semantic.Bumper.Normalize(set);
//            //Semantic.Bumper.NormalizeBumpSetCount(set, length);
//            string.Join(Semantic.Lex.DelimitSymbol, set).Should().Be(expected);
//        }
//    }
//}
