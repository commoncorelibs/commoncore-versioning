﻿//using System;
//using System.Collections.Generic;
//using System.Diagnostics.CodeAnalysis;
//using FluentAssertions;
//using Xunit;

//namespace CommonCore.Versioning.Tests
//{
//    [ExcludeFromCodeCoverage]
//    public class Test_Semantic_Op
//    {
//        public static IEnumerable<object[]> GetData_Validate(bool valid) => TestInfo.GetData_Validate(valid);

//        [Fact]
//        public void Bump_Throw()
//        {
//            Action act = () => Semantic.Op.Increment(null, Index.End);
//            act.Should().Throw<ArgumentNullException>();
//        }

//        [Fact]
//        public void Bump()
//        {
//            var tokens = new List<string>();

//            Semantic.Op.Increment(tokens, Index.End);
//            tokens.Should().HaveCount(1);
//            tokens[0].Should().Be(Semantic.Lex.DefaultToken);

//            Semantic.Op.Increment(tokens, 0);
//            tokens.Should().HaveCount(1);
//            tokens[0].Should().Be("1");
//        }

//        [Fact]
//        public void Reset_Throw()
//        {
//            Action act;

//            act = () => Semantic.Op.Reset(null, Index.End, 1);
//            act.Should().Throw<ArgumentNullException>();

//            act = () => Semantic.Op.Reset(new List<string>(), Index.End, 1);
//            act.Should().Throw<ArgumentOutOfRangeException>();

//            act = () => Semantic.Op.Reset(new List<string>(), 0, -1);
//            act.Should().Throw<ArgumentOutOfRangeException>();
//        }

//        [Theory]
//        [InlineData("test", "test", 1)]
//        [InlineData("test", "test", 0)]
//        [InlineData("test", "test", 2)]

//        [InlineData("0", "2", 1)]
//        [InlineData("0", "2", 0)]
//        [InlineData("00", "2", 2)]

//        [InlineData("0", "005", 1)]
//        [InlineData("000", "005", 0)]
//        [InlineData("00", "005", 2)]
//        public void Reset(string expected, string subject, int resetLength = 1)
//        {
//            var tokens = new List<string>() { subject };
//            Semantic.Op.Reset(tokens, 0, resetLength);
//            tokens[0].Should().Be(expected);
//        }
//    }
//}
