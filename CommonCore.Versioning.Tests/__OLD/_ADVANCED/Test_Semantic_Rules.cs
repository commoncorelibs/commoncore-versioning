﻿//using System;
//using System.Collections.Generic;
//using FluentAssertions;
//using Xunit;

//namespace CommonCore.Versioning.Tests
//{
//    public class Test_Semantic_Rules
//    {
//        [Fact]
//        public void CoreExistModelRule()
//        {
//            var errors = new List<Exception>();
//            Semantic.Rules.CoreExistModelRule.Validate(errors, "test", null).Should().BeFalse();
//            Semantic.Rules.CoreExistModelRule.Validate(errors, "test", new string[0][]).Should().BeFalse();
//            Semantic.Rules.CoreExistModelRule.Validate(errors, "test", new string[][] { null, null, null }).Should().BeFalse();
//            Semantic.Rules.CoreExistModelRule.Validate(errors, "test", new string[][] { new string[0], null, null }).Should().BeTrue();
//        }

//        [Fact]
//        public void MaxCountModelRule()
//        {
//            var errors = new List<Exception>();
//            Semantic.Rules.MaxCountModelRule.Validate(errors, "test", null).Should().BeTrue();
//            Semantic.Rules.MaxCountModelRule.Validate(errors, "test", new string[0][]).Should().BeTrue();
//            Semantic.Rules.MaxCountModelRule.Validate(errors, "test", new string[][] { null }).Should().BeTrue();
//            Semantic.Rules.MaxCountModelRule.Validate(errors, "test", new string[][] { null, null }).Should().BeTrue();
//            Semantic.Rules.MaxCountModelRule.Validate(errors, "test", new string[][] { null, null, null }).Should().BeTrue();
//            Semantic.Rules.MaxCountModelRule.Validate(errors, "test", new string[][] { null, null, null, null }).Should().BeFalse();
//        }

//        [Theory]
//        [InlineData(false, null)]
//        [InlineData(false, new string[0])]
//        [InlineData(true, new[] { "" })]
//        public void NotEmptySetRule(bool expected, string[] subject)
//        {
//            var errors = new List<Exception>();
//            Semantic.Rules.NotEmptySetRule.Validate(errors, "test", subject).Should().Be(expected);
//        }

//        [Theory]
//        [InlineData(false, null)]
//        [InlineData(false, new string[0])]
//        [InlineData(false, new[] { "", "" })]
//        [InlineData(true, new[] { "", "", "" })]
//        [InlineData(true, new[] { "", "", "", "" })]
//        public void CoreMinCountSetRule(bool expected, string[] subject)
//        {
//            var errors = new List<Exception>();
//            Semantic.Rules.CoreMinCountSetRule.Validate(errors, "test", subject).Should().Be(expected);
//        }

//        [Theory]
//        [InlineData(true, null)]
//        [InlineData(true, new string[0])]
//        [InlineData(true, new[] { "", "" })]
//        [InlineData(true, new[] { "", "", "" })]
//        [InlineData(false, new[] { "", "", "", "" })]
//        public void CoreMaxCountSetRule(bool expected, string[] subject)
//        {
//            var errors = new List<Exception>();
//            Semantic.Rules.CoreMaxCountSetRule.Validate(errors, "test", subject).Should().Be(expected);
//        }

//        [Theory]
//        [InlineData(false, null)]
//        [InlineData(false, "")]
//        [InlineData(true, "X")]
//        public void NotEmptyRule(bool expected, string subject)
//        {
//            var errors = new List<Exception>();
//            Semantic.Rules.NotEmptyRule.Validate(errors, "test", subject).Should().Be(expected);
//        }

//        [Theory]
//        [InlineData(true, null)]
//        [InlineData(true, "")]
//        [InlineData(true, "0")]
//        [InlineData(false, "0X")]
//        public void NotLeadingZeroRule(bool expected, string subject)
//        {
//            var errors = new List<Exception>();
//            Semantic.Rules.NotLeadingZeroRule.Validate(errors, "test", subject).Should().Be(expected);
//        }

//        [Theory]
//        [InlineData(false, null)]
//        [InlineData(false, "")]
//        [InlineData(false, "0X")]
//        [InlineData(false, "-1")]
//        [InlineData(true, "0")]
//        public void OnlyDigitsRule(bool expected, string subject)
//        {
//            var errors = new List<Exception>();
//            Semantic.Rules.OnlyDigitsRule.Validate(errors, "test", subject).Should().Be(expected);
//        }

//        [Theory]
//        [InlineData(false, null)]
//        [InlineData(false, "")]
//        [InlineData(false, "-1X!")]
//        [InlineData(true, "-1X")]
//        public void OnlyAlphaNumericRule(bool expected, string subject)
//        {
//            var errors = new List<Exception>();
//            Semantic.Rules.OnlyAlphaNumericRule.Validate(errors, "test", subject).Should().Be(expected);
//        }
//    }
//}
