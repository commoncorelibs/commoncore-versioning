﻿//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Diagnostics.CodeAnalysis;
//using FluentAssertions;
//using Xunit;
//using System.Collections;

//namespace CommonCore.Versioning.Tests
//{
//    public class Test_SemanticModel
//    {
//        public void Basics()
//        {
//            (SemanticModel model, List<Exception> errors) = Semantic.Parse("0.0.0");

//            model.Count.Should().Be(Semantic.Lex.SemanticSetCount);

//            model.Core.Should().HaveCount(3);
//            model.Cycle.Should().BeNull();
//            model.Build.Should().BeNull();

//            object.ReferenceEquals(model.Core, model[Semantic.Lex.CoreIndex]).Should().BeTrue();
//            object.ReferenceEquals(model.Cycle, model[Semantic.Lex.CycleIndex]).Should().BeTrue();
//            object.ReferenceEquals(model.Build, model[Semantic.Lex.BuildIndex]).Should().BeTrue();

//            //Action actEnumerable = () =>
//            //{
//            //    foreach (var set in ((IEnumerable)model)) { }
//            //    foreach (var set in model) { }
//            //};
//            //actEnumerable.Should().NotThrow();

//            model[Semantic.Lex.CoreIndex] = null;
//            model.Core.Should().BeNull();

//            model[Semantic.Lex.CycleIndex] = null;
//            model.Cycle.Should().BeNull();

//            model[Semantic.Lex.BuildIndex] = null;
//            model.Build.Should().BeNull();
//        }
//    }
//}
