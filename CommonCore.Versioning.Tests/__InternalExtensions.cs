﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace CommonCore.Versioning.Tests
{
    [ExcludeFromCodeCoverage]
    internal static class __InternalExtensions
    {
        public static IEnumerable<T> ToEnumerable<T>(this IEnumerable<T> self)
            => self ?? Enumerable.Empty<T>();
    }
}
