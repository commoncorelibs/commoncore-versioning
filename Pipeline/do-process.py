#!/usr/bin/env python3


def write_file(path, content):
    import os
    # Validate path (part 1)
    if path.endswith('/'):
        raise Exception('File location may not be a directory.')

    # Get absolute filepath
    path = os.path.abspath(path)
    if not path.lower().endswith('.txt'):
        path += '.txt'

    with open(path, mode='w', encoding='utf-8') as handle:
        handle.write(content)

def parse_args():
    """Parse the command line arguments."""
    import argparse
    import textwrap
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=textwrap.dedent('''Some description.'''))
    parser.add_argument("-t", "--token", type=str, help="The Gitlab private token used for API calls.")
    parser.add_argument("-i", "--pid", type=int, help="The Gitlab project id used for API calls.")
    parser.add_argument("-o", "--output", type=str, help="Path to write the latest version.")
    return parser.parse_args()

def main():
    import gitlab
    from Pipeline import Bumper
    from Pipeline import BumpType
    
    print("Processing...")

    args = parse_args()

    token = args.token
    if token is not None:
        print("SUCCESS: Gitlab token provided.")
    else:
        print("ERROR: Gitlab token must be provided as argument -t <token>.")
        return 1

    pid = args.pid
    if pid is not None:
        print(f"SUCCESS: Gitlab project ID '{pid}' provided.")
    else:
        print("ERROR: Gitlab project ID must be provided as argument -i <pid>.")
        return 1

    output = args.output
    if output is not None:
        print("SUCCESS: Output path provided.")
    else:
        print("ERROR: Output path must be provided as argument -o <path>.")
        return 1
        
    print("Initializing the bumper...")
    bumper = Bumper()
    verbose = False
    print("Verbose Output: " + ("Enabled" if verbose else "Disabled"))
    if verbose:
        print("Done!")
        
    if verbose:
        print("Initializing Gitlab API and locating project...")
    lab = gitlab.Gitlab('https://gitlab.com/', private_token=token)
    project = lab.projects.get(pid)
    if verbose:
        print("SUCCESS: Done!")
    else:
        print("SUCCESS: Initializing Gitlab API and locating project.")

    commits = project.commits.list(order_by='created_at', sort='desc')
    if commits is None or len(commits) == 0:
        print("ERROR: No commits found.")
        return 1

    commit = commits[0]
    tags = commit.refs('tag')
    if tags is not None and len(tags) > 0:
        print("ERROR: Commit is already tagged.")
        return 1

    versions = [o.name for o in project.tags.list(all=True)]
    versions = bumper.SortSemver(versions, False)
    if versions is None or len(versions) == 0:
        latest = "0.0.0"
    else:
        latest = bumper.GetTagLatest(versions, False)
    print(f"Latest version '{latest}' found.")


    bumpType = bumper.GetMessageBumpType(commit.message)
    print(f"Bump type '{bumpType.name if bumpType is not None else None}' found.")
    if bumpType is None:
        version = latest
        version = bumper.BumpVersion(version, BumpType.Pre)
    else:
        version = bumper.BumpVersion(latest, bumpType)
        print(f"Version bumped from '{latest}' to '{version}'.")
        print(f"Tagging commit with '{version}'.")
        bumper.TagRepo(version)
    print(f"Writing version '{version}' to '{output}'.")
    write_file(output, version)
    return 0


if __name__ == "__main__":
    import sys
    try:
        sys.exit(main())
    except SystemExit:
        pass
    else:
        pass
