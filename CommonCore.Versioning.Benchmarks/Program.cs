﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Linq;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;
using BenchmarkDotNet.Running;

namespace CommonCore.Versioning.Benchmarks
{
    /*
|          Method |     Mean |     Error |    StdDev |      Min |      Max | Ratio | RatioSD |     Gen 0 | Allocated |
|---------------- |---------:|----------:|----------:|---------:|---------:|------:|--------:|----------:|----------:|
|    IntParse_ROS | 3.445 ms | 0.0363 ms | 0.0303 ms | 3.396 ms | 3.507 ms |  1.00 |    0.00 |  761.7188 |      3 MB |
|    IntParse_STR | 3.198 ms | 0.0608 ms | 0.0475 ms | 3.143 ms | 3.289 ms |  0.93 |    0.02 |  761.7188 |      3 MB |
|        Plus_ROS | 4.074 ms | 0.0793 ms | 0.0814 ms | 3.942 ms | 4.211 ms |  1.18 |    0.02 | 2156.2500 |      9 MB |
|        Plus_STR | 3.820 ms | 0.0164 ms | 0.0137 ms | 3.796 ms | 3.846 ms |  1.11 |    0.01 | 2160.1563 |      9 MB |
|      Concat_ROS | 5.278 ms | 0.0680 ms | 0.0567 ms | 5.181 ms | 5.343 ms |  1.53 |    0.02 | 2734.3750 |     11 MB |
|      Concat_STR | 4.856 ms | 0.0163 ms | 0.0136 ms | 4.831 ms | 4.872 ms |  1.41 |    0.01 | 2734.3750 |     11 MB |
|     UseSpan_ROS | 2.617 ms | 0.0383 ms | 0.0320 ms | 2.583 ms | 2.668 ms |  0.76 |    0.01 | 1699.2188 |      7 MB |
|     UseSpan_STR | 2.514 ms | 0.0440 ms | 0.0602 ms | 2.465 ms | 2.684 ms |  0.74 |    0.02 | 1699.2188 |      7 MB |
| Create_ROS_Wrap | 3.972 ms | 0.0514 ms | 0.0429 ms | 3.914 ms | 4.022 ms |  1.15 |    0.01 | 3632.8125 |     14 MB |
|      Create_ROS | 3.968 ms | 0.0323 ms | 0.0286 ms | 3.926 ms | 4.033 ms |  1.15 |    0.02 | 3632.8125 |     14 MB |
|      Create_STR | 2.821 ms | 0.0216 ms | 0.0180 ms | 2.794 ms | 2.851 ms |  0.82 |    0.01 | 2867.1875 |     11 MB |
|     Create2_STR | 2.777 ms | 0.0551 ms | 0.0656 ms | 2.722 ms | 2.947 ms |  0.81 |    0.02 | 2867.1875 |     11 MB |
|     Create3_STR | 2.563 ms | 0.0379 ms | 0.0354 ms | 2.499 ms | 2.617 ms |  0.75 |    0.01 | 2867.1875 |     11 MB |
    */
    [SimpleJob(RuntimeMoniker.Net50, baseline: true)]
    [MinColumn, MaxColumn]
    [MemoryDiagnoser]
    public class StringMath
    {
        private static string[] Data;

        static StringMath()
        {
            Data = Enumerable.Range(0, 100_000).Select(index => index.ToString()).ToArray();
        }

        [Benchmark(Baseline = true)]
        public void IntParse_ROS() { foreach (var data in Data) { Utils.Method_IntParse_ROS(data); } }

        [Benchmark]
        public void IntParse_STR() { foreach (var data in Data) { Utils.Method_IntParse_STR(data); } }

        [Benchmark]
        public void Plus_ROS() { foreach (var data in Data) { Utils.Method_Plus_ROS(data); } }

        [Benchmark]
        public void Plus_STR() { foreach (var data in Data) { Utils.Method_Plus_STR(data); } }

        [Benchmark]
        public void Concat_ROS() { foreach (var data in Data) { Utils.Method_Concat_ROS(data); } }

        [Benchmark]
        public void Concat_STR() { foreach (var data in Data) { Utils.Method_Concat_STR(data); } }

        [Benchmark]
        public void UseSpan_ROS() { foreach (var data in Data) { Utils.Method_UseSpan_ROS(data); } }

        [Benchmark]
        public void UseSpan_STR() { foreach (var data in Data) { Utils.Method_UseSpan_STR(data); } }

        [Benchmark]
        public void Create_ROS_Wrap() { foreach (var data in Data) { Utils.Method_Create_ROS_Wrap(data); } }

        [Benchmark]
        public void Create_ROS() { foreach (var data in Data) { Utils.Method_Create_ROS(data); } }

        [Benchmark]
        public void Create_STR() { foreach (var data in Data) { Utils.Method_Create_STR(data); } }

        [Benchmark]
        public void Create2_STR() { foreach (var data in Data) { Utils.Method_Create2_STR(data); } }

        [Benchmark]
        public void Create3_STR() { foreach (var data in Data) { Utils.Method_Create3_STR(data); } }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            //var summary = BenchmarkRunner.Run<Md5VsSha256>();
            var summarys = BenchmarkSwitcher.FromAssembly(typeof(Program).Assembly).Run(args);
        }
    }
}
