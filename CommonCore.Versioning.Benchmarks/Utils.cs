﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CommonCore.Versioning.Benchmarks
{
    public static class Utils
    {
        public static string Method_IntParse_ROS(ReadOnlySpan<char> chars)
        {
            return chars.IsEmpty ? "0" : (int.Parse(chars) + 1).ToString();
        }

        public static string Method_IntParse_STR(string chars)
        {
            return chars is null || chars.Length == 0 ? "0" : (int.Parse(chars) + 1).ToString();
        }

        public static string Method_Plus_ROS(ReadOnlySpan<char> chars)
        {
            if (chars.IsEmpty) { return "0"; }

            int position = -1;
            for (int index = chars.Length - 1; index > -1; index--)
            { if (chars[index] != '9') { position = index; break; } }

            if (position == -1) { return "1" + new string('0', chars.Length); }
            else
            {
                return chars.Slice(0, position).ToString()
                    + (char) (chars[position] + 1)
                    + new string('0', chars.Length - position - 1);
            }
        }

        public static string Method_Plus_STR(string chars)
        {
            if (chars is null || chars.Length == 0) { return "0"; }

            int position = -1;
            for (int index = chars.Length - 1; index > -1; index--)
            { if (chars[index] != '9') { position = index; break; } }

            if (position == -1) { return "1" + new string('0', chars.Length); }
            else
            {
                return chars.Substring(0, position)
                    + (char) (chars[position] + 1)
                    + new string('0', chars.Length - position - 1);
            }
        }

        public static string Method_Concat_ROS(ReadOnlySpan<char> chars)
        {
            if (chars.IsEmpty) { return "0"; }

            int position = -1;
            for (int index = chars.Length - 1; index > -1; index--)
            { if (chars[index] != '9') { position = index; break; } }

            if (position == -1) { return string.Concat("1", new string('0', chars.Length)); }
            else
            {
                return string.Concat(chars.Slice(0, position).ToString(),
                    (char) (chars[position] + 1),
                    new string('0', chars.Length - position - 1));
            }
        }

        public static string Method_Concat_STR(string chars)
        {
            if (chars is null || chars.Length == 0) { return "0"; }

            int position = -1;
            for (int index = chars.Length - 1; index > -1; index--)
            { if (chars[index] != '9') { position = index; break; } }

            if (position == -1) { return string.Concat("1", new string('0', chars.Length)); }
            else
            {
                return string.Concat(chars.Substring(0, position),
                    (char) (chars[position] + 1),
                    new string('0', chars.Length - position - 1));
            }
        }

        public static string Method_UseSpan_ROS(ReadOnlySpan<char> chars)
        {
            if (chars.IsEmpty) { return "0"; }

            int position = -1;
            for (int index = chars.Length - 1; index > -1; index--)
            { if (chars[index] != '9') { position = index; break; } }

            if (position == -1)
            {
                var dest = new Span<char>(new char[chars.Length + 1]);
                dest.Fill('0');
                dest[0] = '1';
                return dest.ToString();
            }
            else
            {
                var dest = new Span<char>(new char[chars.Length]);
                chars.Slice(0, position).CopyTo(dest);
                dest[position] = (char) (chars[position] + 1);
                dest.Slice(position + 1).Fill('0');
                return dest.ToString();
            }
        }

        public static string Method_UseSpan_STR(string chars)
        {
            if (chars is null || chars.Length == 0) { return "0"; }

            int position = -1;
            for (int index = chars.Length - 1; index > -1; index--)
            { if (chars[index] != '9') { position = index; break; } }

            if (position == -1)
            {
                var dest = new Span<char>(new char[chars.Length + 1]);
                dest.Fill('0');
                dest[0] = '1';
                return dest.ToString();
            }
            else
            {
                var dest = new Span<char>(new char[chars.Length]);
                chars.AsSpan().Slice(0, position).CopyTo(dest);
                dest[position] = (char) (chars[position] + 1);
                dest.Slice(position + 1).Fill('0');
                return dest.ToString();
            }
        }

        public static string Method_Create_ROS_Wrap(ReadOnlySpan<char> chars)
            => Method_Create_STR(chars.ToString());

        public static string Method_Create_ROS(ReadOnlySpan<char> chars)
        {
            if (chars.IsEmpty) { return "0"; }

            int position = -1;
            for (int index = chars.Length - 1; index > -1; index--)
            { if (chars[index] != '9') { position = index; break; } }

            if (position == -1)
            {
                return string.Create(chars.Length + 1, 0, (dest, _) => { dest.Fill('0'); dest[0] = '1'; });
            }
            else
            {
                return string.Create(chars.Length, (position, source: chars.ToString()), (dest, state) =>
                {
                    state.source.AsSpan().Slice(0, position).CopyTo(dest);
                    dest[state.position] = (char) (state.source[state.position] + 1);
                    dest.Slice(state.position + 1).Fill('0');
                });
            }
        }

        public static string Method_Create_STR(string chars)
        {
            if (chars is null || chars.Length == 0) { return "0"; }

            int position = -1;
            for (int index = chars.Length - 1; index > -1; index--)
            { if (chars[index] != '9') { position = index; break; } }

            if (position == -1)
            {
                return string.Create(chars.Length + 1, 0, (dest, _) => { dest.Fill('0'); dest[0] = '1'; });
            }
            else
            {
                return string.Create(chars.Length, (position, source: chars), (dest, state) =>
                {
                    state.source.AsSpan().Slice(0, position).CopyTo(dest);
                    dest[state.position] = (char) (state.source[state.position] + 1);
                    dest.Slice(state.position + 1).Fill('0');
                });
            }
        }

        public static string Method_Create2_STR(string chars)
        {
            if (chars is null || chars.Length == 0) { return "0"; }

            int position = -1;
            for (int index = chars.Length - 1; index > -1; index--)
            { if (chars[index] != '9') { position = index; break; } }

            if (position == -1)
            {
                return string.Create(chars.Length + 1, 0, (dest, _) => { dest.Fill('0'); dest[0] = '1'; });
            }
            else
            {
                return string.Create(chars.Length, (position, source: chars), (dest, state) =>
                {
                    state.source.CopyTo(dest, 0);
                    dest[state.position] = (char) (state.source[state.position] + 1);
                    dest.Fill('0', position + 1);
                });
            }
        }

        public static string Method_Create3_STR(string chars)
        {
            if (chars is null || chars.Length == 0) { return Semantic.Texts.DefaultTokenString; }

            int position = -1;
            for (int index = chars.Length - 1; index > -1; index--)
            { if (chars[index] != '9') { position = index; break; } }

            if (position == -1)
            {
                return string.Create(chars.Length + 1, 0, (dest, _) => { dest.Fill('0'); dest[0] = '1'; });
            }
            else
            {
                return string.Create(chars.Length, (position, source: chars), (dest, state) =>
                {
                    for (int index = 0; index < state.source.Length; index++) { dest[index] = state.source[index]; }
                    dest[state.position] = (char) (state.source[state.position] + 1);
                    for (int index = position + 1; index < dest.Length; index++) { dest[index] = '0'; }
                });
            }
        }

        public static void Fill<T>(this Span<T> self, T value, int startIndex)
        {
            for (int index = startIndex; index < self.Length; index++)
            { self[index] = value; }
        }

        public static void CopyTo(this string self, Span<char> destination, int destinationStartIndex)
        {
            for (int index = 0; index < self.Length; index++)
            {
                int destinationIndex = destinationStartIndex + index;
                if (destinationIndex >= destination.Length) { break; }
                destination[destinationIndex] = self[index];
            }
        }
    }
}
