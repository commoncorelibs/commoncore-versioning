﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.CommandLine;
using CommonCore.CommandLine.AutoBuild.Attributes;
using CommonCore.CommandLine.Rendering;

namespace CommonCore.Versioning.ConsoleTesting.Commands
{
    public static class ScanVersionsCommand
    {
        public static void Scan(
            [Argument] string[] versions,
            bool verbose = false
            )
        {
            var screen = new Screen();
            foreach (string version in versions)
            {
                var scan = CommonCore.Versioning.Semantic.Scan(version);
                screen.Add()
                    .Add($"Source: {scan.Source}")
                    .Add($"Core:  {scan.Core.SymbolIndex,3} | {scan.Core.HasSymbol,5} | {scan.Core.SectionIndex,3} | {scan.Core.SectionLength,3} | {scan.Core.HasSection,5} | {scan.Core.Content,20} | {scan.Core.TokenCount,2} |")
                    .Add($"Cycle: {scan.Cycle.SymbolIndex,3} | {scan.Cycle.HasSymbol,5} | {scan.Cycle.SectionIndex,3} | {scan.Cycle.SectionLength,3} | {scan.Cycle.HasSection,5} | {scan.Cycle.Content,20} | {scan.Cycle.TokenCount,2} |")
                    .Add($"Build: {scan.Build.SymbolIndex,3} | {scan.Build.HasSymbol,5} | {scan.Build.SectionIndex,3} | {scan.Build.SectionLength,3} | {scan.Build.HasSection,5} | {scan.Build.Content,20} | {scan.Build.TokenCount,2} |")
                    ;
            }

            screen.Render();
        }
    }
}
