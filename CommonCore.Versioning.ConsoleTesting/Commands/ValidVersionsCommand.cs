﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections.Generic;
using System.CommandLine;
using CommonCore.CommandLine.AutoBuild.Attributes;
using CommonCore.CommandLine.Rendering;

namespace CommonCore.Versioning.ConsoleTesting.Commands
{
    public static class ValidVersionsCommand
    {
        public static void Valid(
            [Argument] string[] versions
            )
        {
            var screen = new Screen();

            List<Exception> errors = new();
            foreach (string version in versions)
            {
                errors.Clear();
                bool valid = Semantic.Validate(version, errors);
                screen.Add($"Semantic Version '{version}' is {(valid ? "valid" : "INVALID")}").Add();
                if (!valid)
                {
                    screen.Add(errors.Count).Add(' ').Add("ERRORS").Add();
                    foreach (var error in errors)
                    {
                        screen.Add(error.Message).Add();
                    }
                }
            }
            
            screen.Render();
        }
    }
}
