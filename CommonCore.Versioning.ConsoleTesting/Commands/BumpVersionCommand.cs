﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System.CommandLine;
using System.CommandLine.Parsing;
using CommonCore.CommandLine.AutoBuild.Attributes;
using CommonCore.CommandLine.Rendering;
using CommonCore.CommandLine.Rendering.Screens;

namespace CommonCore.Versioning.ConsoleTesting.Commands
{
    public static class BumpVersionCommand
    {
        [CommandReference(typeof(BumpVersionCommand), nameof(BumpVersionCommand.Major), true)]
        [CommandReference(typeof(BumpVersionCommand), nameof(BumpVersionCommand.Minor), true)]
        [CommandReference(typeof(BumpVersionCommand), nameof(BumpVersionCommand.Patch), true)]
        [CommandReference(typeof(BumpVersionCommand), nameof(BumpVersionCommand.Cycle), true)]
        [CommandReference(typeof(BumpVersionCommand), nameof(BumpVersionCommand.Build), true)]
        public static void Bump(ParseResult result
            )
        {
            HelpScreen.Instance.Show(result);
        }

        public static void Major(
            [Argument] string? version
            )
        {
            var screen = new Screen()
                .Add(SemanticVersion.Create(version).ToBumpedMajor().ToString()).Add()
                .Render();
                ;
        }

        public static void Minor(
            [Argument] string? version
            )
        {
            var screen = new Screen()
                .Add(SemanticVersion.Create(version).ToBumpedMinor().ToString()).Add()
                .Render();
            ;
        }

        public static void Patch(
            [Argument] string? version,
            string? prefix = ""
            )
        {
            var screen = new Screen()
                .Add(SemanticVersion.Create(version).ToBumpedPatch(prefix).ToString()).Add()
                .Render();
            ;
        }

        public static void Cycle(
            [Argument] string? version,
            string? prefix = ""
            )
        {
            var screen = new Screen()
                .Add(SemanticVersion.Create(version).ToBumpedCycle(prefix).ToString()).Add()
                .Render();
            ;
        }

        public static void Build(
            [Argument] string? version,
            string? prefix = ""
            )
        {
            var screen = new Screen()
                .Add(SemanticVersion.Create(version).ToBumpedBuild(prefix).ToString()).Add()
                .Render();
            ;
        }
    }
}
