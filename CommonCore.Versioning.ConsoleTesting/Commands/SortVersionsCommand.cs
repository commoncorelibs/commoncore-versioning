﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.CommandLine;
using System.Linq;
using CommonCore.CommandLine.AutoBuild.Attributes;
using CommonCore.CommandLine.Rendering;

namespace CommonCore.Versioning.ConsoleTesting.Commands
{
    public static class SortVersionsCommand
    {
        // Verbose true should use a screen with index numbers
        // Verbose false should just print the list
        // Maybe add delimiter option
        // Need ascending/descending option
        public static void Sort(
            [Argument] string[] versions,
            bool verbose = false
            )
        {
            var sorted = versions
                .Select(version => SemanticVersion.Create(version))
                .OrderBy(version => version);
            var screen = new Screen().Add(string.Join("\n", sorted)).Render();
        }
    }
}
