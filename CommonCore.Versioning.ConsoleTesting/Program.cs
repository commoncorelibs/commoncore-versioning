﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.CommandLine;
using System.CommandLine.Parsing;
using CommonCore.CommandLine.AutoBuild.Attributes;
using CommonCore.CommandLine.Rendering.Screens;
using CommonCore.Versioning.ConsoleTesting.Commands;

namespace CommonCore.Versioning.ConsoleTesting
{
    [EnableDefaultExceptionReporting]
    [EnableDefaultErrorReporting]
    [EnableDefaultGlobalOptions]
    [AppMITLicense]
    static class Program
    {
        /// <summary>
        /// This is an example Hello World application
        /// using the <c>CommonCore.CommandLine</c> library.
        /// </summary>
        [CommandEntryPoint]
        [CommandReference(typeof(ScanVersionsCommand), nameof(ScanVersionsCommand.Scan), true)]
        [CommandReference(typeof(ValidVersionsCommand), nameof(ValidVersionsCommand.Valid), true)]
        [CommandReference(typeof(SortVersionsCommand), nameof(SortVersionsCommand.Sort), true)]
        [CommandReference(typeof(BumpVersionCommand), nameof(BumpVersionCommand.Bump), true)]
        [CommandReference(nameof(Help))]
        [CommandReference(nameof(License))]
        [CommandReference(nameof(Version))]
        static void Main(IConsole console, ParseResult result) => HelpScreen.Instance.Show(result);

        // https://docs.npmjs.com/cli/v6/using-npm/semver
        // validate
        // clean
        // bump [major|minor|patch|prerelease|build]


        /// <summary>
        /// Print the program help information.
        /// </summary>
        static void Help(ParseResult result) => HelpScreen.Instance.Show(result);

        /// <summary>
        /// Print the program license information.
        /// </summary>
        static void License() => LicenseScreen.Instance.Show();

        /// <summary>
        /// Print the program version information.
        /// </summary>
        static void Version() => VersionScreen.Instance.Show();
    }

}
