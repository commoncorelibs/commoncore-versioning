﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace CommonCore.Versioning.ConsoleTesting
{
    class Program_OLD
    {
        static void Main(string[] args)
        {
            {
            }

            {
                //var text = "a1..5-pre0.010.alpha.0";

                //Console.WriteLine(text);
                //var tree = SemanticParser.BuildTree(text);
                //if (!tree.Valid)
                //{
                //    Console.WriteLine($"pattern: {tree}");
                //    Console.WriteLine("ERRORS:");
                //    foreach (var error in tree.Errors)
                //    {
                //        Console.WriteLine(error.Message);
                //    }
                //}
            }

            {
                //var bumper = new Bumper();
                //var pattern = ".^.-+";

                //var tokens = bumper.Tokenize(pattern);
                //Console.WriteLine();
                //Console.WriteLine("Tokenize: " + string.Join("", bumper.Textualize(tokens)));
                //Console.WriteLine(string.Join(", ", tokens));

                //tokens = bumper.Normalize(tokens);
                //Console.WriteLine();
                //Console.WriteLine("Normalize: " + string.Join("", bumper.Textualize(tokens)));
                //Console.WriteLine(string.Join(", ", tokens));

                //var patterns = new[]
                //{ 
                //    "^..",
                //    "^..-",
                //    "^..+",
                //    "^..-+",
                //    ".^.",
                //    ".^.-",
                //    ".^.+",
                //    ".^.-+",
                //    "..^",
                //    "..^-",
                //    "..^+",
                //    "..^-+",
                //    "..-^",
                //    "..-^+",
                //    "..-+^"
                //};
                //Console.WriteLine($"Count:    {patterns.Length}");
                //Console.WriteLine($"Shortest: {patterns.Min(p => p.Length)}");
                //Console.WriteLine($"Longest:  {patterns.Max(p => p.Length)}");
                //foreach (var pattern in patterns)
                //{ Console.WriteLine($"{pattern}: {pattern.Length}"); }
            }

            //{
            //    var bumper = new Bumper();

            //    bumper.Test("a1..5-pre0.010.alpha.0+build.08s0f8080dfs8800sd");
            //    bumper.Test("a1..5-pre0.010.alpha.0+");
            //    bumper.Test("a1..5-pre0.010.alpha.0");
            //}
        }
    }

    public class Bumper
    {
        public void Test(ReadOnlySpan<char> pattern)
        {

            Console.WriteLine();
        }

        //public IEnumerable<TokenType> Tokenize(string pattern)
        //{
        //    for (int index = 0; index < pattern.Length; index++)
        //    {
        //        switch (pattern[index])
        //        {
        //            case '[': { yield return TokenType.Open; break; }
        //            case ']': { yield return TokenType.Close; break; }
        //            case '.': { yield return TokenType.Dot; break; }
        //            case '-': { yield return TokenType.Dash; break; }
        //            case '+': { yield return TokenType.Plus; break; }
        //            case '^': { yield return TokenType.Bump; break; }
        //            case '*': { yield return TokenType.Reset; break; }
        //            case '#': { yield return TokenType.Skip; break; }
        //            default: { yield return TokenType.Unknown; break; }
        //        }
        //    }
        //}

        //public IEnumerable<TokenType> Normalize(IEnumerable<TokenType> tokens)
        //{
        //    bool bumped = false;
        //    TokenType prev = TokenType.None;
        //    foreach (var token in tokens)
        //    {
        //        switch (token)
        //        {
        //            case TokenType.Bump: { bumped = true; break; }
        //            case TokenType.Close:
        //            case TokenType.Dot:
        //            case TokenType.Dash:
        //            case TokenType.Plus:
        //            {
        //                switch (prev)
        //                {
        //                    case TokenType.None:
        //                    case TokenType.Open:
        //                    case TokenType.Dot:
        //                    case TokenType.Dash:
        //                    case TokenType.Plus:
        //                    {
        //                        yield return bumped ? TokenType.Reset : TokenType.Skip;
        //                        break;
        //                    }
        //                }
        //                break;
        //            }
        //        }
        //        yield return token;
        //        prev = token;
        //    }
        //}

        //public IEnumerable<char> Textualize(IEnumerable<TokenType> tokens)
        //{
        //    foreach (var token in tokens)
        //    {
        //        switch (token)
        //        {
        //            case TokenType.Open: { yield return '['; break; }
        //            case TokenType.Close: { yield return ']'; break; }
        //            case TokenType.Dot: { yield return '.'; break; }
        //            case TokenType.Dash: { yield return '-'; break; }
        //            case TokenType.Plus: { yield return '+'; break; }
        //            case TokenType.Bump: { yield return '^'; break; }
        //            case TokenType.Reset: { yield return '*'; break; }
        //            case TokenType.Skip: { yield return '#'; break; }
        //            default: { yield return '?'; break; }
        //        }
        //    }
        //}

        //public enum TokenType
        //{
        //    None = 0,
        //    Open,// [
        //    Close,// ]
        //    Dot,// .
        //    Dash,// -
        //    Plus,// +
        //    Bump,// ^
        //    Reset,// *
        //    Skip,// #
        //    Unknown
        //}

        /*
        The version 0.0.0-pre.0+build.0 would be transformed:

            MAJOR
        🚫 [] ➡ 1.0.0
        🚫 [^] ➡ 1.0.0
        🚫 [^.] ➡ 1.0.0
        ✅ [^..] ➡ 1.0.0
        ✅ [^..-] ➡ 1.0.0-pre.0
        ✅ [^..+] ➡ 1.0.0+build.0
        ✅ [^..-+] ➡ 1.0.0-pre.0+build.0

            MINOR
        🚫 [.] ➡ 0.1.0
        🚫 [.^] ➡ 0.1.0
        ✅ [.^.] ➡ 0.1.0
        ✅ [.^.-] ➡ 0.1.0-pre.0
        ✅ [.^.+] ➡ 0.1.0+build.0
        ✅ [.^.-+] ➡ 0.1.0-pre.0+build.0

            PATCH
        🚫 [..] ➡ 0.0.1
        ✅ [..^] ➡ 0.0.1
        ✅ [..^-] ➡ 0.0.1-pre.0
        ✅ [..^+] ➡ 0.0.1+build.0
        ✅ [..^-+] ➡ 0.0.1-pre.0+build.0

            CYCLE
        🚫 [..-] ➡ 0.0.1-pre.1
        ✅ [..-^] ➡ 0.0.1-pre.1
        ✅ [..-^+] ➡ 0.0.1-pre.1+build.0

            BUILD
        🚫 [..-+] ➡ 0.0.1-pre.0+build.1
        ✅ [..-+^] ➡ 0.0.1-pre.0+build.1
        */

        //public SemanticVersion Bump(string bumping)
        //{
        //    var symbols = new[] { '.', '-', '+', '^' };
        //    var pattern = bumping.Trim().TrimStart('[').TrimEnd(']').Trim();
        //    for (int index = 0; index < bumping.Length; index++)
        //    {
        //        char c = pattern[index];
        //        if (!symbols.Contains(c))
        //        { throw new FormatException($"Invalid symbol '{c}' at position '{index}' in pattern '{pattern}'."); }
        //    }

        //    int element = 0;
        //    char symbol = '\0';
        //    var part = SemanticVersion.BumpType.Major;
        //    for (int index = 0; index < bumping.Length; index++)
        //    {
        //        //if 
        //    }
        //}
    }
}
