﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;

namespace CommonCore.Text
{
    public readonly struct StringSpan
    {
        public StringSpan(string? source, int index, int length)
        {
            this.Source = source ?? string.Empty;
            this.Index = index;
            this.Length = length;
        }

        public string Source { get; }

        public int Index { get; }

        public int Length { get; }

        public char this[int index]
            => this.Source[this.Index + index];

        public ReadOnlySpan<char> AsSpan()
            => this.Source.AsSpan(this.Index, this.Length);

        public ReadOnlyMemory<char> AsMemory()
            => this.Source.AsMemory(this.Index, this.Length);

        public override string ToString() => this.Source.Substring(this.Index, this.Length);

        public static implicit operator ReadOnlySpan<char>(StringSpan span)
            => span.AsSpan();
    }
}
