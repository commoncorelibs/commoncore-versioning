﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;

namespace CommonCore
{
    internal static class SpanExtensions
    {
        public static bool Any<T>(this ReadOnlySpan<T> self, Func<T, bool> func)
        {
            if (self.IsEmpty) { return false; }
            for (int index = 0; index < self.Length; index++)
            { if (func(self[index])) { return true; } }
            return false;
        }

        public static bool All<T>(this ReadOnlySpan<T> self, Func<T, bool> func)
        {
            if (self.IsEmpty) { return false; }
            for (int index = 0; index < self.Length; index++)
            { if (!func(self[index])) { return false; } }
            return true;
        }

        public static int Count<T>(this ReadOnlySpan<T> self, Func<T, bool> filter)
        {
            int count = 0;
            foreach (var item in self) { if (filter(item)) { count++; } }
            return count;
        }

        public static void CopyTo(this IEnumerable<string> self, Span<char> destination, int destinationStartIndex, out int count)
        {
            Throw.If.Self.IsNull(self);
            if (!self.Any()) { count = 0; return; }
            Throw.If.Arg.IsIndexOutOfRange(nameof(destinationStartIndex), destinationStartIndex, destination);

            count = 0;
            foreach (string str in self)
            {
                if (str is null || str.Length == 0) { continue; }
                str.AsSpan().CopyTo(destination.Slice(destinationStartIndex + count));
                count += str.Length;
            }
        }

        public static string[] SplitToStrings(this ReadOnlySpan<char> self, char delimiter)
        {
            if (self.IsEmpty) { return Array.Empty<string>(); }

            int count = 1;
            for (int index = 0; index < self.Length; index++)
            { if (self[index] == delimiter) { count++; } }

            string[] array = new string[count];

            int arrayIndex = 0;
            int prev = -1;
            for (int index = 0; index < self.Length; index++)
            {
                if (self[index] == delimiter)
                {
                    array[arrayIndex++] = self[(prev + 1)..index].ToString();
                    prev = index;
                }
            }
            if (prev < self.Length)
            {
                array[arrayIndex++] = self[(prev + 1)..].ToString();
            }
            return array;
        }

        //static ReadOnlySpan<T> AsSpan<T>(this ImmutableArray<T> array)
        //{
        //    return Unsafe.As<ImmutableArray<T>, T[]>(ref array);
        //}
    }
}
