﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System.Collections.Generic;

namespace CommonCore
{
    internal static class IEnumerableExtensions
    {
        /// <summary>
        /// Inserts the specified <paramref name="delimiter"/> between each
        /// item of the enumeration.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="delimiter"></param>
        /// <returns></returns>
        public static IEnumerable<T> Intersperse<T>(this IEnumerable<T> source, T delimiter)
        {
            using var enumerator = source.GetEnumerator();
            if (!enumerator.MoveNext()) { yield break; }

            yield return enumerator.Current;
            while (enumerator.MoveNext())
            {
                yield return delimiter;
                yield return enumerator.Current;
            }
        }

        public static IEnumerable<T> And<T>(this IEnumerable<T> self, T and)
        {
            Throw.If.Self.IsNull(self);
            foreach (var value in self) { yield return value; }
            yield return and;
        }

        public static IEnumerable<T> And<T>(this IEnumerable<T> self, IEnumerable<T> and)
        {
            Throw.If.Self.IsNull(self);
            foreach (var value in self) { yield return value; }
            foreach (var value in and) { yield return value; }
        }
    }
}
