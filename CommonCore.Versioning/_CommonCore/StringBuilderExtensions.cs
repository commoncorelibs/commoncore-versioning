﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections.Generic;
using System.Text;

namespace CommonCore
{
    internal static class StringBuilderExtensions
    {
        public static StringBuilder Append(this StringBuilder self, ReadOnlySpan<string> value)
        {
            for (int index = 0; index < value.Length; index++)
            {
                self.Append(value[index]);
            }
            return self;
        }

        //public static StringBuilder AppendJoin(this StringBuilder self, char separator, ReadOnlySpan<string> value)
        //{
        //    for (int index = 0; index < value.Length; index++)
        //    {
        //        if (index > 0) { self.Append(separator); }
        //        self.Append(value[index]);
        //    }
        //    return self;
        //}

        //public static StringBuilder AppendJoin(this StringBuilder self, string separator, ReadOnlySpan<string> value)
        //{
        //    for (int index = 0; index < value.Length; index++)
        //    {
        //        if (index > 0) { self.Append(separator); }
        //        self.Append(value[index]);
        //    }
        //    return self;
        //}

        //public static StringBuilder AppendJoin(this StringBuilder self, char separator, IReadOnlyList<string> value)
        //{
        //    for (int index = 0; index < value.Count; index++)
        //    {
        //        if (index > 0) { self.Append(separator); }
        //        self.Append(value[index]);
        //    }
        //    return self;
        //}

        //public static StringBuilder AppendJoin(this StringBuilder self, string separator, IReadOnlyList<string> value)
        //{
        //    for (int index = 0; index < value.Count; index++)
        //    {
        //        if (index > 0) { self.Append(separator); }
        //        self.Append(value[index]);
        //    }
        //    return self;
        //}

        public static StringBuilder AppendJoin<T>(this StringBuilder self, string separator, ReadOnlySpan<T> value)
        {
            for (int index = 0; index < value.Length; index++)
            {
                if (index > 0) { self.Append(separator); }
                self.Append(value[index]);
            }
            return self;
        }

        public static StringBuilder AppendJoin<T>(this StringBuilder self, string separator, IReadOnlyList<T> value)
        {
            for (int index = 0; index < value.Count; index++)
            {
                if (index > 0) { self.Append(separator); }
                self.Append(value[index]);
            }
            return self;
        }
    }
}
