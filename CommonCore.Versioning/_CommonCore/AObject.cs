﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

namespace CommonCore
{
    /// <summary>
    /// The <see cref="AObject"/> <c>abstract</c> <c>class</c> provides an implicit
    /// conversion to a boolean value based on null equality, returning <c>false</c>
    /// if the object is <c>null</c> and <c>true</c> if it is not <c>null</c>.
    /// </summary>
    public abstract class AObject
    {
        /// <summary>
        /// Implicitly convert the <see cref="AObject"/> to a boolean value based on null equality.
        /// Returns <c>false</c> if the object is <c>null</c> and <c>true</c> if it is not <c>null</c>.
        /// </summary>
        /// <param name="obj">The <see cref="AObject"/> to check for null equality.</param>
        /// <returns><c>true</c> if the object is not equal to <c>null</c>; otherwise <c>false</c>.</returns>
        public static implicit operator bool(AObject obj) => obj is not null;
    }
}
