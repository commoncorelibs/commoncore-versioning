﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;

namespace CommonCore.Collections
{
    /// <summary>
    /// Represents a dynamic data collection that provides notifications when items are
    /// added, removed, or replaced, or when any contained item has raised a
    /// <see cref="INotifyPropertyChanged.PropertyChanged"/> event.
    /// </summary>
    /// <typeparam name="TItem">The type of <see cref="INotifyPropertyChanged"/> implementing elements in the collection.</typeparam>
    public class ObservableItemCollection<TItem> : ObservableCollection<TItem>
        where TItem : INotifyPropertyChanged
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ObservableItemCollection{T}"/> class.
        /// </summary>
        public ObservableItemCollection() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ObservableItemCollection{T}"/> class
        /// that contains elements copied from the specified list.
        /// </summary>
        /// <param name="list">The list from which the elements are copied.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="list"/> is <c>null</c>.</exception>
        public ObservableItemCollection(List<TItem> list) : base(list)
        {
            Throw.If.Arg.IsNull(nameof(list), list);
            this.SubscribeItems(this.Items);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ObservableItemCollection{T}"/> class
        /// that contains elements copied from the specified collection.
        /// </summary>
        /// <param name="enumerable">The collection from which the elements are copied.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="enumerable"/> is <c>null</c>.</exception>
        public ObservableItemCollection(IEnumerable<TItem> enumerable) : base(enumerable)
        {
            Throw.If.Arg.IsNull(nameof(enumerable), enumerable);
            this.SubscribeItems(this.Items);
        }

        /// <summary>
        /// Occurs when a <see cref="INotifyPropertyChanged.PropertyChanged"/> event is raised on
        /// an object contained in this collection.
        /// </summary>
        public event EventHandler<ItemPropertyChangedEventArgs> ItemPropertyChanged;

        /// <summary>
        /// Raises the <see cref="ObservableCollection{T}.CollectionChanged"/> event with the provided arguments.
        /// </summary>
        /// <param name="e">Arguments of the event being raised.</param>
        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove || e.Action == NotifyCollectionChangedAction.Replace)
            { this.UnsubscribeItems(e.OldItems); }

            if (e.Action == NotifyCollectionChangedAction.Add || e.Action == NotifyCollectionChangedAction.Replace)
            { this.SubscribeItems(e.NewItems); }

            base.OnCollectionChanged(e);
        }

        /// <summary>
        /// Raises the <see cref="ObservableItemCollection{T}.ItemPropertyChanged"/> event with the provided arguments.
        /// </summary>
        /// <param name="sender">Item raising the <see cref="INotifyPropertyChanged.PropertyChanged"/> event.</param>
        /// <param name="e">Arguments of the event being raised.</param>
        /// <exception cref="InvalidOperationException">If <paramref name="sender"/> is not part of the collection.</exception>
        protected void OnItemPropertyChanged(object sender, PropertyChangedEventArgs e)
            => this.ItemPropertyChanged?.Invoke(this, new ItemPropertyChangedEventArgs(this.Items.IndexOf((TItem) sender), e.PropertyName));

        /// <summary>
        /// Subscribe to the <see cref="INotifyPropertyChanged.PropertyChanged"/> event for each
        /// element in the <paramref name="items"/> collection.
        /// </summary>
        /// <param name="items">Collection of elements to subscribe to.</param>
        protected void SubscribeItems(IEnumerable items)
        { foreach (TItem item in items) { item.PropertyChanged += this.OnItemPropertyChanged; } }

        /// <summary>
        /// Unsubscribe from the <see cref="INotifyPropertyChanged.PropertyChanged"/> event for each
        /// element in the <paramref name="items"/> collection.
        /// </summary>
        /// <param name="items">Collection of elements to unsubscribe from.</param>
        protected void UnsubscribeItems(IEnumerable items)
        { foreach (TItem item in items) { item.PropertyChanged -= this.OnItemPropertyChanged; } }

        /// <summary>
        /// Removes all items from the collection.
        /// </summary>
        protected override void ClearItems()
        {
            this.UnsubscribeItems(this.Items);
            base.ClearItems();
        }
    }
}
