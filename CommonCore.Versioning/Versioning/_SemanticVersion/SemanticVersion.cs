﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace CommonCore.Versioning
{
    [DebuggerDisplay("{ToString(),raw}")]
    public readonly partial struct SemanticVersion
    {
        public static SemanticVersion Default { get; }
            = new(SemanticSection.CreateEmpty(ESemanticSectionType.Core, 3), SemanticSection.CreateEmpty(ESemanticSectionType.Cycle), SemanticSection.CreateEmpty(ESemanticSectionType.Build));

        public static SemanticVersion Create(string? version)
        {
            Semantic.Scan(version).Parse(out string[]? core, out string[]? cycle, out string[]? build);
            return SemanticVersion.Create(core, cycle, build);
        }

        public static SemanticVersion Create(SemanticSection core, SemanticSection cycle, SemanticSection build)
            => new(SemanticSection.Create(core), SemanticSection.Create(cycle), SemanticSection.Create(build));

        public static SemanticVersion Create(IEnumerable<SemanticToken>? core, IEnumerable<SemanticToken>? cycle, IEnumerable<SemanticToken>? build)
            => new(SemanticSection.Create(core, ESemanticSectionType.Core),
                SemanticSection.Create(cycle, ESemanticSectionType.Cycle),
                SemanticSection.Create(build, ESemanticSectionType.Build));

        public static SemanticVersion Create(IEnumerable<string>? core, IEnumerable<string>? cycle, IEnumerable<string>? build)
            => new(SemanticSection.Create(core, ESemanticSectionType.Core),
                SemanticSection.Create(cycle, ESemanticSectionType.Cycle),
                SemanticSection.Create(build, ESemanticSectionType.Build));
    }

    public readonly partial struct SemanticVersion
    {
        public SemanticVersion(SemanticSection core, SemanticSection cycle, SemanticSection build)
        {
            this.Core = core;
            this.Cycle = cycle;
            this.Build = build;
        }

        public bool IsEmpty => this.Core.IsEmpty && this.Cycle.IsEmpty && this.Build.IsEmpty;

        public SemanticSection Core { get; init; }

        public SemanticSection Cycle { get; init; }

        public SemanticSection Build { get; init; }

        public SemanticToken this[ESemanticSectionType type, int index]
            => this[type][index];

        public SemanticSection this[ESemanticSectionType type]
            => type switch
            {
                ESemanticSectionType.Core => this.Core,
                ESemanticSectionType.Cycle => this.Cycle,
                ESemanticSectionType.Build => this.Build,
                _ => throw new IndexOutOfRangeException()
            };

        public override string ToString()
            => SemanticVersionFormatter.Default.ToString(this);
    }

    public readonly partial struct SemanticVersion : IEquatable<SemanticVersion>
    {
        public override int GetHashCode()
            => SemanticVersionEqualityComparer.Default.GetHashCode(this);

        public bool Equals(object? obj)
            => obj is SemanticVersion other && SemanticVersionEqualityComparer.Default.Equals(this, other);

        public bool Equals(SemanticVersion other)
            => SemanticVersionEqualityComparer.Default.Equals(this, other);

        public static bool operator ==(SemanticVersion left, SemanticVersion right)
            => SemanticVersionEqualityComparer.Default.Equals(left, right);

        public static bool operator !=(SemanticVersion left, SemanticVersion right)
            => !SemanticVersionEqualityComparer.Default.Equals(left, right);
    }

    public readonly partial struct SemanticVersion : IComparable, IComparable<SemanticVersion>
    {
        public int CompareTo(object? obj)
            => obj is SemanticVersion other ? SemanticVersionComparer.Default.Compare(this, other) : 0;

        public int CompareTo(SemanticVersion other)
            => SemanticVersionComparer.Default.Compare(this, other);

        public static bool operator >(SemanticVersion left, SemanticVersion right)
            => SemanticVersionComparer.Default.Compare(left, right) > 0;

        public static bool operator <(SemanticVersion left, SemanticVersion right)
            => SemanticVersionComparer.Default.Compare(left, right) < 0;

        public static bool operator >=(SemanticVersion left, SemanticVersion right)
            => SemanticVersionComparer.Default.Compare(left, right) >= 0;

        public static bool operator <=(SemanticVersion left, SemanticVersion right)
            => SemanticVersionComparer.Default.Compare(left, right) <= 0;
    }

    public readonly partial struct SemanticVersion : IEnumerable<SemanticSection>
    {
        public IEnumerator<SemanticSection> GetEnumerator()
        {
            yield return this.Core;
            yield return this.Cycle;
            yield return this.Build;
        }

        [ExcludeFromCodeCoverage]
        IEnumerator IEnumerable.GetEnumerator()
            => this.GetEnumerator();
    }

    public class SemanticVersionComparer : Comparer<SemanticVersion>
    {
        public static new SemanticVersionComparer Default { get; } = new SemanticVersionComparer();

        /// <summary>
        /// Compares two semantic models for precedence, returning <c>-1</c>, <c>0</c>, or <c>1</c>.
        /// This method assumes both models are semantically valid.
        /// </summary>
        /// <param name="left">The first model to be compared.</param>
        /// <param name="right">The other model to be compared.</param>
        /// <returns>
        /// <c>-1</c> if <paramref name="left"/> is less than <paramref name="right"/>;
        /// <c>1</c> if <paramref name="left"/> is greater than <paramref name="right"/>;
        /// otherwise <c>0</c> if <paramref name="left"/> and <paramref name="right"/> are equal.
        /// </returns>
        public override int Compare(SemanticVersion left, SemanticVersion right)
            => this.Compare(left.Core, left.Cycle, left.Build, right.Core, right.Cycle, right.Build);

        public int Compare(
            IReadOnlyList<SemanticToken>? leftCore, IReadOnlyList<SemanticToken>? leftCycle, IReadOnlyList<SemanticToken>? leftBuild,
            IReadOnlyList<SemanticToken>? rightCore, IReadOnlyList<SemanticToken>? rightCycle, IReadOnlyList<SemanticToken>? rightBuild)
        {
            int result;

            result = SemanticSectionComparer.Default.Compare(leftCore, rightCore, false);
            if (result != 0) { return result; }

            result = SemanticSectionComparer.Default.Compare(leftCycle, rightCycle, true);
            if (result != 0) { return result; }

            result = SemanticSectionComparer.Default.Compare(leftBuild, rightBuild, false);
            if (result != 0) { return result; }

            return 0;
        }
    }

    public class SemanticVersionEqualityComparer : EqualityComparer<SemanticVersion>
    {
        public static new SemanticVersionEqualityComparer Default { get; } = new SemanticVersionEqualityComparer();

        public override int GetHashCode(SemanticVersion version)
            => HashCode.Combine(version.Core, version.Cycle, version.Build);

        public override bool Equals(SemanticVersion left, SemanticVersion right)
            => this.Equate(left.Core, left.Cycle, left.Build, right.Core, right.Cycle, right.Build);

        public bool Equate(
            IReadOnlyList<SemanticToken> leftCore, IReadOnlyList<SemanticToken> leftCycle, IReadOnlyList<SemanticToken> leftBuild,
            IReadOnlyList<SemanticToken> rightCore, IReadOnlyList<SemanticToken> rightCycle, IReadOnlyList<SemanticToken> rightBuild)
        {
            Throw.If.Arg.IsNull(nameof(leftCore), leftCore);
            Throw.If.Arg.IsNull(nameof(leftCycle), leftCycle);
            Throw.If.Arg.IsNull(nameof(leftBuild), leftBuild);
            Throw.If.Arg.IsNull(nameof(rightCore), rightCore);
            Throw.If.Arg.IsNull(nameof(rightCycle), rightCycle);
            Throw.If.Arg.IsNull(nameof(rightBuild), rightBuild);

            return leftCore.SequenceEqual(rightCore)
                && leftCycle.SequenceEqual(rightCycle)
                && leftBuild.SequenceEqual(rightBuild);
        }
    }

    public class SemanticVersionFormatter
    {
        public static SemanticVersionFormatter Default { get; } = new SemanticVersionFormatter();

        public string ToString(SemanticVersion version)
            => this.Join(version);

        public string Join(SemanticVersion version)
            => this.Join(version.Core, version.Cycle, version.Build);

        /// <summary>
        /// Joins the specified semantic sets into a semantic version string
        /// and returns the result as a new <see cref="string"/>.
        /// </summary>
        /// <returns>A semantic version string.</returns>
        public string Join(IReadOnlyList<SemanticToken>? core, IReadOnlyList<SemanticToken>? cycle, IReadOnlyList<SemanticToken>? build)
        {
#if NETSTANDARD2_0
            //string output = string.Empty;
            //if (core is not null && core.Count > 0) { output += Join(core); }
            //if (cycle is not null) { output += Semantic.CycleSymbol; }
            //if (cycle is not null && cycle.Count > 0) { output += Join(cycle); }
            //if (build is not null) { output += Semantic.BuildSymbol; }
            //if (build is not null && build.Count > 0) { output += Join(build); }
            //return output;

            System.Text.StringBuilder output = new();
            if (core is not null && core.Count > 0) { output.AppendJoin(Semantic.Texts.DelimitSymbolString, core); }
            if (cycle is not null) { output.Append(Semantic.Texts.CycleSymbol); }
            if (cycle is not null && cycle.Count > 0) { output.AppendJoin(Semantic.Texts.DelimitSymbolString, cycle); }
            if (build is not null) { output.Append(Semantic.Texts.BuildSymbol); }
            if (build is not null && build.Count > 0) { output.AppendJoin(Semantic.Texts.DelimitSymbolString, build); }
            return output.ToString();
#else
            int length = 0;
            if (core is not null && core.Count > 0)
            { length += core.Count - 1 + core.Sum(str => str.Length); }

            if (cycle is not null) { length += 1; }
            if (cycle is not null && cycle.Count > 0)
            { length += cycle.Count - 1 + cycle.Sum(str => str.Length); }

            if (build is not null) { length += 1; }
            if (build is not null && build.Count > 0)
            { length += build.Count - 1 + build.Sum(str => str.Length); }

            if (length == 0) { return string.Empty; }
            else
            {
                return string.Create(length,
                (
                    core,
                    cycle,
                    build,
                    delimiter: Semantic.Texts.DelimitSymbol,
                    cycleSymbol: Semantic.Texts.CycleSymbol,
                    buildSymbol: Semantic.Texts.BuildSymbol
                ),
                (dest, state) =>
                {
                    int index = 0;
                    if (state.core is not null)
                    {
                        state.core.Select(token => token.Value).Intersperse(Semantic.Texts.DelimitSymbolString).CopyTo(dest, index, out int count);
                        index += count;
                    }
                    if (state.cycle is not null)
                    {
                        dest[index++] = state.cycleSymbol;
                        state.cycle.Select(token => token.Value).Intersperse(Semantic.Texts.DelimitSymbolString).CopyTo(dest, index, out int count);
                        index += count;
                    }
                    if (state.build is not null)
                    {
                        dest[index++] = state.buildSymbol;
                        state.build.Select(token => token.Value).Intersperse(Semantic.Texts.DelimitSymbolString).CopyTo(dest, index, out int count);
                        index += count;
                    }
                });
            }
#endif
        }
    }

    public class SemanticVersionBumper
    {
        public static SemanticVersionBumper Default { get; } = new SemanticVersionBumper();

        public SemanticVersion Reset(SemanticVersion version)
            => SemanticVersion.Default;

        public SemanticVersion BumpMajor(SemanticVersion version)
        {
            var core = version.Core.ToBumped(EBumpAction.Reset, 0, null);
            return SemanticVersion.Create(core, SemanticSection.DefaultCycle, SemanticSection.DefaultBuild);
        }

        public SemanticVersion BumpMinor(SemanticVersion version)
        {
            var core = version.Core.ToBumped(EBumpAction.Reset, 1, null);
            return SemanticVersion.Create(core, SemanticSection.DefaultCycle, SemanticSection.DefaultBuild);
        }

        public SemanticVersion BumpPatch(SemanticVersion version, string? sectionPrefix)
        {
            if (version.Cycle.IsEmpty)
            {
                var core = version.Core.ToBumped(EBumpAction.Reset, 2, null);
                return SemanticVersion.Create(core, SemanticSection.DefaultCycle, SemanticSection.DefaultBuild);
            }
            else
            {
                return SemanticVersion.Create(version.Core,
                    SemanticSection.CreateEmpty(ESemanticSectionType.Cycle, sectionPrefix, 1),
                    SemanticSection.DefaultBuild);
            }
        }

        public SemanticVersion BumpCycle(SemanticVersion version, string? sectionPrefix)
        {
            if (version.Cycle.IsEmpty)
            {
                var core = version.Core.ToBumped(EBumpAction.Reset, 2, null);
                return SemanticVersion.Create(core,
                    SemanticSection.CreateEmpty(ESemanticSectionType.Cycle, sectionPrefix, 1),
                    SemanticSection.DefaultBuild);
            }
            else
            {
                return SemanticVersion.Create(version.Core, version.Cycle.ToBumped(EBumpAction.Remove, -1, sectionPrefix), SemanticSection.DefaultBuild);
            }
        }

        public SemanticVersion BumpBuild(SemanticVersion version, string? sectionPrefix)
        {
            var build = version.Build.ToBumped(EBumpAction.Remove, -1, sectionPrefix);
            return SemanticVersion.Create(version.Core, version.Cycle, build);
        }
    }

    public static partial class SemanticVersionExtensions
    {
        public static SemanticVersion ToBumpedMajor(this SemanticVersion self)
            => SemanticVersionBumper.Default.BumpMajor(self);

        public static SemanticVersion ToBumpedMinor(this SemanticVersion self)
            => SemanticVersionBumper.Default.BumpMinor(self);

        public static SemanticVersion ToBumpedPatch(this SemanticVersion self, string? sectionPrefix)
            => SemanticVersionBumper.Default.BumpPatch(self, sectionPrefix);

        public static SemanticVersion ToBumpedCycle(this SemanticVersion self, string? sectionPrefix)
            => SemanticVersionBumper.Default.BumpCycle(self, sectionPrefix);

        public static SemanticVersion ToBumpedBuild(this SemanticVersion self, string? sectionPrefix)
            => SemanticVersionBumper.Default.BumpBuild(self, sectionPrefix);
    }
}
