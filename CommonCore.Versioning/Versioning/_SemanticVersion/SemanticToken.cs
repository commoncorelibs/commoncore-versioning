﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using CommonCore.Text.Numeric;

namespace CommonCore.Versioning
{
    public readonly partial struct SemanticToken
    {
        public static SemanticToken Default { get; }
            = new(Semantic.Texts.DefaultTokenString, ESemanticTokenType.Numeric);

        public static SemanticToken Create(string? token)
        {
            ESemanticTokenType tokenType
                = NumericTextHelper.IsAlphabetic(token)
                ? ESemanticTokenType.Alpha
                : NumericTextHelper.IsNumeric(token)
                ? ESemanticTokenType.Numeric
                : token.All(c => NumericTextHelper.IsAlphaNumeric(c) || c == '-')
                ? ESemanticTokenType.AlphaNumeric
                : ESemanticTokenType.Other;
            return new(token, tokenType);
        }

        public static SemanticToken Create(string? token, ESemanticTokenType tokenType)
            => new(token, tokenType);
    }

    public readonly partial struct SemanticToken
    {
        public SemanticToken(string? value, ESemanticTokenType tokenType)
        {
            this.Value = value ?? string.Empty;
            this.TokenType = tokenType;
        }

        public ESemanticTokenType TokenType { get; }

        public string Value { get; }

        public int Length
            => this.Value.Length;

        public bool IsEmpty
            => this.Value.Length > 0;

        public char this[int index]
            => this.Value[index];

        public ReadOnlySpan<char> AsSpan()
            => this.Value.AsSpan();

        public bool IsNumeric()
            => this.TokenType == ESemanticTokenType.Numeric;

        public override string ToString()
            => SemanticTokenFormatter.Default.ToString(this);

        public static implicit operator string(SemanticToken token)
            => SemanticTokenFormatter.Default.ToString(token);

        public static implicit operator ReadOnlySpan<char>(SemanticToken token)
            => token.Value.AsSpan();
    }

    public readonly partial struct SemanticToken : IEquatable<SemanticToken>
    {
        public override int GetHashCode()
            => SemanticTokenEqualityComparer.Default.GetHashCode(this);

        public bool Equals(object? obj)
            => obj is SemanticToken other && SemanticTokenEqualityComparer.Default.Equals(this, other);

        public bool Equals(SemanticToken other)
            => SemanticTokenEqualityComparer.Default.Equals(this, other);

        public static bool operator ==(SemanticToken left, SemanticToken right)
            => SemanticTokenEqualityComparer.Default.Equals(left, right);

        public static bool operator !=(SemanticToken left, SemanticToken right)
            => !SemanticTokenEqualityComparer.Default.Equals(left, right);
    }

    public readonly partial struct SemanticToken : IComparable, IComparable<SemanticToken>
    {
        public int CompareTo(object? obj)
            => obj is SemanticToken other ? SemanticTokenComparer.Default.Compare(this, other) : 0;

        public int CompareTo(SemanticToken other)
            => SemanticTokenComparer.Default.Compare(this, other);

        public static bool operator >(SemanticToken left, SemanticToken right)
            => SemanticTokenComparer.Default.Compare(left, right) > 0;

        public static bool operator <(SemanticToken left, SemanticToken right)
            => SemanticTokenComparer.Default.Compare(left, right) < 0;

        public static bool operator >=(SemanticToken left, SemanticToken right)
            => SemanticTokenComparer.Default.Compare(left, right) >= 0;

        public static bool operator <=(SemanticToken left, SemanticToken right)
            => SemanticTokenComparer.Default.Compare(left, right) <= 0;
    }

    public readonly partial struct SemanticToken : IEnumerable<char>
    {
        public IEnumerator<char> GetEnumerator()
            => this.Value.GetEnumerator();

        [ExcludeFromCodeCoverage]
        IEnumerator IEnumerable.GetEnumerator()
            => ((IEnumerable) this.Value).GetEnumerator();
    }

    public class SemanticTokenComparer : Comparer<SemanticToken>
    {
        public static new SemanticTokenComparer Default { get; } = new SemanticTokenComparer();

        /// <inheritdoc cref="Compare(ReadOnlySpan{char}, ReadOnlySpan{char})"/>
        public override int Compare(SemanticToken left, SemanticToken right)
            => this.Compare(left.AsSpan(), right.AsSpan());

        /// <summary>
        /// Compares two semantic tokens for precedence, returning <c>-1</c>, <c>0</c>, or <c>1</c>.
        /// This method assumes both tokens are semantically valid.
        /// </summary>
        /// <param name="left">The first token to be compared.</param>
        /// <param name="right">The other token to be compared.</param>
        /// <returns>
        /// <c>-1</c> if <paramref name="left"/> is less than <paramref name="right"/>;
        /// <c>1</c> if <paramref name="left"/> is greater than <paramref name="right"/>;
        /// otherwise <c>0</c> if <paramref name="left"/> and <paramref name="right"/> are equal.
        /// </returns>
        public int Compare(ReadOnlySpan<char> left, ReadOnlySpan<char> right)
        {
            //return string.Compare(left.Value, right.Value);
            // Empty tokens are equal;
            // otherwise empty is lower than non-empty.
            if (left.IsEmpty && right.IsEmpty) { return 0; }
            else if (left.IsEmpty) { return -1; }
            else if (right.IsEmpty) { return 1; }
            else
            {
                bool numLeft = NumericTextHelper.IsNumeric(left);
                bool numRight = NumericTextHelper.IsNumeric(right);
                // If both are digits, then compare numerically;
                // otherwise digits are lower than text;
                // otherwise compare as normal text.
                if (numLeft && numRight) { return NumericTextHelper.Compare(left, right); }
                else if (numLeft && !numRight) { return -1; }
                else if (!numLeft && numRight) { return 1; }
                // SequenceCompareTo can throw values outside -1, 0, and 1, so need to clamp it.
                else
                {
                    int result = left.SequenceCompareTo(right);
                    result = result < -1 ? -1 : result > 1 ? 1 : result;
                    return result;
                }
            }
        }
    }

    public class SemanticTokenEqualityComparer : EqualityComparer<SemanticToken>
    {
        public static new SemanticTokenEqualityComparer Default { get; } = new SemanticTokenEqualityComparer();

        public override int GetHashCode(SemanticToken token)
            => HashCode.Combine(token.Value);

        public override bool Equals(SemanticToken left, SemanticToken right)
            => this.Equals(left.AsSpan(), right.AsSpan());

        public bool Equals(ReadOnlySpan<char> left, ReadOnlySpan<char> right)
            => left.SequenceEqual(right);
    }

    public class SemanticTokenFormatter
    {
        public static SemanticTokenFormatter Default { get; } = new SemanticTokenFormatter();

        public string ToString(SemanticToken token)
            => token.Value;

        public int ToInteger(SemanticToken token)
            => NumericTextHelper.IsNumeric(token.AsSpan())
            && int.TryParse(token.Value, NumberStyles.None, CultureInfo.InvariantCulture.NumberFormat, out int value)
            ? value
            : 0;
    }

    public static partial class SemanticTokenExtensions
    {
        public static int ToInteger(this SemanticToken self)
            => SemanticTokenFormatter.Default.ToInteger(self);
    }

    public class SemanticTokenBumper
    {
        public static SemanticTokenBumper Default { get; } = new SemanticTokenBumper();

        public SemanticToken Reset(SemanticToken token)
            => token.TokenType == ESemanticTokenType.Numeric
            ? SemanticToken.Default
            : token;

        public SemanticToken Bump(SemanticToken token, bool useDefualtForNonNumeric)
            => token.TokenType == ESemanticTokenType.Numeric
            ? SemanticToken.Create(NumericTextHelper.Increment(token.Value), token.TokenType)
            : useDefualtForNonNumeric
            ? SemanticToken.Default
            : token;
    }

    public static partial class SemanticTokenExtensions
    {
        public static SemanticToken ToBumped(this SemanticToken self, bool useDefualtForNonNumeric = false)
            => SemanticTokenBumper.Default.Bump(self, useDefualtForNonNumeric);

        public static SemanticToken ToReset(this SemanticToken self)
            => SemanticTokenBumper.Default.Reset(self);
    }
}
