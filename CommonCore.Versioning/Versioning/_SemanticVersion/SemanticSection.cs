﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;

namespace CommonCore.Versioning
{
    public readonly partial struct SemanticSection
    {
        public static SemanticSection Default { get; }
            = SemanticSection.CreateEmpty(ESemanticSectionType.None);

        public static SemanticSection DefaultCore { get; }
            = SemanticSection.CreateEmpty(ESemanticSectionType.Core, 3);

        public static SemanticSection DefaultCycle { get; }
            = SemanticSection.CreateEmpty(ESemanticSectionType.Cycle);

        public static SemanticSection DefaultBuild { get; }
            = SemanticSection.CreateEmpty(ESemanticSectionType.Build);

        public static SemanticSection CreateEmpty(ESemanticSectionType sectionType, int count = 0)
            => new(count == 0 ? null : Enumerable.Repeat(SemanticToken.Default, count), sectionType);

        public static SemanticSection CreateEmpty(ESemanticSectionType sectionType, string? prefix, int count = 0)
        {
            var tokens = Enumerable.Empty<SemanticToken>()
                .AndIf(prefix is not null && prefix.Length > 0, SemanticToken.Create(prefix))
                .AndIf(count > 0, Enumerable.Repeat(SemanticToken.Default, count))
                ;
            return new(tokens, sectionType);
        }

        public static SemanticSection Create(SemanticSection tokens)
            => new(tokens.Tokens, tokens.SectionType);

        public static SemanticSection Create(IEnumerable<SemanticToken>? tokens, ESemanticSectionType sectionType)
            => new(tokens, sectionType);

        public static SemanticSection Create(IEnumerable<string>? tokens, ESemanticSectionType sectionType)
            => new(tokens?.Select(token => SemanticToken.Create(token)), sectionType);
    }

    public readonly partial struct SemanticSection
    {
        public SemanticSection(IEnumerable<SemanticToken>? tokens, ESemanticSectionType sectionType)
        {
            this.SectionType = ESemanticSectionType.None;
            this.Tokens = ImmutableArray.CreateRange(tokens ?? Array.Empty<SemanticToken>());
        }

        public ESemanticSectionType SectionType { get; }

        public ImmutableArray<SemanticToken> Tokens { get; }

        public int Length
            => this.Tokens.Length;

        public bool IsEmpty
            => this.Tokens.Length == 0;

        public SemanticToken this[int index]
            => this.Tokens[index];

        public ReadOnlySpan<SemanticToken> AsSpan()
            => this.Tokens.AsSpan();

        public override string ToString()
            => SemanticSectionFormatter.Default.ToString(this);
    }

    public readonly partial struct SemanticSection : IEquatable<SemanticSection>
    {
        public override int GetHashCode()
            => SemanticSectionEqualityComparer.Default.GetHashCode(this);

        public bool Equals(object? obj)
            => obj is SemanticSection other && SemanticSectionEqualityComparer.Default.Equals(this, other);

        public bool Equals(SemanticSection other)
            => SemanticSectionEqualityComparer.Default.Equals(this, other);

        public static bool operator ==(SemanticSection left, SemanticSection right)
            => SemanticSectionEqualityComparer.Default.Equals(left, right);

        public static bool operator !=(SemanticSection left, SemanticSection right)
            => !SemanticSectionEqualityComparer.Default.Equals(left, right);
    }

    public readonly partial struct SemanticSection : IComparable, IComparable<SemanticSection>
    {
        public int CompareTo(object? obj)
            => obj is SemanticSection other ? SemanticSectionComparer.Default.Compare(this, other) : 0;

        public int CompareTo(SemanticSection other)
            => SemanticSectionComparer.Default.Compare(this, other);

        public static bool operator >(SemanticSection left, SemanticSection right)
            => SemanticSectionComparer.Default.Compare(left, right) > 0;

        public static bool operator <(SemanticSection left, SemanticSection right)
            => SemanticSectionComparer.Default.Compare(left, right) < 0;

        public static bool operator >=(SemanticSection left, SemanticSection right)
            => SemanticSectionComparer.Default.Compare(left, right) >= 0;

        public static bool operator <=(SemanticSection left, SemanticSection right)
            => SemanticSectionComparer.Default.Compare(left, right) <= 0;
    }

    public readonly partial struct SemanticSection : IEnumerable<SemanticToken>
    {
        public IEnumerator<SemanticToken> GetEnumerator()
            => ((IEnumerable<SemanticToken>) this.Tokens).GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
            => ((IEnumerable) this.Tokens).GetEnumerator();
    }

    public readonly partial struct SemanticSection : IReadOnlyList<SemanticToken>
    {
        SemanticToken IReadOnlyList<SemanticToken>.this[int index]
            => this.Tokens[index];

        int IReadOnlyCollection<SemanticToken>.Count
            => this.Tokens.Length;

        IEnumerator<SemanticToken> IEnumerable<SemanticToken>.GetEnumerator()
            => ((IEnumerable<SemanticToken>) this.Tokens).GetEnumerator();
    }

    public readonly partial struct SemanticSection : IReadOnlyList<string>
    {
        string IReadOnlyList<string>.this[int index]
            => this.Tokens[index].Value;

        int IReadOnlyCollection<string>.Count
            => this.Tokens.Length;

        IEnumerator<string> IEnumerable<string>.GetEnumerator()
            => ((IEnumerable<SemanticToken>) this.Tokens).Select(token => token.Value).GetEnumerator();
    }

    public class SemanticSectionComparer : Comparer<SemanticSection>
    {
        public static new SemanticSectionComparer Default { get; } = new SemanticSectionComparer();

        /// <inheritdoc cref="Compare(IReadOnlyList{SemanticToken}, IReadOnlyList{SemanticToken}, bool)"/>
        public override int Compare(SemanticSection left, SemanticSection right)
            => this.Compare(left, right);

        /// <summary>
        /// Compares two semantic sets for precedence, returning <c>-1</c>, <c>0</c>, or <c>1</c>.
        /// This method assumes both sets are semantically valid.
        /// By default, null and empty sets have lower precedence than non-null and non-empty sets, respectively.
        /// Specifying <paramref name="invertEmptyPrecedence"/> as <c>true</c> will reverse this behavior, making
        /// null sets higher than non-null sets and empty sets higher than non-empty sets.
        /// </summary>
        /// <param name="left">The first set to be compared.</param>
        /// <param name="right">The other set to be compared.</param>
        /// <param name="invertEmptyPrecedence">The value indicating that empty sets are higher precedence than non-empty sets.</param>
        /// <returns>
        /// <c>-1</c> if <paramref name="left"/> is less than <paramref name="right"/>;
        /// <c>1</c> if <paramref name="left"/> is greater than <paramref name="right"/>;
        /// otherwise <c>0</c> if <paramref name="left"/> and <paramref name="right"/> are equal.
        /// </returns>
        public int Compare(IReadOnlyList<SemanticToken>? left, IReadOnlyList<SemanticToken>? right, bool invertEmptyPrecedence = false)
        {
            // If both are null, then equal;
            // otherwise null is lower than non-null,
            // unless empty precedence is inverted.
            if (left is null && right is null) { return 0; }
            else if (left is null) { return invertEmptyPrecedence ? 1 : -1; }
            else if (right is null) { return invertEmptyPrecedence ? -1 : 1; }
            // If both are empty, then equal;
            // otherwise empty is lower than non-empty,
            // unless empty precedence is inverted.
            else if (left.Count == 0 && right.Count == 0) { return 0; }
            else if (left.Count == 0) { return invertEmptyPrecedence ? 1 : -1; }
            else if (right.Count == 0) { return invertEmptyPrecedence ? -1 : 1; }
            else
            {
                (int fallback, int count)
                    // If left is smaller than right, then lower if elements are equal.
                    = left.Count < right.Count ? (-1, left.Count)
                    // If left is larger than right, then higher if elements are equal.
                    : left.Count > right.Count ? (1, right.Count)
                    // If same size, then equal if elements are equal.
                    : (0, left.Count);
                // The first non-equal token decides the precedence;
                // otherwise the fallback precedence is used.
                for (int index = 0; index < count; index++)
                {
                    int result = SemanticTokenComparer.Default.Compare(left[index].AsSpan(), right[index].AsSpan());
                    if (result != 0) { return result; }
                }
                return fallback;
            }
        }
    }

    public class SemanticSectionEqualityComparer : EqualityComparer<SemanticSection>
    {
        public static new SemanticSectionEqualityComparer Default { get; } = new SemanticSectionEqualityComparer();

        public override int GetHashCode(SemanticSection section)
            => HashCode.Combine(section.Tokens);

        public override bool Equals(SemanticSection left, SemanticSection right)
            => this.Equals(left.AsSpan(), right.AsSpan());

        public bool Equals(ReadOnlySpan<SemanticToken> left, ReadOnlySpan<SemanticToken> right)
            => left.SequenceEqual(right);
    }

    public class SemanticSectionFormatter
    {
        public static SemanticSectionFormatter Default { get; } = new SemanticSectionFormatter();

        public string ToString(SemanticSection section)
            => this.Join(section);

        public string Join(SemanticSection section)
            => string.Join(Semantic.Texts.DelimitSymbolString, section);
    }

    public class SemanticSectionBumper
    {
        public static SemanticSectionBumper Default { get; } = new SemanticSectionBumper();

        public SemanticSection Reset(SemanticSection section)
            => section.SectionType switch
            {
                ESemanticSectionType.Core => SemanticSection.DefaultCore,
                ESemanticSectionType.Cycle => SemanticSection.DefaultCycle,
                ESemanticSectionType.Build => SemanticSection.DefaultBuild,
                _ => SemanticSection.Default,
            };

        public SemanticSection Bump(SemanticSection section, EBumpAction bumpAction, int bumpIndex = -1, string? prefix = null)
        {
            if (section.IsEmpty) { return this.BumpEmpty(section, prefix); }

            if (bumpIndex == -1 && this.TryFindBumpPrefixIndex(section, prefix, out bumpIndex))
            {
                if (bumpIndex == section.Length)
                {
                    return this.BumpAppend(section, null);
                }
                else
                {
                    return bumpAction switch
                    {
                        EBumpAction.Reset => this.BumpResetTrailing(section, bumpIndex),
                        EBumpAction.Remove => this.BumpRemoveTrailing(section, bumpIndex),
                        _ => this.BumpIgnoreTrailing(section, bumpIndex),
                    };
                }
            }
            else if (bumpIndex == -1 && this.TryFindBumpIndex(section, out bumpIndex))
            {
                return bumpAction switch
                {
                    EBumpAction.Reset => this.BumpResetTrailing(section, bumpIndex),
                    EBumpAction.Remove => this.BumpRemoveTrailing(section, bumpIndex),
                    _ => this.BumpIgnoreTrailing(section, bumpIndex),
                };
            }
            else { return this.BumpAppend(section, prefix); }
        }

        public SemanticSection BumpEmpty(SemanticSection section, string? prefix = null)
        {
            IEnumerable<SemanticToken> tokens
                = Enumerable.Empty<SemanticToken>()
                .AndIf(!string.IsNullOrEmpty(prefix), SemanticToken.Create(prefix))
                .And(SemanticToken.Default);
                ;
            return SemanticSection.Create(tokens, section.SectionType);
        }

        public SemanticSection BumpAppend(SemanticSection section, string? prefix = null)
        {
            IEnumerable<SemanticToken> tokens
                = section.ToEnumerable()
                .AndIf(!string.IsNullOrEmpty(prefix), SemanticToken.Create(prefix))
                .And(SemanticToken.Default);
            ;
            return SemanticSection.Create(tokens, section.SectionType);
        }

        public SemanticSection BumpResetTrailing(SemanticSection section, int bumpIndex)
        {
            if (bumpIndex < 0 || bumpIndex >= section.Length)
            { throw Throw.Build.ArgumentIndexOutOfRangeException(nameof(bumpIndex), bumpIndex, section.Length); }

            IEnumerable<SemanticToken> tokens
                = section.ToEnumerable()
                .Take(bumpIndex)
                .And(section[bumpIndex].ToBumped(true))
                .And(section.ToEnumerable()
                    .Skip(bumpIndex + 1)
                    .Select(token => token.ToReset()))
                ;
            return SemanticSection.Create(tokens, section.SectionType);
        }

        public SemanticSection BumpRemoveTrailing(SemanticSection section, int bumpIndex)
        {
            if (bumpIndex < 0 || bumpIndex >= section.Length)
            { throw Throw.Build.ArgumentIndexOutOfRangeException(nameof(bumpIndex), bumpIndex, section.Length); }

            IEnumerable<SemanticToken> tokens
                = section.ToEnumerable()
                .Take(bumpIndex)
                .And(section[bumpIndex].ToBumped(true))
                ;
            return SemanticSection.Create(tokens, section.SectionType);
        }

        public SemanticSection BumpIgnoreTrailing(SemanticSection section, int bumpIndex)
        {
            if (bumpIndex < 0 || bumpIndex >= section.Length)
            { throw Throw.Build.ArgumentIndexOutOfRangeException(nameof(bumpIndex), bumpIndex, section.Length); }

            IEnumerable<SemanticToken> tokens
                = section.ToEnumerable()
                .Take(bumpIndex)
                .And(section[bumpIndex].ToBumped(true))
                .And(section.ToEnumerable()
                    .Skip(bumpIndex + 1))
                ;
            return SemanticSection.Create(tokens, section.SectionType);
        }

        /// <summary>
        /// Returns the index of the last semantic token within the specified
        /// <paramref name="section"/> that can be bumped. If no appropriate token is found,
        /// then <c>-1</c> will be returned.
        /// </summary>
        /// <param name="section">The semantic set to search.</param>
        /// <returns>The last token in the set that can be bumped; otherwise <c>-1</c>.</returns>
        public bool TryFindBumpIndex(SemanticSection section, out int bumpIndex)
        {
            bumpIndex = -1;
            if (section.IsEmpty) { return false; }
            else
            {
                for (int index = section.Length - 1; index > -1; index--)
                {
                    if (section[index].IsNumeric())
                    {
                        bumpIndex = index;
                        return true;
                    }
                }
                return false;
            }
        }

        public bool TryFindBumpPrefixIndex(SemanticSection section, string? prefix, out int bumpIndex)
        {
            bumpIndex = -1;
            if (prefix is null || prefix.Length == 0) { return false; }
            for (int index = 0; index < section.Length; index++)
            {
                if (section[index].Value == prefix)
                {
                    bumpIndex = index + 1;
                    return true;
                }
            }
            return false;
        }
    }

    public static partial class SemanticSectionExtensions
    {
        public static SemanticSection ToReset(this SemanticSection self)
            => SemanticSectionBumper.Default.Reset(self);

        public static SemanticSection ToBumped(this SemanticSection self, EBumpAction bumpAction, int bumpIndex = -1, string? prefix = null)
            => SemanticSectionBumper.Default.Bump(self, bumpAction, bumpIndex, prefix);
    }
}
