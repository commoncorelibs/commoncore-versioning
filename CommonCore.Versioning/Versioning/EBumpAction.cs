﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable


namespace CommonCore.Versioning
{
    public enum EBumpAction
    {
        None = -1,
        Reset = 0,
        Remove = 1
    }
}
