﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;

namespace CommonCore.Versioning
{
    internal static class __InternalExtensions
    {
        public static IEnumerable<T> AndIf<T>(this IEnumerable<T> self, bool @if, T and)
        {
            Throw.If.Self.IsNull(self);
            foreach (var value in self) { yield return value; }
            if (@if) { yield return and; }
        }

        public static IEnumerable<T> AndIf<T>(this IEnumerable<T> self, bool @if, IEnumerable<T> and)
        {
            Throw.If.Self.IsNull(self);
            foreach (var value in self) { yield return value; }
            if (@if) { foreach (var value in and) { yield return value; } }
        }

        public static IEnumerable<T> ToEnumerable<T>(this IEnumerable<T> self)
            => self ?? Enumerable.Empty<T>();

        public static IEnumerable<SemanticToken> ToEnumerable(this SemanticSection self)
            => self;
    }
}
