﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections.Generic;
using CommonCore.Text.Numeric;

namespace CommonCore.Versioning
{
    /// <summary>
    /// The <see cref="Semantic"/> <c>static</c> <c>class</c> is the main entry point to the
    /// <see cref="CommonCore.Versioning"/> library.
    /// </summary>
    public static partial class Semantic
    {
        public static SemanticScanVersion Scan(string? version)
            => new(version);

        public static bool Validate(string? version, IList<Exception> errors)
            => Semantic.Rules.Validate(Semantic.Scan(version), errors);

        public static bool Validate(SemanticScanVersion scan, IList<Exception> errors)
            => Semantic.Rules.Validate(scan, errors);

        public static SemanticVersion Create(string? version)
            => SemanticVersion.Create(version);

        public static SemanticVersion Increment(string? version, ESemanticSectionType section)
            => SemanticVersion.Create(version);
    }

    //public readonly struct SemanticParseToken
    //{
    //    ESemanticSection SectionType { get; }

    //    int SectionIndex;

    //    ESemanticTokenType TokenType;

    //    CommonCore.Text.StringSpan Token;
    //}

    public static partial class Semantic
    {
    }

    public static class SemanticExtensions
    {
    }
}
