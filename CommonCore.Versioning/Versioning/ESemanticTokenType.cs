﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

namespace CommonCore.Versioning
{
    public enum ESemanticTokenType : int
    {
        None = -1,
        Unknown = 0,
        Alpha = 1,
        Numeric = 2,
        AlphaNumeric = 3,
        Other = 4
    }
}
