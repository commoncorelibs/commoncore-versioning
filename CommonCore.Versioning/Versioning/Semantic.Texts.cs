﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

namespace CommonCore.Versioning
{
    public static partial class Semantic
    {
        /// <summary>
        /// The <see cref="Semantic.Texts"/> static sub-class provides various
        /// strings needed by the rest of the library.
        /// </summary>
        public static class Texts
        {
            /// <summary>
            /// Gets the <see cref="char"/> symbol representing the beginning of a cycle section.
            /// </summary>
            public static char CycleSymbol { get; } = '-';

            /// <summary>
            /// Gets the <see cref="string"/> symbol representing the beginning of a cycle section.
            /// This is a <see cref="string"/> form of the <see cref="CycleSymbol"/> <see cref="char"/>.
            /// </summary>
            public static string CycleSymbolString { get; } = CycleSymbol.ToString();

            /// <summary>
            /// Gets the <see cref="char"/> symbol representing the beginning of a build section.
            /// </summary>
            public static char BuildSymbol { get; } = '+';

            /// <summary>
            /// Gets the <see cref="string"/> symbol representing the beginning of a build section.
            /// This is a <see cref="string"/> form of the <see cref="BuildSymbol"/> <see cref="char"/>.
            /// </summary>
            public static string BuildSymbolString { get; } = BuildSymbol.ToString();

            /// <summary>
            /// Gets the <see cref="char"/> symbol representing the delimiter used between section tokens.
            /// </summary>
            public static char DelimitSymbol { get; } = '.';

            /// <summary>
            /// Gets the <see cref="string"/> symbol representing the delimiter used between section tokens.
            /// This is a <see cref="string"/> form of the <see cref="DelimitSymbol"/> <see cref="char"/>.
            /// </summary>
            public static string DelimitSymbolString { get; } = DelimitSymbol.ToString();

            /// <summary>
            /// Gets the <see cref="char"/> representing the default numeric token.
            /// </summary>
            public static char DefaultToken { get; } = '0';

            /// <summary>
            /// Gets the <see cref="string"/> representing the default numeric token.
            /// This is a <see cref="string"/> form of the <see cref="DefaultToken"/> <see cref="char"/>.
            /// </summary>
            public static string DefaultTokenString { get; } = DefaultToken.ToString();

            /// <summary>
            /// Gets the lowercase names of the three semantic sections.
            /// </summary>
            public static string[] SectiontNames { get; } = new[] { "core", "cycle", "build" };

            /// <summary>
            /// Gets the uppercase titles of the three semantic sections.
            /// </summary>
            public static string[] SectionTitles { get; } = new[] { "Core", "Cycle", "Build" };

            /// <summary>
            /// Gets the format used to create validation error messages.
            /// Arguments are passed in the order rule title, rule message, subject title, subject text.
            /// </summary>
            public static string ErrorMessageFormat { get; } = "{0}: The {2} '{3}' is not valid. {1}";
        }
    }
}
