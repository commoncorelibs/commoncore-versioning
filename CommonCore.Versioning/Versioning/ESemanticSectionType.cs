﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

namespace CommonCore.Versioning
{
    public enum ESemanticSectionType : int
    {
        None = -1,
        Core = 0,
        Cycle = 1,
        Build = 2
    }
}
