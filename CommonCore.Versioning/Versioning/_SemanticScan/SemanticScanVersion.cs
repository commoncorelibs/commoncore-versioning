﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections.Immutable;

namespace CommonCore.Versioning
{
    public readonly struct SemanticScanVersion
    {
        public SemanticScanVersion(string? source)
        {
            if (source is null || source.Length == 0)
            {
                this.Source = string.Empty;
                this.Core = new(string.Empty, 0, 0, 0);
                this.Cycle = new(string.Empty, -1, -1, 0);
                this.Build = new(string.Empty, -1, -1, 0);
                return;
            }

            int cycleSymbolIndex = source.IndexOf(Semantic.Texts.CycleSymbol);
            int buildSymbolIndex = source.IndexOf(Semantic.Texts.BuildSymbol);
            // If the first cycle symbol is located after the build symbol,
            // then cycle is a false positive and the dash is part of the build,
            // so reset cycle to not being found.
            if (cycleSymbolIndex > -1 && buildSymbolIndex > -1 && cycleSymbolIndex > buildSymbolIndex)
            { cycleSymbolIndex = -1; }

            this.Source = source;

            {
                int coreSymbolIndex = 0;
                int coreLength = cycleSymbolIndex > -1 ? cycleSymbolIndex
                                : buildSymbolIndex > -1 ? buildSymbolIndex
                                : source.Length;
                int coreIndex = 0;
                string coreContent
                                = coreIndex > -1 && coreLength > 0
                                ? source.Substring(coreIndex, coreLength)
                                : string.Empty;
                this.Core = new(coreContent, coreSymbolIndex, coreIndex, coreLength);
            }

            {
                int cycleLength = cycleSymbolIndex > -1
                                ? buildSymbolIndex > -1
                                ? buildSymbolIndex - cycleSymbolIndex - 1
                                : source.Length - cycleSymbolIndex - 1
                                : 0;
                int cycleIndex = cycleLength < 1 ? -1 : cycleSymbolIndex + 1;
                string cycleContent
                                = cycleIndex > -1 && cycleLength > 0
                                ? source.Substring(cycleIndex, cycleLength)
                                : string.Empty;
                this.Cycle = new(cycleContent, cycleSymbolIndex, cycleIndex, cycleLength);
            }

            {
                int buildLength = buildSymbolIndex > -1
                                ? source.Length - buildSymbolIndex - 1
                                : 0;
                int buildIndex = buildLength < 1 ? -1 : buildSymbolIndex + 1;
                string buildContent
                                = buildIndex > -1 && buildLength > 0
                                ? source.Substring(buildIndex, buildLength)
                                : string.Empty;
                this.Build = new(buildContent, buildSymbolIndex, buildIndex, buildLength);
            }
        }

        public string Source { get; }

        public SemanticScanSection Core { get; }

        public SemanticScanSection Cycle { get; }

        public SemanticScanSection Build { get; }

        public SemanticScanSection this[ESemanticSectionType setIndex]
            => setIndex == ESemanticSectionType.Core ? this.Core
            : setIndex == ESemanticSectionType.Cycle ? this.Cycle
            : setIndex == ESemanticSectionType.Build ? this.Build
            : throw new IndexOutOfRangeException();

        /// <summary>
        /// Returns the most basic representation of a semantic version.
        /// The version parsed into three string arrays,
        /// with each array representing a section of the version, ie core, cycle, and build,
        /// and consisting of that section's dot-delimited tokens.
        /// If the cycle or build sections aren't present, then they will be
        /// represented by <c>null</c>.
        /// Absolutely no semantic validation is done by this method.
        /// </summary>
        /// <param name="core">The first semantic section of the version. This section will always be present, even if it is empty.</param>
        /// <param name="cycle">The second semantic section of the version. This section will be <c>null</c> if the cycle prefix symbol '-' is not found.</param>
        /// <param name="build">The third semantic section of the version. This section will be <c>null</c> if the build prefix symbol '+' is not found.</param>
        /// <returns>The most basic representation of a semantic version.</returns>
        public void Parse(out string[] core, out string[]? cycle, out string[]? build)
        {
            core = this.Core.ToTokenArray() ?? Array.Empty<string>();
            cycle = this.Cycle.ToTokenArray();
            build = this.Build.ToTokenArray();
        }

        public override string ToString() => this.Source;
    }
}
