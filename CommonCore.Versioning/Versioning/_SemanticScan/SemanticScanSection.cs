﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Linq;

namespace CommonCore.Versioning
{
    public readonly struct SemanticScanSection
    {
        public SemanticScanSection(string content, int symbolIndex, int sectionIndex, int sectionLength)
        {
            this.Content = content ?? string.Empty;
            this.SymbolIndex = symbolIndex;
            this.SectionIndex = sectionIndex;
            this.SectionLength = sectionLength;
        }

        public string Content { get; }

        public int SymbolIndex { get; }

        public int SectionIndex { get; }

        public int SectionLength { get; }

        public int TokenCount
            => this.HasSection
            ? this.Content.Count(c => c == Semantic.Texts.DelimitSymbol) + 1
            : 0;

        public bool HasSymbol => this.SymbolIndex > -1;

        public bool HasSection => this.SectionIndex > -1 && this.SectionLength > 0;

        //public bool Exists => this.HasSymbol && this.HasSection;

        public string[]? ToTokenArray()
            => this.HasSymbol
            ? this.HasSection
            ? this.Content.Split(Semantic.Texts.DelimitSymbol)
            : Array.Empty<string>()
            : null;

        public override string ToString() => this.Content;
    }
}
