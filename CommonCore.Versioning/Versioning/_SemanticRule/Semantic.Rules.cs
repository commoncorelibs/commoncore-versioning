﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using CommonCore.Text.Numeric;

namespace CommonCore.Versioning
{
    public static partial class Semantic
    {
        /// <summary>
        /// The <see cref="Semantic.Rules"/> static sub-class contains the rules used
        /// to validate semantic versions, sets, and tokens.
        /// </summary>
        public static class Rules
        {
            /// <summary>
            /// Gets the rule for validating that a semantic version:
            /// MUST contain at least the core set (X.Y.Z).
            /// </summary>
            public static SemanticRule<SemanticScanSection> CoreExistSetRule { get; }
                = new(nameof(CoreExistSetRule),
                    "MUST contain the core section (X.Y.Z).",
                    subject => !subject.HasSymbol);

            /// <summary>
            /// Gets the rule for validating that a semantic section of tokens:
            /// MUST contain at least one element.
            /// </summary>
            public static SemanticRule<SemanticScanSection> NotEmptySetRule { get; }
                = new SemanticRule<SemanticScanSection>(nameof(NotEmptySetRule),
                    "MUST contain at least one element.",
                    subject => !subject.HasSection);

            /// <summary>
            /// Gets the rule for validating that a semantic core section of tokens:
            /// MUST contain major, minor, and patch elements.
            /// </summary>
            public static SemanticRule<SemanticScanSection> CoreMinCountSetRule { get; }
                = new SemanticRule<SemanticScanSection>(nameof(CoreMinCountSetRule),
                    "MUST contain major, minor, and patch elements.",
                    subject => subject.TokenCount < 3);

            /// <summary>
            /// Gets the rule for validating that a semantic core section of tokens:
            /// MUST contain only major, minor, and patch elements.
            /// </summary>
            public static SemanticRule<SemanticScanSection> CoreMaxCountSetRule { get; }
                = new SemanticRule<SemanticScanSection>(nameof(CoreMaxCountSetRule),
                    "MUST contain only major, minor, and patch elements.",
                    subject => subject.TokenCount > 3);

            /// <summary>
            /// Gets the rule for validating that a semantic token:
            /// MUST NOT be empty.
            /// </summary>
            public static SemanticRule<string> NotEmptyRule { get; }
            = new SemanticRule<string>(nameof(NotEmptyRule),
                "MUST NOT be empty.",
                subject => subject is null || subject.Length == 0);

            /// <summary>
            /// Gets the rule for validating that a semantic token:
            /// MUST NOT contain leading zeros.
            /// </summary>
            public static SemanticRule<string> NotLeadingZeroRule { get; }
            = new SemanticRule<string>(nameof(NotLeadingZeroRule),
                "MUST NOT contain leading zeros.",
                subject => NumericTextHelper.CountLeadingZeros(subject) > 0);

            /// <summary>
            /// Gets the rule for validating that a semantic token:
            /// MUST contain only ASCII digits (0-9).
            /// </summary>
            public static SemanticRule<string> OnlyDigitsRule { get; }
            = new SemanticRule<string>(nameof(OnlyDigitsRule),
                "MUST contain only ASCII digits (0-9).",
                subject => !NumericTextHelper.IsNumeric(subject));

            /// <summary>
            /// Gets the rule for validating that a semantic token:
            /// MUST contain only ASCII digits, letters, and hyphens (0-9, a-z, A-Z, -).
            /// </summary>
            public static SemanticRule<string> OnlyAlphaNumericRule { get; }
            = new SemanticRule<string>(nameof(OnlyAlphaNumericRule),
                "MUST contain only ASCII digits, letters, and hyphens (0-9, a-z, A-Z, -).",
                subject => subject is null || subject.Length == 0 || !subject.All(c => NumericTextHelper.IsAlphaNumeric(c) || c is '-'));










            /// <summary>
            /// Validates the specified version based on the rules of semantic versions
            /// and fills the specified list with any violations.
            /// </summary>
            /// <param name="version">The semantic version to validate.</param>
            /// <param name="errors">The list to fill with any violations.</param>
            /// <exception cref="ArgumentNullException">If <paramref name="version"/> is <c>null</c>.</exception>
            /// <returns><c>true</c> if no violations were encountered; otherwise <c>false</c>.</returns>
            public static bool Validate(SemanticScanVersion version, IList<Exception> errors)
            {
                if (errors is null) { throw new ArgumentNullException(nameof(errors)); }

                int originalErrorCount = errors.Count;

                {
                    string sectionName = Semantic.Texts.SectiontNames[(int) ESemanticSectionType.Core];
                    SemanticScanSection section = version.Core;

                    {
                        string title = $"section:{sectionName}";
                        Semantic.Rules.CoreExistSetRule.Validate(errors, title, section);
                        Semantic.Rules.NotEmptySetRule.Validate(errors, title, section);
                        Semantic.Rules.CoreMinCountSetRule.Validate(errors, title, section);
                        Semantic.Rules.CoreMaxCountSetRule.Validate(errors, title, section);
                    }
                    string[]? tokens = section.ToTokenArray();
                    if (tokens is not null)
                    {
                        for (int index = 0; index < tokens.Length; index++)
                        {
                            string title = $"token:{sectionName}[{index}]";
                            string token = tokens[index];
                            Semantic.Rules.NotEmptyRule.Validate(errors, title, token);
                            Semantic.Rules.OnlyDigitsRule.Validate(errors, title, token);
                            Semantic.Rules.NotLeadingZeroRule.Validate(errors, title, token);
                        }
                    }
                }

                if (version.Cycle.HasSymbol)
                {
                    string sectionName = Semantic.Texts.SectiontNames[(int) ESemanticSectionType.Cycle];
                    SemanticScanSection section = version.Cycle;

                    Semantic.Rules.NotEmptySetRule.Validate(errors, $"section:{sectionName}", section);
                    string[]? tokens = section.ToTokenArray();
                    if (tokens is not null)
                    {
                        for (int index = 0; index < tokens.Length; index++)
                        {
                            string title = $"token:{sectionName}[{index}]";
                            string token = tokens[index];
                            Semantic.Rules.NotEmptyRule.Validate(errors, title, token);
                            if (!int.TryParse(token, NumberStyles.Any, CultureInfo.InvariantCulture.NumberFormat, out _))
                            {
                                Semantic.Rules.OnlyAlphaNumericRule.Validate(errors, title, token);
                            }
                            else
                            {
                                Semantic.Rules.OnlyDigitsRule.Validate(errors, title, token);
                                Semantic.Rules.NotLeadingZeroRule.Validate(errors, title, token);
                            }
                        }
                    }
                }

                if (version.Build.HasSymbol)
                {
                    string sectionName = Semantic.Texts.SectiontNames[(int) ESemanticSectionType.Build];
                    SemanticScanSection section = version.Build;

                    Semantic.Rules.NotEmptySetRule.Validate(errors, $"section:{sectionName}", section);
                    string[]? tokens = section.ToTokenArray();
                    if (tokens is not null)
                    {
                        for (int index = 0; index < tokens.Length; index++)
                        {
                            string title = $"token:{sectionName}[{index}]";
                            string token = tokens[index];
                            Semantic.Rules.NotEmptyRule.Validate(errors, title, token);
                            if (!int.TryParse(token, NumberStyles.Any, CultureInfo.InvariantCulture.NumberFormat, out _))
                            {
                                Semantic.Rules.OnlyAlphaNumericRule.Validate(errors, title, token);
                            }
                            else
                            {
                                Semantic.Rules.OnlyDigitsRule.Validate(errors, title, token);
                            }
                        }
                    }
                }

                return (errors.Count == originalErrorCount);
            }
        }
    }
}
