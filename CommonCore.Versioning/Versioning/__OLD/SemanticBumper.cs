﻿//#region Copyright (c) 2019 Jay Jeckel
//// Copyright (c) 2019 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Text;
//using BumpType = CommonCore.Versioning.SemanticVersionStruct.BumpType;

//namespace CommonCore.Versioning
//{
//    public class SemanticBumper
//    {
//        //public static SemanticToken BumpToken(SemanticToken original)
//        //    => (original.ContentType == SemanticTokenType.Numeric ? new SemanticToken(SemanticUtils.IncDigits(original.Content), original.ContentType) : throw new InvalidOperationException("Non-numeric tokens can not be bumped."));

//        //public static SemanticToken ResetToken(SemanticToken original)
//        //    => (original.ContentType == SemanticTokenType.Numeric ? SemanticToken.Zero : throw new InvalidOperationException("Non-numeric tokens can not be reset."));

//        //public static void Bump(SemanticTokenList tokens)
//        //{
//        //    int indexTarget = tokens.Count;
//        //    for (int index = tokens.Count - 1; index > -1 ; index--)
//        //    { if (tokens[index].ContentType == SemanticTokenType.Numeric) { indexTarget = index; break; } }
//        //    Bump(tokens, indexTarget);
//        //}

//        //public static void Bump(SemanticTokenList tokens, int index)
//        //{
//        //    if (index == tokens.Count) { tokens.Add(SemanticToken.Zero); }
//        //    else if (index < tokens.Count) { tokens[index] = BumpToken(tokens[index]); }
//        //    else { throw new ArgumentOutOfRangeException(nameof(index)); }
//        //}

//        //public static void Reset(SemanticTokenList tokens)
//        //{
//        //    for (int index = tokens.Count - 1; index > -1; index--)
//        //    { if (tokens[index].ContentType == SemanticTokenType.Numeric) { tokens[index] = ResetToken(tokens[index]); } }
//        //}

//        //public static void Reset(SemanticTokenList tokens, int index)
//        //{
//        //    if (index == tokens.Count) { tokens.Add(SemanticToken.Zero); }
//        //    else if (index < tokens.Count) { tokens[index] = ResetToken(tokens[index]); }
//        //    else { throw new ArgumentOutOfRangeException(nameof(index)); }
//        //}

//        //public bool BumpPatchIgnoreCycle { get; set; } = false;

//        //public bool BumpCycleIgnorePatch { get; set; } = false;

//        //public void BumpMajor(SemanticTokenTree tree) => Bump(tree.Core, 0);

//        //public void BumpMinor(SemanticTokenTree tree) => Bump(tree.Core, 1);

//        //public void BumpPatch(SemanticTokenTree tree, bool ignoreCycle = false)
//        //{
//        //    if (!tree.Cycle.Exists || ignoreCycle || this.BumpPatchIgnoreCycle)
//        //    { Bump(tree.Core, 2); }
//        //    tree.Cycle.Clear();
//        //}







//        //public SemanticVersion Bump(SemanticVersion original, BumpType bump)
//        //{
//        //    switch (bump)
//        //    {
//        //        case BumpType.Major: { return this.BumpMajor(original); }
//        //        case BumpType.Minor: { return this.BumpMinor(original); }
//        //        case BumpType.Patch: { return this.BumpPatch(original); }
//        //        case BumpType.Cycle: { return this.BumpCycle(original); }
//        //        case BumpType.Build: { return this.BumpBuild(original); }
//        //        default: { return original; }
//        //    }
//        //    //var major = ((bump & EBump.Major) == EBump.Major ? this.Major.GetBumped() : this.Major);
//        //    //var minor = ((bump & EBump.Minor) == EBump.Minor ? this.Minor.GetBumped() : this.Minor);
//        //    //var patch = ((bump & EBump.Patch) == EBump.Patch ? this.Patch.GetBumped() : this.Patch);
//        //    //var cycle = ((bump & EBump.Cycle) == EBump.Cycle ? this.Cycle.GetBumped(DefaultPrefixCycle) : this.Cycle);
//        //    //var build = ((bump & EBump.Build) == EBump.Build ? this.Build.GetBumped(DefaultPrefixBuild) : this.Build);
//        //    //return new SemanticVersion(major, minor, patch, cycle, build);
//        //}

//        //public SemanticVersion BumpMajor(SemanticVersion original) => new SemanticVersion(original.Major.GetBumped(), original.Minor.GetReset(), original.Patch.GetReset(), original.Cycle.GetReset(), original.Build.GetReset());

//        //public SemanticVersion BumpMinor(SemanticVersion original) => new SemanticVersion(original.Major, original.Minor.GetBumped(), original.Patch.GetReset(), original.Cycle.GetReset(), original.Build.GetReset());

//        //public SemanticVersion BumpPatch(SemanticVersion original, bool ignoreCycle = false)
//        //{
//        //    var patch = (ignoreCycle || this.BumpPatchIgnoreCycle || !original.Cycle.Exists ? original.Patch.GetBumped() : original.Patch);
//        //    return new SemanticVersion(original.Major, original.Minor, patch, original.Cycle.GetReset(), original.Build.GetReset());
//        //}

//        //public SemanticVersion BumpCycle(SemanticVersion original, string prefix = SemanticVersion.DefaultPrefixCycle, bool ignorePatch = false)
//        //{
//        //    var patch = (ignorePatch || this.BumpCycleIgnorePatch || original.Cycle.Exists ? original.Patch : original.Patch.GetBumped());
//        //    return new SemanticVersion(original.Major, original.Minor, patch, original.Cycle.GetBumped(prefix), original.Build.GetReset());
//        //}

//        //public SemanticVersion BumpBuild(SemanticVersion original, string prefix = SemanticVersion.DefaultPrefixBuild) => new SemanticVersion(original.Major, original.Minor, original.Patch, original.Cycle, original.Build.GetBumped(prefix));

//        ////public SemanticVersion SetCycle(string cycle) => new SemanticVersion(this.Major, this.Minor, this.Patch, new SemanticVersionMeta(DefaultTitleCycle, cycle, SemanticVersionMeta.EPrecedence.PreRelease), this.Build);

//        ////public SemanticVersion SetBuild(string build) => new SemanticVersion(this.Major, this.Minor, this.Patch, this.Cycle, new SemanticVersionMeta(DefaultTitleBuild, build, SemanticVersionMeta.EPrecedence.PostRelease));

//        ////public SemanticVersion SetMeta(string cycle, string build) => new SemanticVersion(this.Major, this.Minor, this.Patch, new SemanticVersionMeta(DefaultTitleCycle, cycle, SemanticVersionMeta.EPrecedence.PreRelease), new SemanticVersionMeta(DefaultTitleBuild, build, SemanticVersionMeta.EPrecedence.PostRelease));

//        ////public SemanticVersion StripCycle() => new SemanticVersion(this.Major, this.Minor, this.Patch, this.Cycle.GetReset(), this.Build);

//        ////public SemanticVersion StripBuild() => new SemanticVersion(this.Major, this.Minor, this.Patch, this.Cycle, this.Build.GetReset());

//        ////public SemanticVersion StripMeta() => new SemanticVersion(this.Major, this.Minor, this.Patch, this.Cycle.GetReset(), this.Build.GetReset());
//    }

//}
