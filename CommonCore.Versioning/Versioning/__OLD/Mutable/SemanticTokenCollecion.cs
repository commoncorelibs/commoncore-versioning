﻿//#region Copyright (c) 2022 Jay Jeckel
//// Copyright (c) 2022 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//#nullable enable

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using CommonCore.Collections;

//namespace CommonCore.Versioning.Mutable
//{
//    public partial class SemanticTokenCollecion : ObservableItemCollection<SemanticToken>
//    {
//        /// <summary>
//        /// Initializes a new instance of the <see cref="SemanticTokenCollecion"/> class.
//        /// </summary>
//        public SemanticTokenCollecion() : base() { }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="SemanticTokenCollecion"/> class
//        /// that contains elements copied from the specified list.
//        /// </summary>
//        /// <param name="items">The items used to initialize the collection.</param>
//        /// <exception cref="ArgumentNullException">If <paramref name="items"/> is <c>null</c>.</exception>
//        public SemanticTokenCollecion(IEnumerable<string> items)
//            : base(items?.Select(value => new SemanticToken(value))) { }

//        public void Add(string item)
//            => base.Add(new SemanticToken(item ?? throw Throw.Build.ArgumentNullException(nameof(item))));

//        /// <summary>
//        /// Adds the specified <paramref name="items"/> to the collection.
//        /// </summary>
//        /// <param name="items">The items to add to the collection.</param>
//        /// <exception cref="ArgumentNullException">If <paramref name="items"/> is <c>null</c>.</exception>
//        public void AddRange(IEnumerable<string> items)
//        {
//            if (items is null) { return; }
//            foreach (string item in items) { this.Add(item); }
//        }

//        /// <summary>
//        /// Adds the specified <paramref name="items"/> to the collection.
//        /// </summary>
//        /// <param name="items">The items to add to the collection.</param>
//        /// <exception cref="ArgumentNullException">If <paramref name="items"/> is <c>null</c>.</exception>
//        public void AddRange(IEnumerable<SemanticToken> items)
//        {
//            if (items is null) { return; }
//            foreach (SemanticToken item in items) { this.Add(item); }
//        }
//    }

//    public partial class SemanticTokenCollecion : IReadOnlyList<string>
//    {
//        string IReadOnlyList<string>.this[int index] => this[index];

//        IEnumerator<string> IEnumerable<string>.GetEnumerator() => ((IEnumerable<SemanticToken>)this).Select(token => token.Value).GetEnumerator();
//    }
//}
