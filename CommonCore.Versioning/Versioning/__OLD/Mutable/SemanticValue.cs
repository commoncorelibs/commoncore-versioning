﻿//#region Copyright (c) 2022 Jay Jeckel
//// Copyright (c) 2022 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//#nullable enable

//using System;
//using System.Collections.Generic;
//using System.Collections.Immutable;
//using System.Diagnostics;
//using CommonCore.Versioning.Mutable;

//namespace CommonCore.Versioning
//{
//    [DebuggerDisplay("{ToString(),raw}")]
//    public readonly ref partial struct SemanticValue
//    {
//        public static SemanticValue Create(ReadOnlySpan<char> version)
//        {
//            Semantic.Scan(version).Parse(out var core, out var cycle, out var build);
//            return SemanticValue.Create(core, cycle, build);
//        }

//        public static SemanticValue Create(string[]? core, string[]? cycle, string[]? build)
//            => new(
//                ImmutableArray.Create(core ?? Array.Empty<string>()),
//                ImmutableArray.Create(cycle ?? Array.Empty<string>()),
//                ImmutableArray.Create(build ?? Array.Empty<string>()));

//        public static SemanticValue Create(IEnumerable<string>? core, IEnumerable<string>? cycle, IEnumerable<string>? build)
//            => new(
//                ImmutableArray.CreateRange(core ?? Array.Empty<string>()),
//                ImmutableArray.CreateRange(cycle ?? Array.Empty<string>()),
//                ImmutableArray.CreateRange(build ?? Array.Empty<string>()));

//        public SemanticValue(ImmutableArray<string> core, ImmutableArray<string> cycle, ImmutableArray<string> build)
//        {
//            this.Core = core;
//            this.Cycle = cycle;
//            this.Build = build;
//        }

//        public bool IsEmpty => this.Core.IsEmpty && this.Cycle.IsEmpty && this.Build.IsEmpty;

//        public ImmutableArray<string> Core { get; init; }

//        public ImmutableArray<string> Cycle { get; init; }

//        public ImmutableArray<string> Build { get; init; }

//        public string this[ESemanticSection setIndex, int idIndex]
//            => this[setIndex][idIndex];

//        public ImmutableArray<string> this[ESemanticSection setIndex]
//            => setIndex == ESemanticSection.Core ? this.Core
//            : setIndex == ESemanticSection.Cycle ? this.Cycle
//            : setIndex == ESemanticSection.Build ? this.Build
//            : throw new IndexOutOfRangeException();

//        public override string ToString()
//            => SemanticValueFormatter.Default.ToString(this);
//    }

//    public readonly ref partial struct SemanticValue// : IEquatable<SemanticVersion>
//    {
//        public override int GetHashCode()
//            => SemanticValueEqualityComparer.Default.GetHashCode(this);

//        public override bool Equals(object? obj)
//            => throw new NotSupportedException();

//        public bool Equals(SemanticValue other)
//            => SemanticValueEqualityComparer.Default.Equals(this, other);

//        public static bool operator ==(SemanticValue left, SemanticValue right)
//            => SemanticValueEqualityComparer.Default.Equals(left, right);

//        public static bool operator !=(SemanticValue left, SemanticValue right)
//            => !SemanticValueEqualityComparer.Default.Equals(left, right);
//    }

//    public readonly ref partial struct SemanticValue// : IComparable, IComparable<SemanticVersion>
//    {
//        public int CompareTo(object? obj)
//            => throw new NotSupportedException();

//        public int CompareTo(SemanticValue other)
//            => SemanticValueComparer.Default.Compare(this, other);

//        public static bool operator >(SemanticValue left, SemanticValue right)
//            => SemanticValueComparer.Default.Compare(left, right) > 0;

//        public static bool operator <(SemanticValue left, SemanticValue right)
//            => SemanticValueComparer.Default.Compare(left, right) < 0;

//        public static bool operator >=(SemanticValue left, SemanticValue right)
//            => SemanticValueComparer.Default.Compare(left, right) >= 0;

//        public static bool operator <=(SemanticValue left, SemanticValue right)
//            => SemanticValueComparer.Default.Compare(left, right) <= 0;
//    }

//    public class SemanticValueComparer// : Comparer<SemanticValue>
//    {
//        public static new SemanticValueComparer Default { get; } = new SemanticValueComparer();

//        public /*override*/ int Compare(SemanticValue left, SemanticValue right)
//            => Semantic.Lex.Compare(left.Core, left.Cycle, left.Build, right.Core, right.Cycle, right.Build);
//    }

//    public class SemanticValueEqualityComparer// : EqualityComparer<SemanticValue>
//    {
//        public static new SemanticValueEqualityComparer Default { get; } = new SemanticValueEqualityComparer();

//        public /*override*/ bool Equals(SemanticValue left, SemanticValue right)
//            => Semantic.Lex.Equate(left.Core, left.Cycle, left.Build, right.Core, right.Cycle, right.Build);

//        public /*override*/ int GetHashCode(SemanticValue version)
//            => HashCode.Combine(version.Core, version.Cycle, version.Build);
//    }

//    public class SemanticValueFormatter
//    {
//        public static SemanticValueFormatter Default { get; } = new SemanticValueFormatter();

//        public string ToString(SemanticValue version)
//            => Semantic.Lex.Join(version.Core, version.Cycle, version.Build);
//    }

//    public static class SemanticValueExtensions
//    {
//        public static SemanticModel ToModel(this SemanticValue self)
//            => SemanticModel.Create(self);
//    }
//}
