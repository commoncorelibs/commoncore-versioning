﻿//#region Copyright (c) 2022 Jay Jeckel
//// Copyright (c) 2022 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//#nullable enable

//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Diagnostics.CodeAnalysis;

//namespace CommonCore.Versioning.Mutable
//{
//    [DebuggerDisplay("{ToString(),raw}")]
//    public partial class SemanticModel
//    {
//        public static SemanticModel Create(ReadOnlySpan<char> version)
//        {
//            Semantic.Scan(version).Parse(out var core, out var cycle, out var build);
//            return new(core, cycle, build);
//        }

//        public static SemanticModel Create(SemanticVersion version)
//            => new(version.Core, version.Cycle, version.Build);

//        public static SemanticModel Create(SemanticValue version)
//            => new(version.Core, version.Cycle, version.Build);

//        public SemanticModel(IEnumerable<string> core, IEnumerable<string> cycle, IEnumerable<string> build)
//        {
//            this.Core = new(core);
//            this.Cycle = new(cycle);
//            this.Build = new(build);
//        }

//        public bool IsEmpty => this.Core.Count == 0 && this.Cycle.Count == 0 && this.Build.Count == 0;

//        public SemanticTokenCollecion Core { get; }

//        public SemanticTokenCollecion Cycle { get; }

//        public SemanticTokenCollecion Build { get; }

//        public string this[ESemanticSection setIndex, int idIndex]
//            => this[setIndex][idIndex];

//        public SemanticTokenCollecion this[ESemanticSection setIndex]
//            => setIndex == ESemanticSection.Core ? this.Core
//            : setIndex == ESemanticSection.Cycle ? this.Cycle
//            : setIndex == ESemanticSection.Build ? this.Build
//            : throw new IndexOutOfRangeException();

//        public override string ToString()
//            => SemanticModelFormatter.Default.ToString(this);
//    }

//    public partial class SemanticModel : IEquatable<SemanticModel>
//    {
//        public override int GetHashCode()
//            => SemanticModelEqualityComparer.Default.GetHashCode(this);

//        public bool Equals(object? obj)
//            => obj is SemanticModel other && SemanticModelEqualityComparer.Default.Equals(this, other);

//        public bool Equals(SemanticModel other)
//            => SemanticModelEqualityComparer.Default.Equals(this, other);

//        public static bool operator ==(SemanticModel left, SemanticModel right)
//            => SemanticModelEqualityComparer.Default.Equals(left, right);

//        public static bool operator !=(SemanticModel left, SemanticModel right)
//            => !SemanticModelEqualityComparer.Default.Equals(left, right);
//    }

//    public partial class SemanticModel : IComparable, IComparable<SemanticModel>
//    {
//        public int CompareTo(object? obj)
//            => obj is SemanticModel other ? SemanticModelComparer.Default.Compare(this, other) : 0;

//        public int CompareTo(SemanticModel other)
//            => SemanticModelComparer.Default.Compare(this, other);

//        public static bool operator >(SemanticModel left, SemanticModel right)
//            => SemanticModelComparer.Default.Compare(left, right) > 0;

//        public static bool operator <(SemanticModel left, SemanticModel right)
//            => SemanticModelComparer.Default.Compare(left, right) < 0;

//        public static bool operator >=(SemanticModel left, SemanticModel right)
//            => SemanticModelComparer.Default.Compare(left, right) >= 0;

//        public static bool operator <=(SemanticModel left, SemanticModel right)
//            => SemanticModelComparer.Default.Compare(left, right) <= 0;
//    }

//    public partial class SemanticModel : IEnumerable<SemanticTokenCollecion>
//    {
//        public IEnumerator<SemanticTokenCollecion> GetEnumerator()
//        {
//            yield return this.Core;
//            yield return this.Cycle;
//            yield return this.Build;
//        }

//        [ExcludeFromCodeCoverage]
//        IEnumerator IEnumerable.GetEnumerator()
//            => this.GetEnumerator();
//    }
//    public class SemanticModelComparer : Comparer<SemanticModel>
//    {
//        public static new SemanticModelComparer Default { get; } = new SemanticModelComparer();

//        public override int Compare(SemanticModel left, SemanticModel right)
//            => Semantic.Lex.Compare(left.Core, left.Cycle, left.Build, right.Core, right.Cycle, right.Build);
//    }

//    public class SemanticModelEqualityComparer : EqualityComparer<SemanticModel>
//    {
//        public static new SemanticModelEqualityComparer Default { get; } = new SemanticModelEqualityComparer();

//        public override bool Equals(SemanticModel left, SemanticModel right)
//            => Semantic.Lex.Equate(left.Core, left.Cycle, left.Build, right.Core, right.Cycle, right.Build);

//        public override int GetHashCode(SemanticModel version)
//            => HashCode.Combine(version.Core, version.Cycle, version.Build);
//    }

//    public class SemanticModelFormatter
//    {
//        public static SemanticModelFormatter Default { get; } = new SemanticModelFormatter();

//        public string ToString(SemanticModel model)
//            => Semantic.Lex.Join(model.Core, model.Cycle, model.Build);
//    }

//    public static class SemanticModelExtensions
//    {
//        public static SemanticValue ToValue(this SemanticModel self)
//        {
//            Throw.If.Self.IsNull(self);
//            return SemanticValue.Create(self.Core, self.Cycle, self.Build);
//        }

//        public static SemanticVersion ToVersion(this SemanticModel self)
//        {
//            Throw.If.Self.IsNull(self);
//            return SemanticVersion.Create(self.Core, self.Cycle, self.Build);
//        }

//        public static SemanticModel SetFrom(this SemanticModel self, SemanticValue version)
//        {
//            Throw.If.Self.IsNull(self);
//            return self.SetCore(version.Core).SetCycle(version.Core).SetBuild(version.Core);
//        }

//        public static SemanticModel SetFrom(this SemanticModel self, SemanticValue version, ESemanticSection setIndex)
//        {
//            Throw.If.Self.IsNull(self);
//            self.Set(version[setIndex], setIndex);
//            return self;
//        }

//        public static SemanticModel SetFrom(this SemanticModel self, SemanticVersion version)
//        {
//            Throw.If.Self.IsNull(self);
//            return self.SetCore(version.Core).SetCycle(version.Core).SetBuild(version.Core);
//        }

//        public static SemanticModel SetFrom(this SemanticModel self, SemanticVersion version, ESemanticSection setIndex)
//        {
//            Throw.If.Self.IsNull(self);
//            self.Set(version[setIndex], setIndex);
//            return self;
//        }

//        public static SemanticModel Set(this SemanticModel self, IEnumerable<string> set, ESemanticSection setIndex)
//        {
//            Throw.If.Self.IsNull(self);
//            switch (setIndex)
//            {
//                case ESemanticSection.Core: { self.SetCore(set); return self; }
//                case ESemanticSection.Cycle: { self.SetCycle(set); return self; }
//                case ESemanticSection.Build: { self.SetBuild(set); return self; }
//                default: { throw new IndexOutOfRangeException(); }
//            }
//        }

//        public static SemanticModel SetCore(this SemanticModel self, IEnumerable<string> set)
//        {
//            Throw.If.Self.IsNull(self);
//            self.Core.Clear();
//            self.Core.AddRange(set);
//            return self;
//        }

//        public static SemanticModel SetCycle(this SemanticModel self, IEnumerable<string> set)
//        {
//            Throw.If.Self.IsNull(self);
//            self.Cycle.Clear();
//            self.Cycle.AddRange(set);
//            return self;
//        }

//        public static SemanticModel SetBuild(this SemanticModel self, IEnumerable<string> set)
//        {
//            Throw.If.Self.IsNull(self);
//            self.Build.Clear();
//            self.Build.AddRange(set);
//            return self;
//        }
//    }
//}
