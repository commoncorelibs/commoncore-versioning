﻿//#region Copyright (c) 2022 Jay Jeckel
//// Copyright (c) 2022 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//#nullable enable

//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Diagnostics.CodeAnalysis;
//using CommonCore.Text.Numeric;

//namespace CommonCore.Versioning.Mutable
//{
//    [DebuggerDisplay("{ToString(),raw}")]
//    public partial class SemanticToken : AObservableObject
//    {
//        public SemanticToken(string value)
//        {
//            Throw.If.Arg.IsNullOrEmpty(nameof(value), value);
//            this._token = value;
//        }

//        public string Value
//        {
//            get => this._token;
//            set => this.RaiseIfPropertyChanged(ref this._token, value ?? throw Throw.Build.ArgumentNullException(nameof(value)));
//        }
//        private string _token;

//        public int Length => this.Value.Length;

//        public char this[int index]
//            => this.Value[index];

//        public override string ToString()
//            => SemanticTokenFormatter.Default.ToString(this);

//        public static implicit operator string(SemanticToken token)
//            => SemanticTokenFormatter.Default.ToString(token);

//        public static implicit operator ReadOnlySpan<char>(SemanticToken token)
//            => token.Value.AsSpan();
//    }

//    public partial class SemanticToken : IEquatable<SemanticToken>
//    {
//        public override int GetHashCode()
//            => SemanticTokenEqualityComparer.Default.GetHashCode(this);

//        public bool Equals(object? obj)
//            => obj is SemanticToken other && SemanticTokenEqualityComparer.Default.Equals(this, other);

//        public bool Equals(SemanticToken other)
//            => SemanticTokenEqualityComparer.Default.Equals(this, other);

//        public static bool operator ==(SemanticToken left, SemanticToken right)
//            => SemanticTokenEqualityComparer.Default.Equals(left, right);

//        public static bool operator !=(SemanticToken left, SemanticToken right)
//            => !SemanticTokenEqualityComparer.Default.Equals(left, right);
//    }

//    public partial class SemanticToken : IComparable, IComparable<SemanticToken>
//    {
//        public int CompareTo(object? obj)
//            => obj is SemanticToken other ? SemanticTokenComparer.Default.Compare(this, other) : 0;

//        public int CompareTo(SemanticToken other)
//            => SemanticTokenComparer.Default.Compare(this, other);

//        public static bool operator >(SemanticToken left, SemanticToken right)
//            => SemanticTokenComparer.Default.Compare(left, right) > 0;

//        public static bool operator <(SemanticToken left, SemanticToken right)
//            => SemanticTokenComparer.Default.Compare(left, right) < 0;

//        public static bool operator >=(SemanticToken left, SemanticToken right)
//            => SemanticTokenComparer.Default.Compare(left, right) >= 0;

//        public static bool operator <=(SemanticToken left, SemanticToken right)
//            => SemanticTokenComparer.Default.Compare(left, right) <= 0;
//    }

//    public partial class SemanticToken : IEnumerable<char>
//    {
//        public IEnumerator<char> GetEnumerator()
//            => this.Value.GetEnumerator();

//        [ExcludeFromCodeCoverage]
//        IEnumerator IEnumerable.GetEnumerator()
//            => ((IEnumerable) this.Value).GetEnumerator();
//    }

//    public class SemanticTokenComparer : Comparer<SemanticToken>
//    {
//        public static new SemanticTokenComparer Default { get; } = new SemanticTokenComparer();

//        public override int Compare(SemanticToken left, SemanticToken right)
//            => string.Compare(left.Value, right.Value);
//    }

//    public class SemanticTokenEqualityComparer : EqualityComparer<SemanticToken>
//    {
//        public static new SemanticTokenEqualityComparer Default { get; } = new SemanticTokenEqualityComparer();

//        public override bool Equals(SemanticToken left, SemanticToken right)
//            => string.Equals(left.Value, right.Value);

//        public override int GetHashCode(SemanticToken token)
//            => HashCode.Combine(token.Value);
//    }

//    public class SemanticTokenFormatter
//    {
//        public static SemanticTokenFormatter Default { get; } = new SemanticTokenFormatter();

//        public string ToString(SemanticToken token)
//            => token.Value;
//    }

//    public static class SemanticTokenExtensions
//    {
//        /// <summary>
//        /// Returns <c>true</c> if <paramref name="chars"/> contains only digits; otherwise <c>false</c>.
//        /// </summary>
//        /// <param name="chars">The chars to compare.</param>
//        /// <returns><c>true</c> if <paramref name="chars"/> contains only digits; otherwise <c>false</c>.</returns>
//        public static bool IsNumeric(this SemanticToken self)
//            => TextMathHelper.IsNumeric(self);

//        /// <summary>
//        /// Returns <c>true</c> if <paramref name="chars"/> contains only hyphens, letters, or digits; otherwise <c>false</c>.
//        /// </summary>
//        /// <param name="chars">The chars to compare.</param>
//        /// <returns><c>true</c> if <paramref name="chars"/> contains only hyphens, letters, or digits; otherwise <c>false</c>.</returns>
//        public static bool IsAlphaNumeric(this SemanticToken self)
//        {
//            if (self is null) { return false; }
//            for (int index = 0; index < self.Length; index++)
//            {
//                char c = self[index];
//                if (!(TextMathHelper.IsNumeric(c) || c is '-' or >= 'a' and <= 'z' or >= 'A' and <= 'Z')) { return false; }
//            }
//            return true;
//        }
//    }
//}
