﻿//#region Copyright (c) 2019 Jay Jeckel
//// Copyright (c) 2019 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//using System;
//using System.Collections.Generic;
//using System.Text;

//namespace CommonCore.Versioning
//{
//    public class SemanticVersion// : ISemanticVersion
//    {
//        public static SemanticVersion From(ReadOnlySpan<char> input)
//        {
//            var data = Semantic.Data.Parse(input);
//            var errors = Semantic.Data.Validate(data);
//            if (errors.Count > 0) { throw new AggregateException(errors); }
//            var version = new SemanticVersion() { Data = data };
//            return version;
//        }

//        public SemanticData Data { get; set; }

//        public int Major { get => Semantic.Utils.ParseNumber(this.Data[0][0]); set => this.Data[0][0] = value.ToString(); }

//        public int Minor { get => Semantic.Utils.ParseNumber(this.Data[0][1]); set => this.Data[0][1] = value.ToString(); }

//        public int Patch { get => Semantic.Utils.ParseNumber(this.Data[0][2]); set => this.Data[0][2] = value.ToString(); }

//        public List<string> Cycle { get => this.Data[1]; set => this.Data[1] = value; }

//        public List<string> Build { get => this.Data[2]; set => this.Data[2] = value; }
//    }

//    public class SemanticVersionComparer : Comparer<SemanticVersion>
//    {
//        public static SemanticVersionComparer Default { get; } = new SemanticVersionComparer();

//        public override int Compare(SemanticVersion left, SemanticVersion right)
//            => Semantic.Data.Compare(left.Data, right.Data);
//    }
//}
