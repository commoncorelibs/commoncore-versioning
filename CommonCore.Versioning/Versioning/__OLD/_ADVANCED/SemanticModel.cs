﻿//#region Copyright (c) 2019 Jay Jeckel
//// Copyright (c) 2019 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace CommonCore.Versioning
//{
//    /// <summary>
//    /// The <see cref="SemanticModel"/> class is a very thin wrapper around three lists
//    /// of strings representing the most basic framework of a Semantic Version.
//    /// At its heart, the semantic model can be thought of as a length-three array of
//    /// string lists, the lists representing the three semantic sets, core, cycle, and build.
//    /// The structure is not constrained by any further semantic rules, as that is better
//    /// left up to a validation step.
//    /// </summary>
//    public class SemanticModel : IReadOnlyList<List<string>>
//    {
//        public SemanticModel() { }

//        public int Count { get; } = Semantic.Lex.SemanticSetCount;

//        public List<string> Core { get; set; }

//        public List<string> Cycle { get; set; }

//        public List<string> Build { get; set; }

//        //public bool CycleExists => this.Cycle != null;

//        //public bool CycleEmpty => !this.CycleExists || this.Cycle.Count == 0;

//        //public bool BuildExists => this.Build != null;

//        //public bool BuildEmpty => !this.BuildExists || this.Build.Count == 0;

//        public List<string> this[int index]
//        {
//            get
//            {
//                if (Semantic.Lex.CoreIndex.Equals(index)) { return this.Core; }
//                else if (Semantic.Lex.CycleIndex.Equals(index)) { return this.Cycle; }
//                else if (Semantic.Lex.BuildIndex.Equals(index)) { return this.Build; }
//                else { throw new ArgumentOutOfRangeException(nameof(index)); }
//            }
//            set
//            {
//                if (Semantic.Lex.CoreIndex.Equals(index)) { this.Core = value; }
//                else if (Semantic.Lex.CycleIndex.Equals(index)) { this.Cycle = value; }
//                else if (Semantic.Lex.BuildIndex.Equals(index)) { this.Build = value; }
//                else { throw new ArgumentOutOfRangeException(nameof(index)); }
//            }
//        }

//        public string this[int iset, int index]
//        {
//            get => this[iset][index];
//            set => this[iset][index] = value;
//        }

//        public IEnumerator<List<string>> GetEnumerator() { yield return this.Core; yield return this.Cycle; yield return this.Build; }

//        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

//        public override string ToString() => Semantic.Lex.Join(this);
//    }

//    public class SemanticModelEqualityComparer : EqualityComparer<SemanticModel>
//    {
//        public static SemanticModelEqualityComparer Default { get; } = new SemanticModelEqualityComparer();

//        public override bool Equals(SemanticModel left, SemanticModel right)
//            => Semantic.Lex.Compare(left, right) == 0;

//        public override int GetHashCode(SemanticModel model) => model?.GetHashCode() ?? 0;
//    }

//    public class SemanticModelComparer : Comparer<SemanticModel>
//    {
//        public static SemanticModelComparer Default { get; } = new SemanticModelComparer();

//        public override int Compare(SemanticModel left, SemanticModel right)
//            => Semantic.Lex.Compare(left, right);
//    }
//}
