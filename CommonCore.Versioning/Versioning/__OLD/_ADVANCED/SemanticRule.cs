﻿//#region Copyright (c) 2019 Jay Jeckel
//// Copyright (c) 2019 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//using System;
//using System.Collections.Generic;

//namespace CommonCore.Versioning
//{
//    /// <summary>
//    /// The <see cref="SemanticRule{TSubject}"/> class provides a mechanism for representing
//    /// a condition, validating an object against that condition, and populating notice of
//    /// violations back to the calling code.
//    /// </summary>
//    /// <typeparam name="TSubject"></typeparam>
//    public class SemanticRule<TSubject>
//    {
//        /// <summary>
//        /// Initializes a new instance of the <see cref="SemanticRule{TSubject}"/> class.
//        /// </summary>
//        /// <param name="title">The title of the rule.</param>
//        /// <param name="message">The display message describing the rule.</param>
//        /// <param name="violationFunc">
//        /// A delegate that accepts a <typeparamref name="TSubject"/> object and
//        /// returns <c>true</c> if the passed object violates the rule.
//        /// </param>
//        /// <param name="subjectToStringFunc">
//        /// A delegate that accepts a <typeparamref name="TSubject"/> object and
//        /// returns a <see cref="string"/> representation of the passed object for display.
//        /// </param>
//        /// <exception cref="ArgumentNullException">
//        /// If <paramref name="title"/>, <paramref name="message"/>, or
//        /// <paramref name="violationFunc"/> are <c>null</c>.
//        /// </exception>
//        public SemanticRule(string title, string message,
//            Func<TSubject, bool> violationFunc,
//            Func<TSubject, string> subjectToStringFunc = null)
//        {
//            this.Title = title ?? throw new ArgumentNullException(nameof(title));
//            this.Message = message ?? throw new ArgumentNullException(nameof(message));
//            this.ViolationFunc = violationFunc ?? throw new ArgumentNullException(nameof(violationFunc));
//            this.SubjectToStringFunc = subjectToStringFunc;
//        }

//        /// <summary>
//        /// Gets the title of the rule.
//        /// </summary>
//        public string Title { get; }

//        /// <summary>
//        /// Gets the display message describing the rule.
//        /// </summary>
//        public string Message { get; }

//        /// <summary>
//        /// Gets the delegate that returns <c>true</c> if the passed object violates the rule.
//        /// </summary>
//        public Func<TSubject, bool> ViolationFunc { get; }

//        /// <summary>
//        /// Gets the delegate that returns a <see cref="string"/> representation of the passed object for display.
//        /// The delegate is only invoked if a passed object violates the rule.
//        /// </summary>
//        public Func<TSubject, string> SubjectToStringFunc { get; }

//        /// <summary>
//        /// Determines if the specified <paramref name="subject"/> violates the rule
//        /// according to the <see cref="ViolationFunc"/> delegate.
//        /// </summary>
//        /// <param name="subject">The object to validate.</param>
//        /// <returns><c>true</c> if the <paramref name="subject"/> violates the rule; otherwise <c>false</c>.</returns>
//        public virtual bool IsViolation(TSubject subject) => this.ViolationFunc(subject);

//        /// <summary>
//        /// Determines if the specified <paramref name="subject"/> violates the rule
//        /// according to the <see cref="ViolationFunc"/> delegate.
//        /// If a violation is encountered, then a display message is built using the
//        /// <paramref name="subjectTitle"/> and added to the specified <paramref name="errors"/>
//        /// list in the form of a <see cref="FormatException"/>.
//        /// </summary>
//        /// <param name="errors"></param>
//        /// <param name="subjectTitle"></param>
//        /// <param name="subject"></param>
//        /// <returns></returns>
//        public virtual bool Validate(IList<Exception> errors, string subjectTitle, TSubject subject)
//        {
//            if (!this.IsViolation(subject)) { return true; }
//            else
//            {
//                string subjectText;
//                if (this.SubjectToStringFunc is null) { subjectText = subject?.ToString(); }
//                else { subjectText = this.SubjectToStringFunc.Invoke(subject); }
//                var message = string.Format(Semantic.Texts.ErrorMessageFormat, this.Title, this.Message, subjectTitle, subjectText);
//                errors.Add(new FormatException(message));
//                return false;
//            }
//        }
//    }
//}
