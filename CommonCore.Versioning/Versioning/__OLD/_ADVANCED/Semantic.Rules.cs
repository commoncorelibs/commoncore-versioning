﻿//#region Copyright (c) 2019 Jay Jeckel
//// Copyright (c) 2019 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//using ReadOnlyModel = System.Collections.Generic.IReadOnlyList<System.Collections.Generic.IReadOnlyList<string>>;
//using ReadOnlySet = System.Collections.Generic.IReadOnlyList<string>;

//namespace CommonCore.Versioning
//{
//    public static partial class Semantic
//    {
//        /// <summary>
//        /// The <see cref="Semantic.Rules"/> static sub-class contains the rules used
//        /// to validate semantic models, sets, and tokens.
//        /// </summary>
//        public static class Rules
//        {
//            /// <summary>
//            /// Gets the rule for validating that a semantic model:
//            /// MUST contain at least the core set (X.Y.Z).
//            /// </summary>
//            public static SemanticRule<ReadOnlyModel> CoreExistModelRule { get; }
//                = new SemanticRule<ReadOnlyModel>(nameof(CoreExistModelRule),
//                    "MUST contain the core set (X.Y.Z).",
//                    subject => subject is null || subject.Count == 0 || subject[0] is null,
//                    Semantic.Lex.Join);

//            /// <summary>
//            /// Gets the rule for validating that a semantic model:
//            /// MUST contain only the core, cycle, and build sets (X.Y.Z-CYCLE+BUILD).
//            /// </summary>
//            public static SemanticRule<ReadOnlyModel> MaxCountModelRule { get; }
//                = new SemanticRule<ReadOnlyModel>(nameof(MaxCountModelRule),
//                    "MUST contain only the core, cycle, and build sets (X.Y.Z-CYCLE+BUILD).",
//                    subject => (subject?.Count ?? 0) > Semantic.Lex.SemanticSetCount,
//                    Semantic.Lex.Join);

//            /// <summary>
//            /// Gets the rule for validating that a semantic set of tokens:
//            /// MUST contain at least one element.
//            /// </summary>
//            public static SemanticRule<ReadOnlySet> NotEmptySetRule { get; }
//                = new SemanticRule<ReadOnlySet>(nameof(NotEmptySetRule),
//                    "MUST contain at least one element.",
//                    subject => (subject?.Count ?? 0) == 0,
//                    Semantic.Lex.Join);

//            /// <summary>
//            /// Gets the rule for validating that a semantic core set of tokens:
//            /// MUST contain major, minor, and patch elements.
//            /// </summary>
//            public static SemanticRule<ReadOnlySet> CoreMinCountSetRule { get; }
//                = new SemanticRule<ReadOnlySet>(nameof(CoreMinCountSetRule),
//                    "MUST contain major, minor, and patch elements.",
//                    subject => (subject?.Count ?? 0) < 3,
//                    Semantic.Lex.Join);

//            /// <summary>
//            /// Gets the rule for validating that a semantic core set of tokens:
//            /// MUST contain only major, minor, and patch elements.
//            /// </summary>
//            public static SemanticRule<ReadOnlySet> CoreMaxCountSetRule { get; }
//                = new SemanticRule<ReadOnlySet>(nameof(CoreMaxCountSetRule),
//                    "MUST contain only major, minor, and patch elements.",
//                    subject => (subject?.Count ?? 0) > 3,
//                    Semantic.Lex.Join);

//            /// <summary>
//            /// Gets the rule for validating that a semantic token:
//            /// MUST NOT be empty.
//            /// </summary>
//            public static SemanticRule<string> NotEmptyRule { get; }
//            = new SemanticRule<string>(nameof(NotEmptyRule),
//                "MUST NOT be empty.",
//                subject => (subject?.Length ?? 0) == 0);

//            /// <summary>
//            /// Gets the rule for validating that a semantic token:
//            /// MUST NOT contain leading zeros.
//            /// </summary>
//            public static SemanticRule<string> NotLeadingZeroRule { get; }
//            = new SemanticRule<string>(nameof(NotLeadingZeroRule),
//                "MUST NOT contain leading zeros.",
//                subject => Semantic.Utils.CountLeadingZeros(subject) > 0);

//            /// <summary>
//            /// Gets the rule for validating that a semantic token:
//            /// MUST contain only ASCII digits (0-9).
//            /// </summary>
//            public static SemanticRule<string> OnlyDigitsRule { get; }
//            = new SemanticRule<string>(nameof(OnlyDigitsRule),
//                "MUST contain only ASCII digits (0-9).",
//                subject => !Semantic.Utils.HasOnlyDigits(subject));

//            /// <summary>
//            /// Gets the rule for validating that a semantic token:
//            /// MUST contain only ASCII digits, letters, and hyphens (0-9, a-z, A-Z, -).
//            /// </summary>
//            public static SemanticRule<string> OnlyAlphaNumericRule { get; }
//            = new SemanticRule<string>(nameof(OnlyAlphaNumericRule),
//                "MUST contain only ASCII digits, letters, and hyphens (0-9, a-z, A-Z, -).",
//                subject => !Semantic.Utils.HasOnlyAlphaNumeric(subject));
//        }
//    }
//}
