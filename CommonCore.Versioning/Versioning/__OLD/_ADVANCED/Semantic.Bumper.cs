﻿//#region Copyright (c) 2019 Jay Jeckel
//// Copyright (c) 2019 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using ReadOnlyModel = System.Collections.Generic.IReadOnlyList<System.Collections.Generic.IReadOnlyList<string>>;
//using ReadOnlySet = System.Collections.Generic.IReadOnlyList<string>;
//using ReadOnlyToken = System.ReadOnlySpan<char>;
//using WriteSet = System.Collections.Generic.IList<string>;

//namespace CommonCore.Versioning
//{
//    public static partial class Semantic
//    {
//        public static class Bumper
//        {
//            // Define the bumping tokens.
//            public static char OpenSymbol { get; } = '[';
//            public static char CloseSymbol { get; } = ']';
//            public static char IncSymbol { get; } = '^';
//            public static char ResetSymbol { get; } = '_';
//            public static char SkipSymbol { get; } = '#';

//            public static string MajorPattern { get; } = "^._._";
//            public static string MinorPattern { get; } = "#.^._";
//            public static string PatchPattern { get; } = "#.#.^";
//            public static string CyclePattern { get; } = "#.#.#-^";
//            public static string BuildPattern { get; } = "#.#.#-#+^";

//            // Define a token list for validation.
//            public static char[] Symbols { get; } = new[]
//            {
//                IncSymbol, ResetSymbol, SkipSymbol,
//                Semantic.Lex.CycleSymbol, Semantic.Lex.BuildSymbol, Semantic.Lex.DelimitSymbol
//            };

//            public static void Normalize(WriteSet set)
//            {
//                // A null set means no set, so do nothing.
//                if (set is null) { return; }
//                // An empty set is meaningless, but does equate to a one-element skip set.
//                if (set.Count == 0) { set.Add(SkipSymbol.ToString()); }
//                // Normalize the tokens.
//                // Before the increment token, replace empty tokens with skip.
//                // After the increment token, replace empty tokens with reset.
//                // Non-empty tokens are ignored.
//                bool reset = false;
//                for (int index = 0; index < set.Count; index++)
//                {
//                    string token = set[index];
//                    if (token.Length == 0) { set[index] = (reset ? ResetSymbol : SkipSymbol).ToString(); }
//                    else if (token[0] == IncSymbol) { reset = true; }
//                    else { }
//                }
//            }

//            public static void ValidateBumpPattern(string input, out string pattern)
//            {
//                bool open = input[0] == OpenSymbol;
//                bool close = input[^1] == CloseSymbol;
//                if (open != close)
//                { throw new FormatException($"The bump pattern MUST use or omit both the open and close symbols '[]'."); }

//                pattern = input[(open ? 1 : 0)..(close ? ^2 : ^1)];

//                // Do simple validation so we can assume valid tokens.
//                if (pattern.Count(c => Symbols.Contains(c)) != pattern.Length)
//                { throw new FormatException($"The bump pattern MUST contain only allowed symbols '^_#-+.'."); }
//            }

//            public static void ValidateBumpPattern(ReadOnlyModel model)
//            {
//                // Do simple validation so we can assume valid tokens.
//                //if (input.Count(c => BumpSymbols.Contains(c)) != input.Length)
//                //{ throw new FormatException($"The bump pattern MUST contain only allowed characters '[^_#-+.]'."); }

//                for (int iset = 0; iset < model.Count; iset++)
//                {
//                    var set = model[iset];
//                    for (int index = 0; index < set.Count; index++)
//                    {
//                        string token = set[index];
//                        //if (token.Length > 1)
//                        //{ throw new FormatException($"The token '{token}' MUST be empty or contain only one valid token '[^_#-+.]'."); }
//                    }
//                }
//            }
//        }
//    }
//}
