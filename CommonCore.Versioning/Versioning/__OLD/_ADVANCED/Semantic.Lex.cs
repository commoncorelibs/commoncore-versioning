﻿//#region Copyright (c) 2019 Jay Jeckel
//// Copyright (c) 2019 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//using System;
//using System.Collections.Generic;
//using System.Globalization;
//using System.Linq;
//using ReadOnlyModel = System.Collections.Generic.IReadOnlyList<System.Collections.Generic.IReadOnlyList<string>>;
//using ReadOnlySet = System.Collections.Generic.IReadOnlyList<string>;
//using ReadOnlyToken = System.ReadOnlySpan<char>;

//namespace CommonCore.Versioning
//{
//    public static partial class Semantic
//    {
//        /// <summary>
//        /// The <see cref="Semantic.Lex"/> static sub-class provides the low level logic
//        /// for handling semantic input, models, sets, and tokens. The lexing methods are
//        /// all non-destructive and treat all inputs as immutable.
//        /// </summary>
//        /// <remarks>
//        /// Methods in this class should not accept <see cref="SemanticModel"/> objects directly
//        /// as it is mutable and all these methods should be non-destructive.
//        /// Instead, they should take interfaces that are more appropriate to their operation,
//        /// such as <c>IReadOnlyList&lt;IReadOnlyList&lt;string&gt;&gt;</c>
//        /// or <c>IReadOnlyList&lt;IList&lt;string&gt;&gt;</c>.
//        /// </remarks>
//        public static class Lex
//        {
//            /// <summary>
//            /// Gets the number of sets contained by the semantic model.
//            /// </summary>
//            public static int SemanticSetCount { get; } = 3;

//            /// <summary>
//            /// Gets the index of the core set in the semantic model.
//            /// </summary>
//            public static Index CoreIndex { get; } = 0;

//            /// <summary>
//            /// Gets the index of the cycle set in the semantic model.
//            /// </summary>
//            public static Index CycleIndex { get; } = 1;

//            /// <summary>
//            /// Gets the index of the build set in the semantic model.
//            /// </summary>
//            public static Index BuildIndex { get; } = 2;

//            /// <summary>
//            /// Gets the <see cref="char"/> symbol representing the beginning of a cycle set.
//            /// </summary>
//            public static char CycleSymbol { get; } = '-';

//            /// <summary>
//            /// Gets the <see cref="char"/> symbol representing the beginning of a build set.
//            /// </summary>
//            public static char BuildSymbol { get; } = '+';

//            /// <summary>
//            /// Gets the <see cref="char"/> symbol representing the delimiter
//            /// used between semantic set tokens.
//            /// </summary>
//            public static char DelimitSymbol { get; } = '.';

//            /// <summary>
//            /// Gets the <see cref="string"/> representing the default numeric token.
//            /// </summary>
//            public static string DefaultToken { get; } = "0";

//            /// <summary>
//            /// Joins the specified semantic model into a semantic version string
//            /// and returns the result as a new <see cref="string"/>.
//            /// </summary>
//            /// <param name="model">The semantic model to join together.</param>
//            /// <returns>A semantic version string.</returns>
//            public static string Join(ReadOnlyModel model)
//            {
//                if (model is null || model.Count == 0) { return string.Empty; }

//                // 7 string allocations
//                {
//                    var version = string.Empty;
//                    if (model.Count > CoreIndex.Value && model[CoreIndex] != null)
//                    { version += Semantic.Lex.Join(model[CoreIndex]); }
//                    if (model.Count > CycleIndex.Value && model[CycleIndex] != null)
//                    { version += CycleSymbol + Semantic.Lex.Join(model[CycleIndex]); }
//                    if (model.Count > BuildIndex.Value && model[BuildIndex] != null)
//                    { version += BuildSymbol + Semantic.Lex.Join(model[BuildIndex]); }
//                    return version;
//                }

//                // 1 StringBuilder allocation.
//                //{
//                //    var output = new System.Text.StringBuilder();
//                //    if (model.Count > CoreIndex.Value && model[CoreIndex] != null)
//                //    { output.AppendJoin(Semantic.Lex.DelimitSymbol, model[CoreIndex]); }
//                //    if (model.Count > CycleIndex.Value && model[CycleIndex] != null)
//                //    { output.Append(Semantic.Lex.CycleSymbol).AppendJoin(Semantic.Lex.DelimitSymbol, model[CycleIndex]); }
//                //    if (model.Count > BuildIndex.Value && model[BuildIndex] != null)
//                //    { output.Append(Semantic.Lex.BuildSymbol).AppendJoin(Semantic.Lex.DelimitSymbol, model[BuildIndex]); }
//                //    return output.ToString();
//                //}

//                // 1 string allocation.
//                //{
//                //    int length = 0;
//                //    for (int iset = 0; iset < model.Count; iset++)
//                //    {
//                //        var set = model[iset];
//                //        if (set != null)
//                //        {
//                //            if (iset > 0) { length += 1; }
//                //            if (set.Count > 0)
//                //            { length += set.Sum(token => token.Length) + (set.Count - 1); }
//                //        }
//                //    }

//                //    if (length == 0) { return string.Empty; }
//                //    return string.Create(length, model, (dest, state) =>
//                //    {
//                //        int idest = 0;
//                //        for (int iset = 0; iset < model.Count; iset++)
//                //        {
//                //            var set = model[iset];
//                //            if (set != null)
//                //            {
//                //                if (iset == 1) { dest[idest++] = Semantic.Lex.CycleSymbol; }
//                //                else if (iset > 1) { dest[idest++] = Semantic.Lex.BuildSymbol; }
//                //                if (set.Count > 0)
//                //                { idest += dest.CopyJoin(idest, Semantic.Lex.DelimitSymbol, set); }
//                //            }
//                //        }
//                //    });
//                //}
//            }

//            /// <summary>
//            /// Joins the specified semantic set of tokens using a dot delimiter and returns
//            /// the result as a new <see cref="string"/>.
//            /// </summary>
//            /// <param name="set">The semantic set to join together.</param>
//            /// <returns>A dot-delimited string of the set tokens.</returns>
//            public static string Join(ReadOnlySet set)
//#if NETSTANDARD2_0
//                => (set is null || set.Count == 0 ? string.Empty : string.Join(DelimitSymbol.ToString(), set));
//#else
//                => (set is null || set.Count == 0 ? string.Empty : string.Join(DelimitSymbol, set));
//#endif

//            /// <summary>
//            /// Returns the most basic representation of a semantic version.
//            /// The version parsed into a length-three array of string arrays,
//            /// with each sub-array consisting of the results of splitting the
//            /// dot-delimited set of identifiers.
//            /// If the cycle or build sets aren't present, then they will be
//            /// represented by <c>null</c>.
//            /// Absolutely no semantic validation is done by this method.
//            /// </summary>
//            /// <param name="input">The semantic version string to parse.</param>
//            /// <returns>The most basic representation of a semantic version.</returns>
//            public static List<string>[] Scan(string input) => (input is null ? null : Scan(input.AsSpan()));

//            /// <summary>
//            /// Returns the most basic representation of a semantic version.
//            /// See <see cref="Semantic.Lex.Scan(string)"/> for details.
//            /// </summary>
//            /// <param name="input">The semantic version string to parse.</param>
//            /// <returns>The most basic representation of a semantic version.</returns>
//            public static List<string>[] Scan(ReadOnlyToken input)
//            {
//                int indexBuild = input.IndexOf('+');
//                // If the first cycle symbol is located after the build symbol,
//                // then cycle is a false positive and the dash is part of the build,
//                // so reset cycle to not being found.
//                int indexCycle = input.IndexOf('-');
//                if (indexCycle > -1 && indexBuild > -1 && indexCycle > indexBuild) { indexCycle = -1; }

//                // Define a couple helper flags to simplify subsequent logic.
//                bool hasBuild = indexBuild > -1;
//                bool hasCycle = indexCycle > -1;

//                // Calculate lengths of the three sets.
//                int lenBuild = (hasBuild ? input.Length - indexBuild : 0);
//                int lenCycle = (hasCycle ? input.Length - lenBuild - indexCycle : 0);
//                int lenCore = input.Length - lenBuild - lenCycle;

//                // There is always a core set, even if it is empty.
//                var listCore = input.Slice(0, lenCore).ToString().Split(DelimitSymbol).ToList();
//                // Have to offset the indexes and lengths to remove the prefix character.
//                // If the set wasn't found, then the list has to be null to differentiate
//                // between a malformed empty set and one that isn't present in the input.
//                var listCycle = (hasCycle ? input.Slice(indexCycle + 1, lenCycle - 1).ToString().Split(DelimitSymbol).ToList() : null);
//                var listBuild = (hasBuild ? input.Slice(indexBuild + 1, lenBuild - 1).ToString().Split(DelimitSymbol).ToList() : null);

//                return new List<string>[] { listCore, listCycle, listBuild };
//            }

//            /// <summary>
//            /// Validates the specified model based on the rules of semantic versions
//            /// and returns a list containing any violations.
//            /// </summary>
//            /// <param name="model">The semantic model to validate.</param>
//            /// <exception cref="ArgumentNullException">If <paramref name="model"/> is <c>null</c>.</exception>
//            /// <returns>A list containing any violations.</returns>
//            public static List<Exception> Validate(ReadOnlyModel model)
//            {
//                if (model is null) { throw new ArgumentNullException(nameof(model)); }
//                var errors = new List<Exception>();
//                Semantic.Lex.Validate(model, errors);
//                return errors;
//            }

//            /// <summary>
//            /// Validates the specified model based on the rules of semantic versions
//            /// and fills the specified list with any violations.
//            /// </summary>
//            /// <param name="model">The semantic model to validate.</param>
//            /// <param name="errors">The list to fill with any violations.</param>
//            /// <exception cref="ArgumentNullException">If <paramref name="model"/> is <c>null</c>.</exception>
//            /// <returns><c>true</c> if no violations were encountered; otherwise <c>false</c>.</returns>
//            public static bool Validate(ReadOnlyModel model, IList<Exception> errors)
//            {
//                if (model is null) { throw new ArgumentNullException(nameof(model)); }
//                if (errors is null) { throw new ArgumentNullException(nameof(errors)); }

//                int originalErrorCount = errors.Count;
//                // Validate the model.
//                {
//                    var title = "model";
//                    Semantic.Rules.CoreExistModelRule.Validate(errors, title, model);
//                    Semantic.Rules.MaxCountModelRule.Validate(errors, title, model);
//                }

//                for (int iset = 0; iset < model.Count; iset++)
//                {
//                    var elements = model[iset];
//                    if (elements == null) { continue; }

//                    // Validate the set.
//                    {
//                        var title = $"set:{Semantic.Texts.SetTitles[iset]}";
//                        Semantic.Rules.NotEmptySetRule.Validate(errors, title, elements);
//                        if (CoreIndex.Equals(iset))
//                        {
//                            Semantic.Rules.CoreMinCountSetRule.Validate(errors, title, elements);
//                            Semantic.Rules.CoreMaxCountSetRule.Validate(errors, title, elements);
//                        }
//                    }

//                    // Validate the tokens.
//                    {
//                        var title = $"token:{Semantic.Texts.SetTitles[iset]}";
//                        for (int index = 0; index < elements.Count; index++)
//                        {
//                            var subtitle = $"{title}[{index}]";
//                            var element = elements[index];
//                            Semantic.Rules.NotEmptyRule.Validate(errors, subtitle, element);
//                            if (iset > CoreIndex.Value && !int.TryParse(element, NumberStyles.Any, CultureInfo.InvariantCulture.NumberFormat, out _))
//                            { Semantic.Rules.OnlyAlphaNumericRule.Validate(errors, subtitle, element); }
//                            else
//                            {
//                                Semantic.Rules.OnlyDigitsRule.Validate(errors, subtitle, element);
//                                if (iset < BuildIndex.Value) { Semantic.Rules.NotLeadingZeroRule.Validate(errors, subtitle, element); }
//                            }
//                        }
//                    }
//                }
//                return (errors.Count == originalErrorCount);
//            }

//            /// <summary>
//            /// Compares two semantic models for precedence, returning <c>-1</c>, <c>0</c>, or <c>1</c>.
//            /// This method assumes both models are semantically valid.
//            /// </summary>
//            /// <param name="left">The first model to be compared.</param>
//            /// <param name="right">The other model to be compared.</param>
//            /// <returns>
//            /// <c>-1</c> if <paramref name="left"/> is less than <paramref name="right"/>;
//            /// <c>1</c> if <paramref name="left"/> is greater than <paramref name="right"/>;
//            /// otherwise <c>0</c> if <paramref name="left"/> and <paramref name="right"/> are equal.
//            /// </returns>
//            public static int Compare(ReadOnlyModel left, ReadOnlyModel right)
//            {
//                if (left is null && right is null) { return 0; }
//                else if (left is null) { return -1; }
//                else if (right is null) { return 1; }
//                else
//                {
//                    for (int index = 0; index < 3; index++)
//                    {
//                        int result = Compare(left[index], right[index], index == 1);
//                        if (result != 0) { return result; }
//                    }
//                    return 0;
//                }
//            }

//            /// <summary>
//            /// Compares two semantic sets for precedence, returning <c>-1</c>, <c>0</c>, or <c>1</c>.
//            /// This method assumes both sets are semantically valid.
//            /// By default, null and empty sets have lower precedence than non-null and non-empty sets, respectively.
//            /// Specifying <paramref name="invertEmptyPrecedence"/> as <c>true</c> will reverse this behavior, making
//            /// null sets higher than non-null sets and empty sets higher than non-empty sets.
//            /// </summary>
//            /// <param name="left">The first set to be compared.</param>
//            /// <param name="right">The other set to be compared.</param>
//            /// <param name="invertEmptyPrecedence">The value indicating that empty sets are higher precedence than non-empty sets.</param>
//            /// <returns>
//            /// <c>-1</c> if <paramref name="left"/> is less than <paramref name="right"/>;
//            /// <c>1</c> if <paramref name="left"/> is greater than <paramref name="right"/>;
//            /// otherwise <c>0</c> if <paramref name="left"/> and <paramref name="right"/> are equal.
//            /// </returns>
//            public static int Compare(ReadOnlySet left, ReadOnlySet right, bool invertEmptyPrecedence = false)
//            {
//                // If both are absent (ie null) or the same object, then equal;
//                // otherwise absent is lower than non-absent,
//                // unless empty precedence is inverted.
//                if (object.ReferenceEquals(left, right)) { return 0; }
//                else if (left is null) { return invertEmptyPrecedence ? 1 : -1; }
//                else if (right is null) { return invertEmptyPrecedence ? -1 : 1; }
//                // If both are empty, then equal;
//                // otherwise empty is lower than non-empty,
//                // unless empty precedence is inverted.
//                else if (left.Count == 0 && right.Count == 0) { return 0; }
//                else if (left.Count == 0) { return invertEmptyPrecedence ? 1 : -1; }
//                else if (right.Count == 0) { return invertEmptyPrecedence ? -1 : 1; }
//                else
//                {
//                    (int fallback, int count)
//                        // If left is smaller than right, then lower if elements are equal.
//                        = left.Count < right.Count ? (-1, left.Count)
//                        // If left is larger than right, then higher if elements are equal.
//                        : left.Count > right.Count ? (1, right.Count)
//                        // If same size, then equal if elements are equal.
//                        : (0, left.Count);
//                    // The first non-equal token decides the precedence;
//                    // otherwise the fallback precedence is used.
//                    for (int index = 0; index < count; index++)
//                    {
//                        int result = Lex.Compare(left[index].AsSpan(), right[index].AsSpan());
//                        if (result != 0) { return result; }
//                    }
//                    return fallback;
//                }
//            }

//            /// <summary>
//            /// Compares two semantic tokens for precedence, returning <c>-1</c>, <c>0</c>, or <c>1</c>.
//            /// This method assumes both tokens are semantically valid.
//            /// </summary>
//            /// <param name="left">The first token to be compared.</param>
//            /// <param name="right">The other token to be compared.</param>
//            /// <returns>
//            /// <c>-1</c> if <paramref name="left"/> is less than <paramref name="right"/>;
//            /// <c>1</c> if <paramref name="left"/> is greater than <paramref name="right"/>;
//            /// otherwise <c>0</c> if <paramref name="left"/> and <paramref name="right"/> are equal.
//            /// </returns>
//            public static int Compare(ReadOnlyToken left, ReadOnlyToken right)
//            {
//                // Empty tokens are equal;
//                // otherwise empty is lower than non-empty.
//                if (left.IsEmpty && right.IsEmpty) { return 0; }
//                else if (left.IsEmpty) { return -1; }
//                else if (right.IsEmpty) { return 1; }
//                else
//                {
//                    bool numLeft = Semantic.Utils.HasOnlyDigits(left);
//                    bool numRight = Semantic.Utils.HasOnlyDigits(right);
//                    // If both are digits, then compare numerically;
//                    // otherwise digits are lower than text;
//                    // otherwise compare as normal text.
//                    if (numLeft && numRight) { return Semantic.Utils.CompareAssumeOnlyDigits(left, right); }
//                    else if (numLeft && !numRight) { return -1; }
//                    else if (!numLeft && numRight) { return 1; }
//                    // SequenceCompareTo can throw values outside -1, 0, and 1, so need to clamp it.
//                    else { return Semantic.Utils.Clamp(left.SequenceCompareTo(right), -1, 1); }
//                }
//            }

//            /// <summary>
//            /// Parses a semantic compliant numeric <paramref name="token"/> to a <see cref="int"/> if possible;
//            /// otherwise return the specified <paramref name="fallback"/> value.
//            /// The specified <paramref name="token"/> are parsed according to
//            /// <see cref="NumberStyles.None"/> and <see cref="CultureInfo.InvariantCulture"/>.
//            /// </summary>
//            /// <param name="token">The semantic token to parse.</param>
//            /// <param name="fallback">The value to return if parsing fails.</param>
//            /// <returns>A parsed integer; otherwise the fallback value.</returns>
//            public static int ParseNumber(ReadOnlyToken token, int fallback = 0)
//#if NETSTANDARD2_0
//                => int.TryParse(token.ToString(), NumberStyles.None, CultureInfo.InvariantCulture.NumberFormat, out int value) ? value : fallback;
//#else
//                => int.TryParse(token, NumberStyles.None, CultureInfo.InvariantCulture.NumberFormat, out int value) ? value : fallback;
//#endif

//            /// <summary>
//            /// Returns the <see cref="Index"/> of the last semantic token within the specified
//            /// <paramref name="set"/> that can be bumped. If no appropriate token is found,
//            /// then <see cref="Index.End"/> will be returned.
//            /// </summary>
//            /// <param name="set">The semantic set to search.</param>
//            /// <returns>The last token in the set that can be bumped; otherwise <see cref="Index.End"/>.</returns>
//            public static Index GetBumpIndex(ReadOnlySet set)
//            {
//                if (set == null || set.Count == 0) { return Index.End; }
//                for (int index = set.Count - 1; index > -1; index--)
//                { if (Semantic.Utils.HasOnlyDigits(set[index].AsSpan())) { return index; } }
//                return Index.End;
//            }
//        }
//    }
//}
