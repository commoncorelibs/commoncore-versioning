﻿//#region Copyright (c) 2019 Jay Jeckel
//// Copyright (c) 2019 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using ReadOnlyModel = System.Collections.Generic.IReadOnlyList<System.Collections.Generic.IReadOnlyList<string>>;
//using ReadOnlySet = System.Collections.Generic.IReadOnlyList<string>;
//using ReadOnlyToken = System.ReadOnlySpan<char>;
//using WriteSet = System.Collections.Generic.IList<string>;

//namespace CommonCore.Versioning
//{
//    /// <summary>
//    /// The <see cref="Semantic"/> static class is the main entry point to the
//    /// <see cref="CommonCore.Versioning"/> library.
//    /// 
//    /// The <see cref="Semantic"/> class is composed of several static sub-classes
//    /// organizing the various data and logic.
//    /// </summary>
//    public static partial class Semantic
//    {
//        public static (SemanticModel model, List<Exception> errors) Parse(string input)
//        {
//            var raw = Semantic.Lex.Scan(input);
//            var model = new SemanticModel()
//            {
//                Core = raw[Semantic.Lex.CoreIndex],
//                Cycle = raw[Semantic.Lex.CycleIndex],
//                Build = raw[Semantic.Lex.BuildIndex]
//            };
//            var errors = Semantic.Lex.Validate(model);
//            return (model, errors);
//        }

//        public static void Bump(SemanticModel model, string input)
//        {
//            Semantic.Bumper.ValidateBumpPattern(input, out string pattern);
//            var raw = Semantic.Lex.Scan(pattern);
//            var bump = new SemanticModel()
//            {
//                Core = raw[Semantic.Lex.CoreIndex],
//                Cycle = raw[Semantic.Lex.CycleIndex],
//                Build = raw[Semantic.Lex.BuildIndex]
//            };

//            if (bump.Core is null) { model.Core = null; }
//            else
//            {
//                if (model.Core is null) { model.Core = new List<string>(); }
//                BumpSet(model.Core, bump.Core);
//            }

//            if (bump.Cycle is null) { model.Cycle = null; }
//            else
//            {
//                if (model.Cycle is null) { model.Cycle = new List<string>(); }
//                BumpSet(model.Cycle, bump.Cycle);
//            }

//            if (bump.Build is null) { model.Build = null; }
//            else
//            {
//                if (model.Build is null) { model.Build = new List<string>(); }
//                BumpSet(model.Build, bump.Build);
//            }
//        }

//        public static void BumpSet(WriteSet modelSet, WriteSet bumpSet)
//        {
//            if (modelSet is null) { throw new ArgumentNullException(nameof(modelSet)); }
//            if (bumpSet is null) { throw new ArgumentNullException(nameof(bumpSet)); }

//            Semantic.Bumper.Normalize(bumpSet);
//            if (bumpSet.Count > modelSet.Count)
//            { Semantic.Utils.EnsureListCount(modelSet, bumpSet.Count, Semantic.Lex.DefaultToken, false); }
//            else if (bumpSet.Count < modelSet.Count)
//            { Semantic.Utils.EnsureListCount(bumpSet, modelSet.Count, Semantic.Bumper.SkipSymbol.ToString(), true); }

//            for (int index = 0; index < bumpSet.Count; index++)
//            {
//                if (bumpSet[index][0] == Semantic.Bumper.SkipSymbol)
//                { }
//                else if (bumpSet[index][0] == Semantic.Bumper.IncSymbol)
//                { Semantic.Op.Increment(modelSet, index); }
//                else if (bumpSet[index][0] == Semantic.Bumper.ResetSymbol)
//                { Semantic.Op.Reset(modelSet, index); }
//            }
//        }
//    }

//    public static partial class Semantic
//    {
//        //public static Range
//    }

//    //public static class MiscExtensions
//    //{
//    //    public static int Copy(this Span<char> self, int index, ReadOnlySpan<char> source)
//    //    {
//    //        if (self.IsEmpty || source.IsEmpty) { return 0; }
//    //        for (int idest = index, isource = 0; idest < self.Length && isource < source.Length; idest++, isource++)
//    //        { self[index] = source[isource]; }
//    //        return source.Length;
//    //    }

//    //    public static int CopyJoin(this Span<char> self, int index, char delimiter, IEnumerable<string> values)
//    //    {
//    //        if (self.IsEmpty || values == null) { return 0; }
//    //        bool rest = false;
//    //        int length = 0;
//    //        foreach (var value in values)
//    //        {
//    //            if (rest) { self[index++] = delimiter; length += 1; } else { rest = true; }
//    //            length = self.Copy(index, value);
//    //            index += length;
//    //        }
//    //        return length;
//    //    }
//    //}
//}
