﻿//#region Copyright (c) 2019 Jay Jeckel
//// Copyright (c) 2019 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//using System;
//using System.Collections.Generic;

//namespace CommonCore.Versioning
//{
//    public static partial class Semantic
//    {
//        /// <summary>
//        /// The <see cref="Semantic.Utils"/> static sub-class provides general methods
//        /// needed by the rest of the library. While mostly boilerplate, there is some
//        /// interesting code within this class; specifically the
//        /// <see cref="Semantic.Utils.CompareAssumeOnlyDigits"/> and
//        /// <see cref="Semantic.Utils.IncrementAssumeOnlyDigits"/> methods underpin
//        /// the fundamental operations of this library, sorting and bumping semantic versions.
//        /// </summary>
//        public static class Utils
//        {
//            /// <summary>
//            /// Restricts the specified <paramref name="value"/> to the inclusive range
//            /// <paramref name="min"/> to <paramref name="max"/>.
//            /// </summary>
//            /// <param name="value">The value to restrict.</param>
//            /// <param name="min">The minimum value.</param>
//            /// <param name="max">The maximum value.</param>
//            /// <returns>The value restricted to the inclusive range min to max.</returns>
//            public static int Clamp(int value, int min, int max) => value < min ? min : value > max ? max : value;

//            public static void EnsureListCount(IList<string> list, int minCount, string filler, bool prepend = false)
//            {
//                if (list is null) { throw new ArgumentNullException(nameof(list)); }
//                if (minCount < 0) { throw new ArgumentOutOfRangeException(nameof(minCount)); }
//                if (list.Count < minCount)
//                {
//                    int diff = minCount - list.Count;
//                    for (int _ = 0; _ < diff; _++)
//                    { if (prepend) { list.Insert(0, filler); } else { list.Add(filler); } }
//                }
//            }

//            /// <summary>
//            /// Returns <c>true</c> if <paramref name="c"/> is a digit
//            /// (0123456789);
//            /// otherwise <c>false</c>.
//            /// </summary>
//            /// <param name="c">The character to compare.</param>
//            /// <returns><c>true</c> if <paramref name="c"/> is a digit; otherwise <c>false</c>.</returns>
//            public static bool IsDigit(char c) => (c >= '0' && c <= '9');

//            /// <summary>
//            /// Returns <c>true</c> if <paramref name="c"/> is a hyphen or letter
//            /// (- or abcdefghijklmnopqrstuvwxyz or ABCDEFGHIJKLMNOPQRSTUVWXYZ);
//            /// otherwise <c>false</c>.
//            /// </summary>
//            /// <param name="c">The character to compare.</param>
//            /// <returns><c>true</c> if <paramref name="c"/> is a hyphen or letter; otherwise <c>false</c>.</returns>
//            public static bool IsAlpha(char c) => (c == '-' || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'));

//            /// <summary>
//            /// Returns <c>true</c> if <paramref name="c"/> is a hyphen, letter, or digit
//            /// (- or abcdefghijklmnopqrstuvwxyz or ABCDEFGHIJKLMNOPQRSTUVWXYZ or 0123456789);
//            /// otherwise <c>false</c>.
//            /// </summary>
//            /// <param name="c">The character to compare.</param>
//            /// <returns><c>true</c> if <paramref name="c"/> is a hyphen, letter, or digit; otherwise <c>false</c>.</returns>
//            public static bool IsAlphaNumeric(char c) => IsDigit(c) || IsAlpha(c);

//            /// <summary>
//            /// Returns <c>true</c> if <paramref name="chars"/> contains only digits; otherwise <c>false</c>.
//            /// </summary>
//            /// <param name="chars">The chars to compare.</param>
//            /// <returns><c>true</c> if <paramref name="chars"/> contains only digits; otherwise <c>false</c>.</returns>
//            public static bool HasOnlyDigits(ReadOnlySpan<char> chars)
//            {
//                if (chars.IsEmpty) { return false; }
//                for (int index = 0; index < chars.Length; index++)
//                { if (!IsDigit(chars[index])) { return false; } }
//                return true;
//            }

//            public static bool HasOnlyDigits(string chars) => HasOnlyDigits(chars.AsSpan());

//            /// <summary>
//            /// Returns <c>true</c> if <paramref name="chars"/> contains only hyphens, letters, or digits; otherwise <c>false</c>.
//            /// </summary>
//            /// <param name="chars">The chars to compare.</param>
//            /// <returns><c>true</c> if <paramref name="chars"/> contains only hyphens, letters, or digits; otherwise <c>false</c>.</returns>
//            public static bool HasOnlyAlphaNumeric(ReadOnlySpan<char> chars)
//            {
//                if (chars.IsEmpty) { return false; }
//                for (int index = 0; index < chars.Length; index++)
//                { if (!IsAlphaNumeric(chars[index])) { return false; } }
//                return true;
//            }

//            public static bool HasOnlyAlphaNumeric(string chars) => HasOnlyAlphaNumeric(chars.AsSpan());

//            /// <summary>
//            /// Counts the number of sequential zeros that start the specified <paramref name="chars"/>.
//            /// The last character is never counted since even if it is zero it is not a LEADING zero.
//            /// </summary>
//            /// <param name="chars">The chars to compare.</param>
//            /// <returns>The number of sequential zeros that start the specified <paramref name="chars"/>.</returns>
//            public static int CountLeadingZeros(ReadOnlySpan<char> chars)
//            {
//                int count = 0;
//                if (chars.Length <= 1) { return count; }

//                int length = chars.Length - 1;
//                for (int index = 0; index < length; index++)
//                { if (chars[index] == '0') { count++; } else { break; } }
//                return count;
//            }

//            public static int CountLeadingZeros(string chars) => CountLeadingZeros(chars.AsSpan());

//            /// <summary>
//            /// Returns a new numeric <see cref="string"/> representing the result of incrementing the original numeric string.
//            /// The result is equivalent to converting <paramref name="chars"/> to a number, incrementing it by one,
//            /// and converting the result back to a string.
//            /// As the method name suggests, it is assumed that the specified <paramref name="chars"/> is comprised
//            /// of only digit characters.
//            /// The method does not perform any actual conversion to a numeric type, so theoretically there is
//            /// no limit to the size of the number.
//            /// </summary>
//            /// <param name="chars">The value to increment.</param>
//            /// <returns>A new numeric string representing the result of incrementing the original numeric string.</returns>
//            public static string IncrementAssumeOnlyDigits(ReadOnlySpan<char> chars)
//            {
//                // Empty becomes '0', conceptually this is an empty token being created.
//                if (chars.IsEmpty) { return "0"; }

//                // Find the first char from end of source that is NOT nine;
//                // otherwise -1 if all nines.
//                int position = -1;
//                for (int index = chars.Length - 1; index > -1; index--)
//                { if (chars[index] != '9') { position = index; break; } }

//                // If no non-nine is found, then dest length is source length plus one.
//                // Set first dest char to '1' and the rest to '0'.
//                if (position == -1)
//                {
//#if NETSTANDARD2_0
//                    return "1" + new string('0', chars.Length);
//#else
//                    return string.Create(chars.Length + 1, 0, (dest, state) =>
//                    {
//                        dest[state] = '1';
//                        for (int index = state + 1; index < dest.Length; index++) { dest[index] = '0'; }
//                    });
//#endif
//                }
//                else
//                {
//                    //return chars.ToString();
//                    //#if NETSTANDARD2_0
//                    // Looks like 4 allocations to the string.Create() count of 2 allocations.
//                    return chars.Slice(0, position).ToString()
//                        + (char) (chars[position] + 1)
//                        + new string('0', chars.Length - position - 1);
//                    //#else
//                    //                    // Copy source chars before the target position to dest,
//                    //                    // then set target position dest char to incremented source char,
//                    //                    // then set dest chars after the target position to '0'.
//                    //                    // TODO: There has to be a better way to pass a ReadOnlySpan to Create.
//                    //                    //       Changing this method's signature to take a string is not acceptable.
//                    //                    //       Prob have to wait until the dotnet team fixes this oversight officially.
//                    //                    //       Until then, two allocations is better than the three non-create form would cause.
//                    //                    return string.Create(chars.Length, (position, source: chars.ToString()), (dest, state) =>
//                    //                    {
//                    //                        for (int index = 0; index < state.position; index++) { dest[index] = state.source[index]; }
//                    //                        dest[state.position] = (char)(state.source[state.position] + 1);
//                    //                        for (int index = state.position + 1; index < dest.Length; index++) { dest[index] = '0'; }
//                    //                    });
//                    //#endif
//                }
//            }

//            public static string IncrementAssumeOnlyDigits(string chars) => IncrementAssumeOnlyDigits(chars.AsSpan());

//            /// <summary>
//            /// Compares two char sequences as if they were numeric values, returning <c>-1</c>, <c>0</c>, or <c>1</c>.
//            /// As the method name suggests, it is assumed that the specified values are comprised of only digit characters.
//            /// The method does not perform any actual conversion to a numeric type, so theoretically there is
//            /// no limit to the size of the numbers.
//            /// </summary>
//            /// <param name="left">The first digits to be compared.</param>
//            /// <param name="right">The other digits to be compared.</param>
//            /// <returns>
//            /// <c>-1</c> if left is less than right;
//            /// <c>1</c> if left is greater than right;
//            /// otherwise <c>0</c> if left and right are equal.
//            /// </returns>
//            public static int CompareAssumeOnlyDigits(ReadOnlySpan<char> left, ReadOnlySpan<char> right)
//            {
//                // Handle any empty spans, empty lower than non-empty.
//                if (left.IsEmpty && right.IsEmpty) { return 0; }
//                else if (left.IsEmpty) { return -1; }
//                else if (right.IsEmpty) { return 1; }

//                // To approximate numeric comparison, trim any LEADING zeros,
//                // but do NOT trim the last character even if it is a zero.
//                var spanLeft = left.Slice(Semantic.Utils.CountLeadingZeros(left));
//                var spanRight = right.Slice(Semantic.Utils.CountLeadingZeros(right));

//                int result;
//                // Shorter numbers are less than longer, so lower.
//                if (spanLeft.Length < spanRight.Length) { result = -1; }
//                // Longer numbers are greater than shorter, so higher.
//                else if (spanLeft.Length > spanRight.Length) { result = 1; }
//                // SequenceCompareTo can throw values outside -1, 0, and 1, so need to clamp it.
//                else { result = Semantic.Utils.Clamp(spanLeft.SequenceCompareTo(spanRight), -1, 1); }

//                // If values are numerically equal, but one of them originally had leading zeros,
//                // then which is lower precedence depends on the order they are compared, leading to
//                // inconsistent results. To have fully deterministic results, we have to choose one
//                // as lower than the other.
//                // The shorter original value is considered lower.
//                if (result == 0 && left.Length < right.Length) { result = -1; }
//                else if (result == 0 && left.Length > right.Length) { result = 1; }

//                //Console.WriteLine($"{left} ({spanLeft.Length}) | {right} ({spanRight.Length}) | {result}");
//                return result;
//            }

//            public static int CompareAssumeOnlyDigits(string left, string right) => CompareAssumeOnlyDigits(left.AsSpan(), right.AsSpan());

//            /// <summary>
//            /// Compares two char sequences for equality as if they were numeric values.
//            /// </summary>
//            /// <param name="left">The digits to compare to the other.</param>
//            /// <param name="right">The digits to be compared against.</param>
//            /// <returns><c>true</c> if char sequences are numerically equivalent; otherwise <c>false</c>.</returns>
//            public static bool EquateAssumeOnlyDigits(ReadOnlySpan<char> left, ReadOnlySpan<char> right)
//            {
//                if (left.IsEmpty || right.IsEmpty) { return left.IsEmpty == right.IsEmpty; }
//                var spanLeft = left.Slice(Semantic.Utils.CountLeadingZeros(left));
//                var spanRight = right.Slice(Semantic.Utils.CountLeadingZeros(right));
//                return spanLeft.SequenceEqual(spanRight);
//            }

//            public static bool EquateAssumeOnlyDigits(string left, string right) => EquateAssumeOnlyDigits(left.AsSpan(), right.AsSpan());

//        }
//    }
//}
