﻿//#region Copyright (c) 2019 Jay Jeckel
//// Copyright (c) 2019 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//using System;
//using WriteSet = System.Collections.Generic.IList<string>;

//namespace CommonCore.Versioning
//{
//    public static partial class Semantic
//    {
//        /// <summary>
//        /// The <see cref="Semantic.Op"/> static sub-class provides the functionality and logic
//        /// for manipulating semantic models, sets, and tokens.
//        /// </summary>
//        public static class Op
//        {
//            /// <summary>
//            /// Increments the numeric token at the specified <paramref name="index"/> in the
//            /// specified <paramref name="set"/>. Specifying <see cref="Index.End"/> as the
//            /// <paramref name="index"/> will append the default numeric token '0' to the set.
//            /// </summary>
//            /// <param name="set">The semantic set to manipulate.</param>
//            /// <param name="index">The numeric token to increment.</param>
//            /// <exception cref="ArgumentNullException">If <paramref name="set"/> is <c>null</c>.</exception>
//            public static void Increment(WriteSet set, Index index)
//            {
//                if (set is null) { throw new ArgumentNullException(nameof(set)); }
//                if (index.Equals(Index.End)) { set.Add(Semantic.Lex.DefaultToken); }
//                else { set[index] = Semantic.Utils.IncrementAssumeOnlyDigits(set[index].AsSpan()); }
//            }

//            /// <summary>
//            /// Resets the numeric token at the specified <paramref name="index"/> in the
//            /// specified <paramref name="set"/> to the default numeric token '0'.
//            /// Optionally, the length of the token can be preserved in which case each
//            /// position in the token will be reset to the default numeric token.
//            /// </summary>
//            /// <param name="set">The semantic set to manipulate.</param>
//            /// <param name="index">The numeric token to reset.</param>
//            /// <param name="length">Length of the reset token; <c>0</c> will preserve the current length.</param>
//            /// <exception cref="ArgumentNullException">If <paramref name="set"/> is <c>null</c>.</exception>
//            /// <exception cref="ArgumentOutOfRangeException">If <paramref name="index"/> is <see cref="Index.End"/> or <paramref name="index"/> is less than <c>0</c>.</exception>
//            public static void Reset(WriteSet set, Index index, int length = 1)
//            {
//                if (set is null) { throw new ArgumentNullException(nameof(set)); }
//                if (index.Equals(Index.End)) { throw new ArgumentOutOfRangeException(nameof(index)); }
//                if (length < 0) { throw new ArgumentOutOfRangeException(nameof(length)); }
//                if (Semantic.Utils.HasOnlyDigits(set[index].AsSpan()))
//                { set[index] = new string(Semantic.Lex.DefaultToken[0], (length == 0 ? set[index].Length : length)); }
//            }
//        }
//    }
//}
