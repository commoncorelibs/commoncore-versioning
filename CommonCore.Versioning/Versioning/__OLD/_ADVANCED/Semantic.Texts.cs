﻿//#region Copyright (c) 2019 Jay Jeckel
//// Copyright (c) 2019 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//namespace CommonCore.Versioning
//{
//    public static partial class Semantic
//    {
//        /// <summary>
//        /// The <see cref="Semantic.Texts"/> static sub-class provides various
//        /// strings needed by the rest of the library.
//        /// </summary>
//        public static class Texts
//        {
//            /// <summary>
//            /// Gets the titles of the three semantic sets.
//            /// </summary>
//            public static string[] SetTitles { get; } = new[] { "core", "cycle", "build" };

//            /// <summary>
//            /// Gets the format used to create validation error messages.
//            /// Arguments are passed in the order rule title, rule message, subject title, subject text.
//            /// </summary>
//            public static string ErrorMessageFormat { get; } = "{0}: The {2} '{3}' is not valid. {1}";
//        }
//    }
//}
