﻿//#region Copyright (c) 2019 Jay Jeckel
//// Copyright (c) 2019 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text.RegularExpressions;

//namespace CommonCore.Versioning
//{
//    public struct SemanticVersionStruct : IComparable, IComparable<SemanticVersionStruct>, IEquatable<SemanticVersionStruct>
//    {
//        [Flags]
//        public enum BumpType : int { None = 0, Major = 1, Minor = 2, Patch = 4, Cycle = 8, Build = 16 }

//        public const string DefaultTitleMajor = "major";

//        public const string DefaultTitleMinor = "minor";

//        public const string DefaultTitlePatch = "patch";

//        public const string DefaultTitleCycle = "cycle";

//        public const string DefaultTitleBuild = "build";

//        public const string DefaultPrefixCycle = "pre";

//        public const string DefaultPrefixBuild = "build";

//        public static readonly SemanticVersionMeta.EPrecedence DefaultPrecedenceCycle = SemanticVersionMeta.EPrecedence.PreRelease;

//        public static readonly SemanticVersionMeta.EPrecedence DefaultPrecedenceBuild = SemanticVersionMeta.EPrecedence.PostRelease;

//        public static SemanticVersionStruct Zero { get; } = new SemanticVersionStruct(0, 0, 0);

//        public static SemanticVersionStruct One { get; } = new SemanticVersionStruct(1, 0, 0);

//        public static Regex PatternLazy { get; } = new Regex(
//            @"^(.*?)\.(.*?)?\.(.*?)?"
//            + @"(?:-(.*?))?"
//            + @"(?:\+(.*?))?$",
//            RegexOptions.CultureInvariant);

//        public static Regex PatternStrict { get; } = new Regex(
//            @"^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)"
//            + @"(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?"
//            + @"(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$",
//            RegexOptions.CultureInvariant);

//        /// <summary>
//        /// Builds a semantic version string representation of the given values.
//        /// If <paramref name="cycle"/> or <paramref name="build"/> are <c>null</c>
//        /// or <see cref="string.Empty"/>, then that value will be ignored.
//        /// </summary>
//        /// <param name="major">The major part of the semantic version.</param>
//        /// <param name="minor">The minor part of the semantic version.</param>
//        /// <param name="patch">The patch part of the semantic version.</param>
//        /// <param name="cycle">The optional cycle part of the semantic version.</param>
//        /// <param name="build">The optional build part of the semantic version.</param>
//        public static string BuildVersionString(int major, int minor, int patch, string cycle, string build)
//        {
//            var version = $"{major}.{minor}.{patch}";
//            if (!string.IsNullOrEmpty(cycle)) { version += $"-{cycle}"; }
//            if (!string.IsNullOrEmpty(build)) { version += $"+{build}"; }
//            return version;
//        }

//        public static bool Equate(SemanticVersionStruct left, SemanticVersionStruct right) => left.Major == right.Major && left.Minor == right.Minor && left.Patch == right.Patch && left.Cycle == right.Cycle && left.Build == right.Build;

//        public static int Compare(SemanticVersionStruct left, SemanticVersionStruct right)
//        {
//            int result;

//            result = left.Major.CompareTo(right.Major);
//            if (result != 0) { return result; }
//            result = left.Minor.CompareTo(right.Minor);
//            if (result != 0) { return result; }
//            result = left.Patch.CompareTo(right.Patch);
//            if (result != 0) { return result; }
//            result = left.Cycle.CompareTo(right.Cycle);
//            if (result != 0) { return result; }
//            result = left.Build.CompareTo(right.Build);
//            if (result != 0) { return result; }

//            return 0;
//        }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="SemanticVersionStruct" /> struct from the given values.
//        /// </summary>
//        /// <param name="major">The major value of the semantic version.</param>
//        /// <param name="minor">The minor value of the semantic version.</param>
//        /// <param name="patch">The patch value of the semantic version.</param>
//        public SemanticVersionStruct(int major, int minor, int patch) : this(major, minor, patch, null, null) { }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="SemanticVersionStruct" /> struct from the given values.
//        /// If <paramref name="cycle"/> or <paramref name="build"/> are <c>null</c>
//        /// then that part of the meta will be ignored.
//        /// </summary>
//        /// <param name="major">The major value of the semantic version.</param>
//        /// <param name="minor">The minor value of the semantic version.</param>
//        /// <param name="patch">The patch value of the semantic version.</param>
//        /// <param name="cycle">The optional cycle text of the semantic version.</param>
//        /// <param name="build">The optional build text of the semantic version.</param>
//        public SemanticVersionStruct(int major, int minor, int patch, string cycle, string build)
//            : this(new SemanticVersionIdentifier(DefaultTitleMajor, major),
//                  new SemanticVersionIdentifier(DefaultTitleMinor, minor),
//                  new SemanticVersionIdentifier(DefaultTitlePatch, patch),
//                  new SemanticVersionMeta(DefaultTitleCycle, cycle, DefaultPrecedenceCycle),
//                  new SemanticVersionMeta(DefaultTitleBuild, build, DefaultPrecedenceBuild)) { }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="SemanticVersionStruct" /> struct from the given values.
//        /// </summary>
//        /// <param name="major">The major identifier of the semantic version.</param>
//        /// <param name="minor">The minor identifier of the semantic version.</param>
//        /// <param name="patch">The patch identifier of the semantic version.</param>
//        /// <param name="cycle">The optional cycle meta of the semantic version.</param>
//        /// <param name="build">The optional build meta of the semantic version.</param>
//        public SemanticVersionStruct(SemanticVersionIdentifier major, SemanticVersionIdentifier minor, SemanticVersionIdentifier patch, SemanticVersionMeta cycle, SemanticVersionMeta build)
//        {
//            this.Text = BuildVersionString((int)major, (int)minor, (int)patch, (string)cycle, (string)build);
//            this.Major = major;
//            this.Minor = minor;
//            this.Patch = patch;
//            this.Cycle = cycle;
//            this.Build = build;
//        }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="SemanticVersionStruct" /> struct from a <see cref="string"/>.
//        /// </summary>
//        /// <param name="text">The version string to parse.</param>
//        /// <exception cref="ArgumentNullException">If a null version string is passed.</exception>
//        /// <exception cref="FormatException">If the version string cannot be parsed as a semantic version.</exception>
//        public SemanticVersionStruct(string text)
//        {
//            this.Text = text ?? throw new ArgumentNullException(nameof(text));

//            var match = SemanticVersionStruct.PatternLazy.Match(text);
//            if (!match.Success)
//            { throw new FormatException($"Semantic Version string MUST contain at least major, minor, and patch separated by a dot (X.Y.Z)."); }

//            this.Major = new SemanticVersionIdentifier(DefaultTitleMajor, match.Groups[1].Value);
//            this.Minor = new SemanticVersionIdentifier(DefaultTitleMinor, match.Groups[2].Value);
//            this.Patch = new SemanticVersionIdentifier(DefaultTitlePatch, match.Groups[3].Value);

//            Group group;
//            group = match.Groups[4];
//            this.Cycle = new SemanticVersionMeta(DefaultTitleCycle, (group.Success ? group.Value : null), DefaultPrecedenceCycle);
//            group = match.Groups[5];
//            this.Build = new SemanticVersionMeta(DefaultTitleBuild, (group.Success ? group.Value : null), DefaultPrecedenceBuild);
//        }

//        public string Text { get; }

//        /// <summary>
//        /// Gets the major part of the semantic version.
//        /// Major version X (X.y.z | X > 0) MUST be incremented if any backwards incompatible changes are introduced to the public API. It MAY also include minor and patch level changes. Patch and minor version MUST be reset to 0 when major version is incremented.
//        /// </summary>
//        public SemanticVersionIdentifier Major { get; }

//        /// <summary>
//        /// Gets the minor part of the semantic version.
//        /// Minor version Y (x.Y.z | x > 0) MUST be incremented if new, backwards compatible functionality is introduced to the public API. It MUST be incremented if any public API functionality is marked as deprecated. It MAY be incremented if substantial new functionality or improvements are introduced within the private code. It MAY include patch level changes. Patch version MUST be reset to 0 when minor version is incremented.
//        /// </summary>
//        public SemanticVersionIdentifier Minor { get; }

//        /// <summary>
//        /// Gets the patch part of the semantic version.
//        /// Patch version Z (x.y.Z | x > 0) MUST be incremented if only backwards compatible bug fixes are introduced. A bug fix is defined as an internal change that fixes incorrect behavior.
//        /// </summary>
//        public SemanticVersionIdentifier Patch { get; }

//        /// <summary>
//        /// Gets the cycle (ie pre-release,release candidate, etc) part of the semantic version.
//        /// A cycle version MAY be denoted by appending a hyphen and a series of dot separated identifiers immediately following the patch version. Identifiers MUST comprise only ASCII alphanumerics and hyphen [0-9A-Za-z-]. Identifiers MUST NOT be empty. Numeric identifiers MUST NOT include leading zeros. Cycle versions have a lower precedence than the associated normal version. A cycle version indicates that the version is unstable and might not satisfy the intended compatibility requirements as denoted by its associated normal version.
//        /// Examples: 1.0.0-alpha, 1.0.0-alpha.1, 1.0.0-0.3.7, 1.0.0-x.7.z.92.
//        /// </summary>
//        public SemanticVersionMeta Cycle { get; }

//        /// <summary>
//        /// Gets the build part of the semantic version.
//        /// Build metadata MAY be denoted by appending a plus sign and a series of dot separated identifiers immediately following the patch or pre-release version. Identifiers MUST comprise only ASCII alphanumerics and hyphen [0-9A-Za-z-]. Identifiers MUST NOT be empty. Build metadata MUST be ignored when determining version precedence. Thus two versions that differ only in the build metadata, have the same precedence.
//        /// Examples: 1.0.0-alpha+001, 1.0.0+20130313144700, 1.0.0-beta+exp.sha.5114f85.
//        /// </summary>
//        public SemanticVersionMeta Build { get; }

//        public int Length => this.Text.Length;

//        public SemanticVersionStruct Bump(BumpType bump)
//        {
//            switch (bump)
//            {
//                case BumpType.Major: { return this.BumpMajor(); }
//                case BumpType.Minor: { return this.BumpMinor(); }
//                case BumpType.Patch: { return this.BumpPatch(); }
//                case BumpType.Cycle: { return this.BumpCycle(); }
//                case BumpType.Build: { return this.BumpBuild(); }
//                default: { return this; }
//            }
//            //var major = ((bump & EBump.Major) == EBump.Major ? this.Major.GetBumped() : this.Major);
//            //var minor = ((bump & EBump.Minor) == EBump.Minor ? this.Minor.GetBumped() : this.Minor);
//            //var patch = ((bump & EBump.Patch) == EBump.Patch ? this.Patch.GetBumped() : this.Patch);
//            //var cycle = ((bump & EBump.Cycle) == EBump.Cycle ? this.Cycle.GetBumped(DefaultPrefixCycle) : this.Cycle);
//            //var build = ((bump & EBump.Build) == EBump.Build ? this.Build.GetBumped(DefaultPrefixBuild) : this.Build);
//            //return new SemanticVersion(major, minor, patch, cycle, build);
//        }

//        public SemanticVersionStruct BumpMajor() => new SemanticVersionStruct(this.Major.GetBumped(), this.Minor.GetReset(), this.Patch.GetReset(), this.Cycle.GetReset(), this.Build.GetReset());

//        public SemanticVersionStruct BumpMinor() => new SemanticVersionStruct(this.Major, this.Minor.GetBumped(), this.Patch.GetReset(), this.Cycle.GetReset(), this.Build.GetReset());

//        public SemanticVersionStruct BumpPatch()
//        {
//            var patch = (this.Cycle.Exists ? this.Patch : this.Patch.GetBumped());
//            return new SemanticVersionStruct(this.Major, this.Minor, patch, this.Cycle.GetReset(), this.Build.GetReset());
//        }

//        public SemanticVersionStruct BumpCycle(string prefix = DefaultPrefixCycle)
//        {
//            var patch = (this.Cycle.Exists ? this.Patch : this.Patch.GetBumped());
//            return new SemanticVersionStruct(this.Major, this.Minor, patch, this.Cycle.GetBumped(prefix), this.Build.GetReset());
//        }

//        public SemanticVersionStruct BumpBuild(string prefix = DefaultPrefixBuild) => new SemanticVersionStruct(this.Major, this.Minor, this.Patch, this.Cycle, this.Build.GetBumped(prefix));

//        //public SemanticVersion SetCycle(string cycle) => new SemanticVersion(this.Major, this.Minor, this.Patch, new SemanticVersionMeta(DefaultTitleCycle, cycle, SemanticVersionMeta.EPrecedence.PreRelease), this.Build);

//        //public SemanticVersion SetBuild(string build) => new SemanticVersion(this.Major, this.Minor, this.Patch, this.Cycle, new SemanticVersionMeta(DefaultTitleBuild, build, SemanticVersionMeta.EPrecedence.PostRelease));

//        //public SemanticVersion SetMeta(string cycle, string build) => new SemanticVersion(this.Major, this.Minor, this.Patch, new SemanticVersionMeta(DefaultTitleCycle, cycle, SemanticVersionMeta.EPrecedence.PreRelease), new SemanticVersionMeta(DefaultTitleBuild, build, SemanticVersionMeta.EPrecedence.PostRelease));

//        public SemanticVersionStruct StripCycle() => new SemanticVersionStruct(this.Major, this.Minor, this.Patch, this.Cycle.GetReset(), this.Build);

//        public SemanticVersionStruct StripBuild() => new SemanticVersionStruct(this.Major, this.Minor, this.Patch, this.Cycle, this.Build.GetReset());

//        public SemanticVersionStruct StripMeta() => new SemanticVersionStruct(this.Major, this.Minor, this.Patch, this.Cycle.GetReset(), this.Build.GetReset());

//        public override string ToString() => BuildVersionString((int)this.Major, (int)this.Minor, (int)this.Patch, (string)this.Cycle, (string)this.Build);

//        public override int GetHashCode() => 1186239758 + EqualityComparer<string>.Default.GetHashCode(this.Text);

//        public override bool Equals(object obj) => (obj is SemanticVersionStruct ? this.Equals((SemanticVersionStruct)obj) : false);

//        public bool Equals(SemanticVersionStruct other) => Equate(this, other);

//        public int CompareTo(object obj) => (obj is SemanticVersionStruct ? this.CompareTo((SemanticVersionStruct)obj) : throw new ArgumentException($"Invalid comparison type '{obj.GetType()}'.", nameof(obj)));

//        public int CompareTo(SemanticVersionStruct other) => Compare(this, other);

//        public static implicit operator string(SemanticVersionStruct version) => version.Text;

//        public static bool operator ==(SemanticVersionStruct left, SemanticVersionStruct right) => Equate(left, right);

//        public static bool operator !=(SemanticVersionStruct left, SemanticVersionStruct right) => !Equate(left, right);

//        public static bool operator >(SemanticVersionStruct left, SemanticVersionStruct right) => Compare(left, right) > 0;

//        public static bool operator >=(SemanticVersionStruct left, SemanticVersionStruct right) => Compare(left, right) >= 0;

//        public static bool operator <(SemanticVersionStruct left, SemanticVersionStruct right) => Compare(left, right) < 0;

//        public static bool operator <=(SemanticVersionStruct left, SemanticVersionStruct right) => Compare(left, right) <= 0;






























//        public void Validate()
//        {
//            var exceptions = new List<Exception>();

//            try
//            {
//                SemanticVersionRule.NotEmptyRule.Validate(this.Major.Title, this.Major);
//                SemanticVersionRule.OnlyDigitsRule.Validate(this.Major.Title, this.Major);
//                SemanticVersionRule.NotLeadingZeroRule.Validate(this.Major.Title, this.Major);
//            }
//            catch (Exception exception)
//            { exceptions.Add(exception); }
//            try
//            {
//                SemanticVersionRule.NotEmptyRule.Validate(this.Minor.Title, this.Minor);
//                SemanticVersionRule.OnlyDigitsRule.Validate(this.Minor.Title, this.Minor);
//                SemanticVersionRule.NotLeadingZeroRule.Validate(this.Minor.Title, this.Minor);
//            }
//            catch (Exception exception)
//            { exceptions.Add(exception); }
//            try
//            {
//                SemanticVersionRule.NotEmptyRule.Validate(this.Patch.Title, this.Patch);
//                SemanticVersionRule.OnlyDigitsRule.Validate(this.Patch.Title, this.Patch);
//                SemanticVersionRule.NotLeadingZeroRule.Validate(this.Patch.Title, this.Patch);
//            }
//            catch (Exception exception)
//            { exceptions.Add(exception); }

//            if (this.Cycle.Exists)
//            {
//                foreach (var identifier in this.Cycle)
//                {
//                    try
//                    {
//                        SemanticVersionRule.NotEmptyRule.Validate(identifier);
//                        if (identifier.Numeric)
//                        {
//                            SemanticVersionRule.OnlyDigitsRule.Validate(identifier);
//                            SemanticVersionRule.NotLeadingZeroRule.Validate(identifier);
//                        }
//                        else
//                        {
//                            SemanticVersionRule.OnlyAlphaNumericRule.Validate(identifier);
//                        }
//                    }
//                    catch (Exception exception)
//                    { exceptions.Add(exception); }
//                }
//            }

//            if (this.Build.Exists)
//            {
//                foreach (var identifier in this.Build)
//                {
//                    try
//                    {
//                        SemanticVersionRule.NotEmptyRule.Validate(identifier);
//                        if (identifier.Numeric)
//                        { SemanticVersionRule.OnlyDigitsRule.Validate(identifier); }
//                        else
//                        { SemanticVersionRule.OnlyAlphaNumericRule.Validate(identifier); }
//                    }
//                    catch (Exception exception)
//                    { exceptions.Add(exception); }
//                }
//            }

//            if (exceptions.Count > 0)
//            {
//                throw new AggregateException(exceptions);
//            }
//        }
//    }
//}
