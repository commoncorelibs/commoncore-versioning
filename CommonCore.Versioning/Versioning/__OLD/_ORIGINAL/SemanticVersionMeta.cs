﻿//#region Copyright (c) 2019 Jay Jeckel
//// Copyright (c) 2019 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//using System;
//using System.Collections;
//using System.Collections.Generic;

//namespace CommonCore.Versioning
//{
//    public struct SemanticVersionMeta : IComparable, IComparable<SemanticVersionMeta>, IEquatable<SemanticVersionMeta>, IEnumerable<SemanticVersionIdentifier>
//    {
//        public enum EPrecedence : int { PreRelease = -1, PostRelease = 1 }

//        public static bool Equate(SemanticVersionMeta left, SemanticVersionMeta right)
//        {
//            if (left.Count != right.Count) { return false; }
//            for (int index = 0; index < left.Count; index++)
//            {
//                if (left[index] != right[index])
//                { return false; }
//            }
//            return true;
//        }

//        public static int Compare(SemanticVersionMeta left, SemanticVersionMeta right)
//        {
//            var precedence = -1 * (int)left.Precedence;
//            if (!left.Exists && !right.Exists) { return 0; }
//            else if (left.Exists && !right.Exists) { return -1 * precedence; }
//            else if (!left.Exists && right.Exists) { return 1 * precedence; }
//            else if (left.Count < right.Count)
//            {
//                for (int index = 0; index < left.Count; index++)
//                {
//                    int result = left[index].CompareTo(right[index]);
//                    if (result != 0) { return result; }
//                }
//                return -1;
//            }
//            else if (left.Count > right.Count)
//            {
//                for (int index = 0; index < right.Count; index++)
//                {
//                    int result = left[index].CompareTo(right[index]);
//                    if (result != 0)
//                    { return result; }
//                }
//                return 1;
//            }
//            else
//            {
//                for (int index = 0; index < left.Count; index++)
//                {
//                    int result = left[index].CompareTo(right[index]);
//                    if (result != 0)
//                    { return result; }
//                }
//                return 0;
//            }
//        }

//        public static SemanticVersionMeta None { get; } = new SemanticVersionMeta();

//        public SemanticVersionMeta(string title, string text, EPrecedence precedence = EPrecedence.PreRelease)
//        {
//            this.Title = title ?? throw new ArgumentNullException(nameof(title));
//            this.Input = text;
//            this.Precedence = precedence;

//            this.Identifiers = null;
//            if (text != null)
//            {
//                var pieces = this.Input.Split('.');
//                var ids = new SemanticVersionIdentifier[pieces.Length];
//                for (int index = 0; index < pieces.Length; index++)
//                { ids[index] = new SemanticVersionIdentifier($"{this.Title}[{index}]", pieces[index]); }
//                this.Identifiers = Array.AsReadOnly(ids);
//            }
//        }

//        public string Title { get; }

//        public string Input { get; }

//        public EPrecedence Precedence { get; }

//        public int Length => this.Input?.Length ?? 0;

//        public bool Exists => this.Count > 0;

//        public int Count => this.Identifiers?.Count ?? 0;

//        public IReadOnlyList<SemanticVersionIdentifier> Identifiers { get; }

//        public SemanticVersionIdentifier this[int index]// => this.Identifiers[index];
//        {
//            get
//            {
//                if (index < 0 || index >= this.Count)
//                { throw new IndexOutOfRangeException($"Index '{index}' was outside the bounds [0, {this.Count}] of the array."); }
//                return this.Identifiers[index];
//            }
//        }

//        public SemanticVersionMeta GetBumped(string prefix)
//        {
//            if (!this.Exists || this.Length == 0)
//            { return new SemanticVersionMeta(this.Title, $"{prefix}.0", this.Precedence); }

//            int replaceIndex = -1;
//            for (int index = this.Count - 1; index > -1; index--)
//            {
//                var identifier = this[index];
//                if (identifier.Numeric)
//                {
//                    replaceIndex = index;
//                    break;
//                }
//            }
//            if (replaceIndex == -1)
//            { return new SemanticVersionMeta(this.Title, $"{this}.0", this.Precedence); }
//            else
//            {
//                string output = string.Empty;
//                for (int index = 0; index < this.Count; index++)
//                {
//                    if (index > 0) { output += "."; }
//                    output += (index != replaceIndex ? this[index] : this[index].GetBumped());
//                }
//                return new SemanticVersionMeta(this.Title, output, this.Precedence);
//            }
//        }

//        public SemanticVersionMeta GetReset() => new SemanticVersionMeta(this.Title, null, this.Precedence);

//        public override string ToString() => this.Input;

//        public override int GetHashCode() => 1186239758 + EqualityComparer<string>.Default.GetHashCode(this.Input);

//        public override bool Equals(object obj) => (obj is SemanticVersionMeta ? this.Equals((SemanticVersionMeta)obj) : false);

//        public bool Equals(SemanticVersionMeta other) => Equate(this, other);

//        public int CompareTo(object obj) => (obj is SemanticVersionMeta ? this.CompareTo((SemanticVersionMeta)obj) : throw new ArgumentException($"Invalid comparison type '{obj.GetType()}'.", nameof(obj)));

//        public int CompareTo(SemanticVersionMeta other) => Compare(this, other);

//        public IEnumerator<SemanticVersionIdentifier> GetEnumerator() => this.Identifiers?.GetEnumerator();

//        IEnumerator IEnumerable.GetEnumerator() => this.Identifiers?.GetEnumerator();

//        public static explicit operator string(SemanticVersionMeta version) => version.Input;

//        public static bool operator ==(SemanticVersionMeta left, SemanticVersionMeta right) => Equate(left, right);

//        public static bool operator !=(SemanticVersionMeta left, SemanticVersionMeta right) => !Equate(left, right);

//        public static bool operator >(SemanticVersionMeta left, SemanticVersionMeta right) => Compare(left, right) > 0;

//        public static bool operator >=(SemanticVersionMeta left, SemanticVersionMeta right) => Compare(left, right) >= 0;

//        public static bool operator <(SemanticVersionMeta left, SemanticVersionMeta right) => Compare(left, right) < 0;

//        public static bool operator <=(SemanticVersionMeta left, SemanticVersionMeta right) => Compare(left, right) <= 0;
//    }
//}
