﻿//#region Copyright (c) 2019 Jay Jeckel
//// Copyright (c) 2019 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Globalization;
//using System.Linq;

//namespace CommonCore.Versioning
//{
//    [Obsolete("Moving to SemanticIdentifier.", false)]
//    public struct SemanticVersionIdentifier : IComparable, IComparable<SemanticVersionIdentifier>, IEquatable<SemanticVersionIdentifier>, IEnumerable<char>
//    {
//        public static SemanticVersionIdentifier None { get; } = new SemanticVersionIdentifier();

//        public static bool Equals(SemanticVersionIdentifier left, SemanticVersionIdentifier right) => (left.Numeric && right.Numeric ? left.Value == right.Value : !left.Numeric && !right.Numeric ? left.Text == right.Text : false);

//        public static int Compare(SemanticVersionIdentifier left, SemanticVersionIdentifier right) => (left.Numeric && right.Numeric ? left.Value.CompareTo(right.Value) : !left.Numeric && !right.Numeric ? left.Text.CompareTo(right.Text) : left.Numeric ? -1 : 1);

//        public SemanticVersionIdentifier(string title, string text)
//        {
//            this.Title = title ?? throw new ArgumentNullException(nameof(title));
//            this.Text = text;
//            this.Value = int.TryParse(this.Text, NumberStyles.None, CultureInfo.InvariantCulture.NumberFormat, out int value) ? value : 0;

//            //bool numeric = true;
//            //if (!string.IsNullOrEmpty(this.Text))
//            //{
//            //    foreach (var c in this.Text)
//            //    { if (SemanticVersionRule.IsAlpha(c)) { numeric = false; break; } }
//            //}
//            //this.Numeric = numeric;
//            this.Numeric = this.Text?.All(c => Semantic.Utils.IsDigit(c)) ?? false;
//        }

//        public SemanticVersionIdentifier(string title, int value)// : this(title, value.ToString()) { }
//        {
//            this.Title = title ?? throw new ArgumentNullException(nameof(title));
//            this.Text = value.ToString();
//            this.Value = value;
//            this.Numeric = value >= 0;
//        }

//        public string Title { get; }

//        public string Text { get; }

//        public int Value { get; }

//        public bool Numeric { get; }

//        public int Length => this.Text?.Length ?? 0;

//        public char this[int index]
//        {
//            get
//            {
//                if (index < 0 || index >= this.Length)
//                { throw new IndexOutOfRangeException($"Index '{index}' was outside the bounds [0, {this.Length}] of the array."); }
//                return this.Text[index];
//            }
//        }

//        public SemanticVersionIdentifier GetBumped()
//        {
//            if (!this.Numeric)
//            { throw new InvalidOperationException("Only Numeric identifiers can be bumped."); }
//            return new SemanticVersionIdentifier(this.Title, this.Value + 1);
//        }

//        public SemanticVersionIdentifier GetReset()
//        {
//            if (!this.Numeric)
//            { throw new InvalidOperationException("Only Numeric identifiers can be reset."); }
//            return new SemanticVersionIdentifier(this.Title, 0);
//        }

//        public override string ToString() => this.Text;

//        public override int GetHashCode() => 1186239758 + EqualityComparer<string>.Default.GetHashCode(this.Text);

//        public override bool Equals(object obj) => (obj is SemanticVersionIdentifier ? this.Equals((SemanticVersionIdentifier)obj) : false);

//        public bool Equals(SemanticVersionIdentifier other) => Equals(this, other);

//        public int CompareTo(object obj) => (obj is SemanticVersionIdentifier ? this.CompareTo((SemanticVersionIdentifier)obj) : throw new ArgumentException($"Invalid comparison type '{obj.GetType()}'.", nameof(obj)));

//        public int CompareTo(SemanticVersionIdentifier other) => Compare(this, other);

//        public IEnumerator<char> GetEnumerator() => ((IEnumerable<char>)this.Text).GetEnumerator();

//        IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable<char>)this.Text).GetEnumerator();

//        public static explicit operator string(SemanticVersionIdentifier version) => version.Text;

//        public static explicit operator int(SemanticVersionIdentifier version) => version.Value;

//        public static bool operator ==(SemanticVersionIdentifier left, SemanticVersionIdentifier right) => Equals(left, right);

//        public static bool operator !=(SemanticVersionIdentifier left, SemanticVersionIdentifier right) => !Equals(left, right);

//        public static bool operator >(SemanticVersionIdentifier left, SemanticVersionIdentifier right) => Compare(left, right) > 0;

//        public static bool operator >=(SemanticVersionIdentifier left, SemanticVersionIdentifier right) => Compare(left, right) >= 0;

//        public static bool operator <(SemanticVersionIdentifier left, SemanticVersionIdentifier right) => Compare(left, right) < 0;

//        public static bool operator <=(SemanticVersionIdentifier left, SemanticVersionIdentifier right) => Compare(left, right) <= 0;
//    }
//}
