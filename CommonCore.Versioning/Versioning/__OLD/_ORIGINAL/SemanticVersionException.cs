﻿//#region Copyright (c) 2019 Jay Jeckel
//// Copyright (c) 2019 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//using System;
//using System.Collections.Generic;
//using System.Runtime.Serialization;

//namespace CommonCore.Versioning
//{
//    /// <summary>
//    /// The exception that is thrown when a <see cref="SemanticVersionIdentifier"/> violates
//    /// a <see cref="SemanticVersionRule"/>.
//    /// </summary>
//    [Obsolete("Moving to SemanticException.", false)]
//    [Serializable]
//    public class SemanticVersionException : Exception, ISerializable
//    {
//        /// <summary>
//        /// Initializes a new instance of the <see cref="SemanticVersionException"/> class.
//        /// </summary>
//        public SemanticVersionException() : base() { }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="SemanticVersionException"/> class
//        /// with specified <paramref name="identifier"/> and <paramref name="rule"/>.
//        /// </summary>
//        /// <param name="identifier">The violating identifier.</param>
//        /// <param name="rule">The violated rule.</param>
//        public SemanticVersionException(SemanticVersionIdentifier identifier, SemanticVersionRule rule) : base($"Identifier {identifier.Title} '{identifier}' is not valid. {rule}")
//        {
//            this.Identifier = identifier;
//            this.Rule = rule;
//        }

//        public SemanticVersionException(string title, IEnumerable<char> identifier, SemanticVersionRule rule) : base($"Identifier {title} '{string.Concat(identifier)}' is not valid. {rule}")
//        {
//            this.Identifier = new SemanticVersionIdentifier(title, string.Concat(identifier));
//            this.Rule = rule;
//        }

//        /// <summary>
//        /// Gets the <see cref="SemanticVersionIdentifier"/> that violated the <see cref="SemanticVersionRule"/>.
//        /// </summary>
//        public SemanticVersionIdentifier Identifier { get; set; }

//        /// <summary>
//        /// Gets the <see cref="SemanticVersionRule"/> that was violated by the <see cref="SemanticVersionIdentifier"/>.
//        /// </summary>
//        public SemanticVersionRule Rule { get; set; }
//    }
//}
