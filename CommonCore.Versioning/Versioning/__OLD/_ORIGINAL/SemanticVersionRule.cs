﻿//#region Copyright (c) 2019 Jay Jeckel
//// Copyright (c) 2019 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;

//namespace CommonCore.Versioning
//{
//    [Obsolete("Moving to SemanticRule and SemanticRules.", false)]
//    public class SemanticVersionRule
//    {
//        /// <summary>
//        /// MUST NOT be empty.
//        /// </summary>
//        public static SemanticVersionRule NotEmptyRule { get; }
//        = new SemanticVersionRule(nameof(NotEmptyRule), "MUST NOT be empty.", identifier => identifier.Any());

//        /// <summary>
//        /// MUST NOT contain leading zeros.
//        /// </summary>
//        public static SemanticVersionRule NotLeadingZeroRule { get; }
//        = new SemanticVersionRule(nameof(NotLeadingZeroRule), "MUST NOT contain leading zeros.", identifier => identifier.Count() <= 1 || identifier.First() != '0');

//        /// <summary>
//        /// MUST contain only ASCII digits (0-9).
//        /// </summary>
//        public static SemanticVersionRule OnlyDigitsRule { get; }
//        = new SemanticVersionRule(nameof(OnlyDigitsRule), "MUST contain only ASCII digits (0-9).", identifier => !identifier.Any(c => !Semantic.Utils.IsDigit(c)));

//        /// <summary>
//        /// MUST contain only ASCII digits, letters, and hyphens (0-9, a-z, A-Z, -).
//        /// </summary>
//        public static SemanticVersionRule OnlyAlphaNumericRule { get; }
//        = new SemanticVersionRule(nameof(OnlyAlphaNumericRule), "MUST contain only ASCII digits, letters, and hyphens (0-9, a-z, A-Z, -).", identifier => !identifier.Any(c => !Semantic.Utils.IsAlphaNumeric(c)));

//        public SemanticVersionRule(string title, string message, Func<IEnumerable<char>, bool> complianceFunc)
//        {
//            this.Title = title;
//            this.Message = message;
//            this.ComplianceFunc = complianceFunc;
//        }

//        public string Title { get; }

//        public string Message { get; }

//        public Func<IEnumerable<char>, bool> ComplianceFunc { get; }

//        public bool IsCompliant(IEnumerable<char> identifier) => this.ComplianceFunc?.Invoke(identifier) ?? false;

//        public void Validate(SemanticVersionIdentifier identifier)
//        { if (!this.IsCompliant(identifier)) { throw new SemanticVersionException(identifier, this); } }

//        public void Validate(string title, IEnumerable<char> identifier)
//        { if (!this.IsCompliant(identifier)) { throw new SemanticVersionException(title, identifier, this); } }

//        public override string ToString() => this.Message;
//    }
//}
