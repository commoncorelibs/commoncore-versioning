# Vinland Solutions CommonCore.Versioning

This library implements the [Semantic Versioning 2.0.0](https://semver.org/) standard. The standard seeks to solve two problems in the software world, putting some systematic meaning to version numbers and standardize their format, composition, and relation to one another. As if often the case, trying to solve two problems with one tool leads to less than optimal results. However, that said, the standard has been embraced and that is all that really matters.

We'll break down the various elements of the standard and how the library handles them, but first a note on terminology. In this document and the CommonCore.Versioning library, the term *cycle* is used instead of the standard's *prerelease* and *pre-release* terms. This was done because *prerelease*, with or without the hyphen, is a clunky term that leads to confusion as to what the "prerelease" part of the version is for. All other terms should be consistent with the standard.

## The Basics

A semantic version is composed of three core elements and two optional meta elements: major, minor, patch, cycle, and build. The major, minor, and patch elements are required and must be positive numbers. The cycle and build elements are optional and are usually composed of dot-delimited numbers and alphanumeric text. The cycle element is prepended with a dash and the build element is prepended with a plus. If both are present then the cycle is first. It's a normal three element version with two optional sub-version labels. Pretty basic stuff.

## What Versions Mean

Arguably the primary purpose of the standard is giving meaning to version numbers and their changing; how the version number changes represents the types of changes made to the project.

* Major: This element is raised when a breaking change has been made.
* Minor: This element is raised when major, non-breaking changes are introduced.
* Patch: This element is raised when minor, non-breaking changes, like bug fixes, are implemented.

If you don't understand why something so simple is important, then count yourself lucky for not experiencing the various hells of dealing with romantic versioning. This document is focused on breaking down the technical details of implementing the standard, so we'll leave it as that and for more details on this aspect please read the standard.

## The Details

For something so straight forward, there is a lot to break down.

Reminder, the term *cycle* is used here instead of the standard's *prerelease* and *pre-release* terms.

### Identifiers

A semantic version is a series of identifiers. There are three types of identifiers with the following rules.

This information is derived from the standard's text to some extent, but the most important source is the listed [Backus�Naur Grammar](https://semver.org/#backusnaur-form-grammar-for-valid-semver-versions).

1. Digital Identifier
   * MUST contain only digits.
   * MUST NOT be empty.
2. Numeric Identifier
   * MUST contain only digits.
   * MUST NOT be empty.
   * MUST NOT contain leading zeros.
3. AlphaNumeric Identifier
   * MUST contain only digits, letters, and hyphen.
   * MUST contain at least one letter or hyphen.
   * MUST NOT be empty.

The three core elements, major, minor, and patch, are each Numeric Identifiers and are required to form a valid semantic version.

The cycle and build meta elements are composed of one or more dot-delimited identifiers. Except that the cycle element MUST NOT contain Digital Identifiers.

### Precedence

Precedence refers to how versions are compared to each other when ordered. The elements of two versions are compared left to right, with the first difference as determined by the following rules dictating the relationship between them.

1. Digital and Numeric Identifiers are compared numerically.
2. AlphaNumeric Identifiers are compared lexically in ASCII sort order.
3. Digital and Numeric Identifiers have lower precedence than AlphaNumeric Identifiers.
4. A cycle version has lower precedence than a non-cycle version.
5. A smaller set of cycle identifiers has lower precedence than a larger set, if cycles are otherwise equal.
6. Build metadata does not figure into precedence.

A few examples of precedence, assuming ascending order:

* 1.0.0 < 2.0.0 < 2.1.0 < 2.1.1
* 1.0.0-alpha < 1.0.0 < 1.0.1-pre.0
* 1.0.0-alpha < 1.0.0-alpha.1 < 1.0.0-alpha.beta < 1.0.0-beta < 1.0.0-beta.2 < 1.0.0-beta.11 < 1.0.0-rc.1 < 1.0.0

### Incrementing

This is where things get hairy. The rules for bumping versions isn't clearly defined in the standard, especially around edge cases, so much has to be extrapolated from other areas of the text and the listed examples. That leads to one straight forward rule for bumping that has two related exceptions.

1. When an element is bumped, increment it and reset all elements to the right.
   * Unless patch is bumped when cycle exists, then reset cycle and build, but don't increment patch.
   * Unless cycle is bumped when it doesn't exist, then increment patch and add cycle.

The standard is very clear that bumping major resets minor and patch, and that bumping minor resets patch, but no mention is given to reseting cycle or build. By extrapolating, bumping cycle should reset build and bumping anything to the left of cycle should reset both cycle and build. This deduction solidifies our rule.

The ground gets muckier when it comes to the interaction of patch and cycle. The standard is clear that cycle versions precede equivalent non-cycle versions, which implies that bumping a cycle version to a non-cycle version is expected. From that we can infer that bumping patch when cycle is present should lead to an equivalent stable, non-cycle version.

Covering the same ground from the other direction, bumping a non-cycle version can't lead backwards to an equivalent, lower cycle version. From this we infer that bumping cycle on a non-cycle version should increment the version before adding the cycle.

If the above ground was mucky, bumping the cycle and build meta elements is a yawning pit of darkness. There is no explicit information in the standard for how to bump the meta elements. Since the most common use-case is a simple unary `bump cycle` or `bump build` situation, the following rules are extrapolated from the standard.

1. The meta identifiers are search in reverse order.
2. The first Numeric Identifier found is incremented.
3. If none is found, then one is appended to the meta element.

Of course, this completely ignores that meta elements are a series of identifiers and any one of them could be bumpable. That area is being investigated for inclusion in a future release of the library. For now, we'll settle for the 80% of use-cases currently covered.
